var gulp = require('gulp');
var sass = require('gulp-sass');
var pump = require('pump');
var uglify = require('gulp-uglify');
var autoprefixer = require('gulp-autoprefixer');
var notify = require('gulp-notify');
var sourcemaps = require('gulp-sourcemaps');


gulp.task('styles', function(){
    return gulp.src("wp-content/themes/lucient/assets/sass/style.scss")
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
        cascade: false
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('wp-content/themes/lucient/public/css'))
    .pipe(notify({message: 'Styles task complete', wait: true}));
});


gulp.task('compress', function(cb) {
  return pump([
        gulp.src('wp-content/themes/lucient/assets/js/*.js'),
        sourcemaps.init(),
        uglify(),
        sourcemaps.write(),
        gulp.dest('wp-content/themes/lucient/public/js')
    ],
    cb
  )
  .pipe(notify({message: 'Compress task complete', wait: true}));
});


gulp.task('default', gulp.parallel('styles', 'compress'));


gulp.task('watch', function() {
    gulp.watch("wp-content/themes/lucient/assets/sass/*.scss", gulp.series('styles'));
    gulp.watch("wp-content/themes/lucient/assets/js/*.js", gulp.series('compress'));
})