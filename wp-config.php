<?php
# Database Configuration
define( 'DB_NAME', 'solidq_staging' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', '' );
define( 'DB_HOST', 'localhost' );
define( 'DB_HOST_SLAVE', 'localhost' );
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', 'utf8_unicode_ci');
$table_prefix = 'wp_';

# Security Salts, Keys, Etc
define('AUTH_KEY',         'h+q-.8QalNV!69L}J[TtxCHMN~c~6XIP}m i+U]A+Hx,=(V~Yj]J3AR%~t[u|?DI');
define('SECURE_AUTH_KEY',  'M_R/Q<@L<<z$^[Zkr{n0$cjF/wHzu q &+U2ByU Uqkke2]>hV*(6zVu9_IGrG+#');
define('LOGGED_IN_KEY',    'D>H)h!gPGlS|w (s0MotC,[)[`/z>uInhX]7r=4FNIY)2nVhBdf}P%89dg*U~Y_H');
define('NONCE_KEY',        '9D+FYavnNCQ5vYq-v4dJe^X79P)cO4=a,;@]Kw.{D(NBD,lP?Y_$jDRxH%~4>Nq,');
define('AUTH_SALT',        'mZ&4,KSnu[X^tnp[LC$QVsSF;1r6*r12E,#Xl~-6j6DI}@NSTQJHi{p.]1?)n7>o');
define('SECURE_AUTH_SALT', '|[YKL%2oJn%%zz|*t&<d% BU!cbDGgFv8J])fu:dVOhS2<O|#+O-Z:m7l|i),UvI');
define('LOGGED_IN_SALT',   '/V[zl@aM.)3/x71aO)&Slot++^N`D{d`|] 1&;ui2H5HX$]0g:-w]{z|;t 8S(v(');
define('NONCE_SALT',       'q_PY[h*{&txDV.`~PfP58:U>,gIUCk%YeB:z@`wAD#A5~|6:z+ZkJ7PQV+uRLF2>');


# Localized Language Stuff

define( 'WP_CACHE', TRUE );

define( 'WP_AUTO_UPDATE_CORE', false );

define( 'PWP_NAME', 'solidqdev' );

define( 'FS_METHOD', 'direct' );

define( 'FS_CHMOD_DIR', 0775 );

define( 'FS_CHMOD_FILE', 0664 );

define( 'DISALLOW_FILE_MODS', FALSE );

define( 'DISALLOW_FILE_EDIT', FALSE );

define( 'DISABLE_WP_CRON', false );

define( 'FORCE_SSL_LOGIN', false );

define( 'WP_POST_REVISIONS', FALSE );


define( 'WP_TURN_OFF_ADMIN_BAR', false );

umask(0002);

$memcached_servers=array ( );
define('WPLANG','');

# WP Engine ID

# WP Engine Settings




define( 'WP_ALLOW_MULTISITE', true );
define( 'MULTISITE', true );
define( 'SUBDOMAIN_INSTALL', false );
define( 'DOMAIN_CURRENT_SITE', 'solidq.roy' );
define( 'PATH_CURRENT_SITE','/' );
define( 'SITE_ID_CURRENT_SITE', 1 );
define( 'BLOG_ID_CURRENT_SITE', 1 );


define('ARLO_LANGUAGE_REGIONS', [
    'test4'    => [
        'arlo_regions'          => 'at', //['us', 'uk', 'at', 'se', 'no', 'it', 'de', 'dk'],
        'website_regions'       => 'at' //['us', 'uk', 'at', 'se', 'no', 'it', 'de', 'dk']
    ],
    'es'    => [
        'arlo_regions'      => 'sp',
        'website_regions'    => 'es'
    ],
    'de'    => [
        'arlo_regions'      => 'de',
        'website_regions'    => 'de'
    ],
    'it'    => [
        'arlo_regions'      => 'it',
        'website_regions'    => 'it'
    ],
    'us'    => [
        'arlo_regions'      => 'us',
        'website_regions'    => 'us'
    ]
]);

# That's It. Pencils down
if ( !defined('ABSPATH') )
	define('ABSPATH', __DIR__ . '/');
require_once(ABSPATH . 'wp-settings.php');
