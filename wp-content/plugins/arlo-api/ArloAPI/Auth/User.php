<?php

namespace ArloAPI\Auth;

use ArloAPI\Resources\Registrations;
use ArloApiSingleton;

class User
{
    protected $first_name;
    protected $last_name;
    protected $email;
    protected $safeEmail;
    protected $useSafeEmail;
    protected $password;
    private $authClient;
    private $validated;

    public function __construct($form_type = null, $first_name = null, $last_name = null, $email = null, $password = null, $password_confirm = null)
    {
        $this->useSafeEmail = false;
        $this->validateInput($form_type, $first_name, $last_name, $email, $password, $password_confirm);
        $this->authClient = \ArloApiSingleton::get_auth_instance();
    }

    private function validateInput($form_type, $first_name, $last_name, $email, $password, $password_confirm)
    {
        $this->validated = true;
        if($form_type == 'register')
        {
            if (isset($first_name) && !empty($first_name))
            {
                $this->first_name = strtolower(trim(filter_var($first_name, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES)));
            }
            else
            {
                if (!is_array($this->validated)) $this->validated = [];
                $this->validated['first_name'] = 'First Name is required';
            }

            if (isset($last_name) && !empty($last_name))
            {
                $this->last_name = strtolower(trim(filter_var($last_name, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES)));
            }
            else
            {
                if (!is_array($this->validated)) $this->validated = [];
                $this->validated['last_name'] = 'Last Name is required';
            }
        }

        if(isset($email) && !empty($email))
        {
            $this->safeEmail = strtolower(trim(filter_var(str_replace("'", "",$email), FILTER_SANITIZE_EMAIL)));
            if(!filter_var($email,FILTER_VALIDATE_EMAIL))
            {
                if(!filter_var($this->safeEmail,FILTER_VALIDATE_EMAIL))
                {
                    if(!is_array($this->validated)) $this->validated = [];
                    $this->validated['email'] = 'Must be a valid email address';
                }
                else
                {
                    $this->useSafeEmail = true;
                    $this->email = addslashes(strtolower(trim(filter_var($email, FILTER_SANITIZE_EMAIL))));
                }
            }
            else
            {
                $this->email = strtolower(trim(filter_var($email, FILTER_SANITIZE_EMAIL)));
            }
        }
        else
        {
            if(!is_array($this->validated)) $this->validated = [];
            $this->validated['email'] = 'Email is required';
        }

        if(isset($password) && !empty($password))
        {
            if($form_type == 'register')
            {
                if (isset($password_confirm) && !empty($password_confirm))
                {
                    if ($password === $password_confirm)
                    {
                        $this->password = trim(filter_var($password, FILTER_SANITIZE_STRING));
                    }
                    else
                    {
                        if (!is_array($this->validated)) $this->validated = [];
                        $this->validated['password'] = 'Password and confirmation do not match';
                    }
                }
                else
                {
                    if (!is_array($this->validated)) $this->validated = [];
                    $this->validated['password_confirm'] = 'Password confirmation is required';
                }
            }
            else
            {
                $this->password = trim(filter_var($password, FILTER_SANITIZE_STRING));
            }
        }
        else
        {
            if(!is_array($this->validated)) $this->validated = [];
            $this->validated['password'] = 'Password is required';
        }

        if($form_type == 'register')
        {
            if (isset($password_confirm) && empty($password_confirm))
            {
                if (!is_array($this->validated)) $this->validated = [];
                $this->validated['password_confirm'] = 'Password confirmation is required';
            }
        }
    }

    public function register()
    {
        $response = new Response('register');
        $user = $this->authClient->getContactForVerification($this->first_name, $this->last_name, $this->email);

        if(is_array($this->validated))
        {
            $response->error = 'Please check the error messages below.';
            $response->validation = $this->validated;
        }
        else
        {
            $wordPressEmail = ($this->useSafeEmail) ? $this->safeEmail : $this->email;
            if($user)
            {
                $user_id = username_exists($wordPressEmail);

                if (!$user_id && email_exists($wordPressEmail) == false)
                {
                    $user_id = wp_create_user($wordPressEmail, $this->password, $wordPressEmail);
                    if (!is_wp_error($user_id))
                    {
                        $verification_code = md5(time() . wp_generate_uuid4());
                        $string = [
                            'user_id' => $user_id,
                            'code' => $verification_code
                        ];

                        update_user_meta($user_id, 'first_name', ucwords(strtolower($this->first_name)));
                        update_user_meta($user_id, 'last_name', ucwords(strtolower($this->last_name)));
                        update_user_meta($user_id, 'account_activated', 0);
                        update_user_meta($user_id, 'activation_code', $verification_code);
                        update_user_meta($user_id, 'certificate_number', $user->Certificate->Number);
                        update_user_meta($user_id, 'certificate_level', $user->Certificate->Level);
                        update_user_meta($user_id, 'certificate_expiry', $user->Certificate->Expiry);
                        update_user_meta($user_id, 'arlo_user_id', $user->Contact->ID);

                        $url = get_site_url() . '/my-account/?act=' . base64_encode(serialize($string));
                        $body = '<html><head> <meta charset="UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <meta http-equiv="X-UA-Compatible" content="ie-edge"> <title></title> <style>*{font-family: "Helvetica Neue", Helvetica, "Segoe UI", Arial, sans-serif;}body{margin: 0px !important; padding: 0px !important;}@media only screen and (max-width:480px){.wrapper{width: 100% !important;}}</style></head><body><!--[if (gte mso 9)|(IE)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff" style="background-color:#ffffff;"> <tr> <td align="center" valign="top"><![endif]--><table align="center" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff"> <tr> <td align="center" valign="top"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellspacing="0" cellpadding="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="600" class="wrapper"> <tbody> <tr> <td align="center" valign="top"> <a href="'.site_url().'"><img style="margin: 10px 0px; width: 100%;" src="'. ARLO_API_PLUGIN_URL .'assets/email/header.jpg"/></a> <p style="margin: 0 20px; text-align: left">Dear '. ucwords($this->first_name) .',</p><br><p style="margin: 0 20px; text-align: left;">Thank you for registering with Team Teach. In order to fully access you account you will need to verify your email address. Please click the link below to complete the verification process.</p><br><div><!--[if mso]>
  <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="' . $url . '" style="height:40px;v-text-anchor:middle;width:200px;" stroke="f" fillcolor="#9EB934">
    <w:anchorlock/>
    <center>
  <![endif]-->
      <a href="' . $url . '"
style="background-color:#9EB934;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;">VERIFY ACCOUNT</a>
  <!--[if mso]>
    </center>
  </v:rect>
<![endif]--></div> <br><p style="margin: 0 20px; text-align: left;">Kind Regards,<br><br>Team Teach</p><br><br><table cellpadding="0" cellspacing="0" width="100%" bgcolor="#002b56" style="padding: 15px;"> <tr> <td> <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#002b56"> <tbody> <tr> <td style="vertical-align:middle;"><img align="left" width="38px" height="38px" style="margin:0px 0px 0px 15px" src="'. ARLO_API_PLUGIN_URL .'assets/email/phn_ico.png" alt="Phone icon"></td><td style="text-align: left; vertical-align: middle;" width="100%"> <p><a href="tel:+44 (0) 20 3746 0937" style="color: #FFFFFF; text-decoration:none; font-size: 13px;">+44 (0) 20 3746 0938</a></p></td></tr><tr> </tr></tbody> </table> <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#002b56"> <tbody> <tr> <td style="vertical-align:middle;"><img align="left" width="38px" height="38px" style="margin:0px 0px 0px 15px" src="'. ARLO_API_PLUGIN_URL .'assets/email/hs_ico.png" alt="House icon"></td><td style="text-align: left; vertical-align: middle; color:#000000" width="100%"> <p style="color: #FFFFFF; font-size: 13px;">5th Floor, Longbow House, 20 Chiswell Street, London, EC1Y 4TW</p></td></tr><tr> </tr></tbody> </table> <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#002b56"> <tbody> <tr> <td style="vertical-align:middle;"><img align="left" width="38px" height="38px" style="margin:0px 0px 0px 15px" src="'. ARLO_API_PLUGIN_URL .'assets/email/pntr_ico.png" alt="Cursor icon"></td><td style="text-align: left; vertical-align: middle;" width="100%"> <p><a href="https://www.teamteach.co.uk" style="color: #FFFFFF; text-decoration:none; font-size: 13px; font-weight: bold;">www.teamteach.co.uk</a></p></td></tr><tr> </tr></tbody> </table> <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#002b56"> <tbody> <tr> <td style="text-align: left; vertical-align: middle;" width="100%"> <p style="color: #ffffff; font-size: 11px; margin: 10px 0 0 25px;">VAT Number: 753505339 (GB) Registered in England No: 03770582</p></td></tr><tr> </tr></tbody> </table> </td></tr></table> </td></tr></tbody> </table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr></table><!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]--></body></html>';
                        $headers = ['Content-Type: text/html; charset=UTF-8'];
                        wp_mail($wordPressEmail, 'Team Teach - Verify Account', $body, $headers);

                        $admin_body = '<p>A new user has registered and successfully linked their details with Arlo, their details are below</p><br><p><strong>User details:</strong></p><p><strong>Name:</strong> '. $user->Contact->FirstName .' ' . $user->Contact->LastName .'</p><p><strong>Email:</strong> '. $user->Contact->Email .'</p><p><strong>Certificate Level:</strong> '. $user->Certificate->Level .'</p><p><strong>Certificate Expiry:</strong> '. $user->Certificate->Expiry .'</p>';
                        wp_mail(ARLO_ADMIN_EMAIL, 'New user notification', $admin_body, $headers);

                        $response->success = 'Your account has been created, please check your email to verify your account.';
                        $response->level = $user->Certificate->Level;
                    }
                    else
                    {
                        $response->error = 'There has been a problem trying to register your account. Please contact Team Teach to resolve this problem.';
                    }

                }
                else
                {
                    $response->error = 'This email already has an account associated with it. Please try resetting your password, or contact Team Teach if you think this is incorrect.';
                }
            }
            else
            {
                $response->error = 'Sorry, we have not been able to find your details, please contact Team Teach if you think this is incorrect.';
                $admin_body = '<p>A user has  tried to register, however their details could not be matched in Arlo. The users attempted registration details are below</p><br><p><strong>User details:</strong></p><p><strong>Name:</strong> '. $this->first_name .' ' . $this->last_name .'</p><p><strong>Email:</strong> '. $wordPressEmail .'</p>';
                wp_mail(ARLO_ADMIN_EMAIL, 'Failed user registration', $admin_body, 'Content-type: text/html');
            }
        }

        return $response;
    }

    public function resendVerificationEmail($user_id)
    {
        $response = new Response('login');
        $user = get_user_by('ID', $user_id);
        $name = get_user_meta($user_id, 'first_name', true);

        $verification_code = md5(time() . wp_generate_uuid4());
        $string = [
            'user_id' => $user_id,
            'code' => $verification_code
        ];
        update_user_meta($user_id, 'activation_code', $verification_code);

        $url = get_site_url() . '/my-account/?act=' . base64_encode(serialize($string));
        $body = '<html><head> <meta charset="UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <meta http-equiv="X-UA-Compatible" content="ie-edge"> <title></title> <style>*{font-family: "Helvetica Neue", Helvetica, "Segoe UI", Arial, sans-serif;}body{margin: 0px !important; padding: 0px !important;}@media only screen and (max-width:480px){.wrapper{width: 100% !important;}}</style></head><body><!--[if (gte mso 9)|(IE)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff" style="background-color:#ffffff;"> <tr> <td align="center" valign="top"><![endif]--><table align="center" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff"> <tr> <td align="center" valign="top"><!--[if (gte mso 9)|(IE)]> <table align="center" border="0" cellspacing="0" cellpadding="0" width="600"> <tr> <td align="center" valign="top" width="600"><![endif]--> <table border="0" cellpadding="0" cellspacing="0" width="600" class="wrapper"> <tbody> <tr> <td align="center" valign="top"> <a href="'.site_url().'"><img style="margin: 10px 0px; width: 100%;" src="'. ARLO_API_PLUGIN_URL .'assets/email/header.jpg"/></a> <p style="margin: 0 20px; text-align: left">Dear '. ucwords($name) .',</p><br><p style="margin: 0 20px; text-align: left;">Thank you for registering with Team Teach. In order to fully access you account you will need to verify your email address. Please click the link below to complete the verification process.</p><br><div><!--[if mso]>
  <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="' . $url . '" style="height:40px;v-text-anchor:middle;width:200px;" stroke="f" fillcolor="#9EB934">
    <w:anchorlock/>
    <center>
  <![endif]-->
      <a href="' . $url . '"
style="background-color:#9EB934;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;">VERIFY ACCOUNT</a>
  <!--[if mso]>
    </center>
  </v:rect>
<![endif]--></div> <br><p style="margin: 0 20px; text-align: left;">Kind Regards,<br><br>Team Teach</p><br><br><table cellpadding="0" cellspacing="0" width="100%" bgcolor="#002b56" style="padding: 15px;"> <tr> <td> <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#002b56"> <tbody> <tr> <td style="vertical-align:middle;"><img align="left" width="38px" height="38px" style="margin:0px 0px 0px 15px" src="'. ARLO_API_PLUGIN_URL .'assets/email/phn_ico.png" alt="Phone icon"></td><td style="text-align: left; vertical-align: middle;" width="100%"> <p><a href="tel:+44 (0) 20 3746 0937" style="color: #FFFFFF; text-decoration:none; font-size: 13px;">+44 (0) 20 3746 0938</a></p></td></tr><tr> </tr></tbody> </table> <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#002b56"> <tbody> <tr> <td style="vertical-align:middle;"><img align="left" width="38px" height="38px" style="margin:0px 0px 0px 15px" src="'. ARLO_API_PLUGIN_URL .'assets/email/hs_ico.png" alt="House icon"></td><td style="text-align: left; vertical-align: middle; color:#000000" width="100%"> <p style="color: #FFFFFF; font-size: 13px;">5th Floor, Longbow House, 20 Chiswell Street, London, EC1Y 4TW</p></td></tr><tr> </tr></tbody> </table> <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#002b56"> <tbody> <tr> <td style="vertical-align:middle;"><img align="left" width="38px" height="38px" style="margin:0px 0px 0px 15px" src="'. ARLO_API_PLUGIN_URL .'assets/email/pntr_ico.png" alt="Cursor icon"></td><td style="text-align: left; vertical-align: middle;" width="100%"> <p><a href="https://www.teamteach.co.uk" style="color: #FFFFFF; text-decoration:none; font-size: 13px; font-weight: bold;">www.teamteach.co.uk</a></p></td></tr><tr> </tr></tbody> </table> <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#002b56"> <tbody> <tr> <td style="text-align: left; vertical-align: middle;" width="100%"> <p style="color: #ffffff; font-size: 11px; margin: 10px 0 0 25px;">VAT Number: 753505339 (GB) Registered in England No: 03770582</p></td></tr><tr> </tr></tbody> </table> </td></tr></table> </td></tr></tbody> </table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </td></tr></table><!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]--></body></html>';



        $headers = ['Content-Type: text/html; charset=UTF-8'];
        $success = wp_mail($user->user_email, 'Team Teach - Verify Account', $body, $headers);
        $response->success = 'Verification email has been resent, please check your inbox to verify your account';
        return $response;
    }

    public function verifyAccount($package)
    {
        $response = new Response();
        $data = unserialize(base64_decode($package));

        if(array_key_exists('user_id', $data) && array_key_exists('code', $data))
        {
            $code = get_user_meta($data['user_id'], 'activation_code', true);
            if(!empty($code))
            {
                if ($code == $data['code'])
                {
                    update_user_meta($data['user_id'], 'account_activated', 1);
                    delete_user_meta($data['user_id'], 'activation_code');
                    $response->success = 'Account verified, click <a href="/login-register">here</a> to be redirected to the login page.';
                }
                else
                {
                    $response->error = 'Invalid link, please click the button below to send a new email and try again.';
                }
            }
            else
            {
                $response->error = 'This account has already been verified.';
            }
        }
        else
        {
            $response->error = 'Invalid link, please click the button below to send a new email and try again.';
        }

        return $response;
    }

    public function login()
    {
        $response = new Response('login');

        if(is_array($this->validated))
        {
            $response->error = 'Please check the error messages below.';
            $response->validation = $this->validated;
        }
        else
        {
            $wordPressEmail = ($this->useSafeEmail) ? $this->safeEmail : $this->email;
            $credentials = [
                'user_login'    => $wordPressEmail,
                'user_password' => $this->password,
                'remember'      => true
            ];

            $user = wp_signon($credentials);

            if(!is_wp_error($user))
            {
                if(!get_user_meta($user->ID, 'account_activated', true))
                {
                    $response->error = 'You need to verify your account before you can login, please check you inbox. Or click <a href="/login-register?verification=resend&user='.$user->ID.'">here</a> to resend a verification email';
                    wp_logout();
                }
                else
                {
                    wp_set_current_user($user->ID);
                    $arlo_user_id = get_user_meta($user->ID, 'arlo_user_id', true);
                    $arlo_user = $this->authClient->getContactById($arlo_user_id);

                    if ($arlo_user)
                    {
                        if ($this->userMatchesArlo($arlo_user))
                        {
                            update_user_meta($user->ID, 'certificate_number', $arlo_user->Certificate->Number);
                            update_user_meta($user->ID, 'certificate_level', $arlo_user->Certificate->Level);
                            update_user_meta($user->ID, 'certificate_expiry', $arlo_user->Certificate->Expiry);
                            $response->level = $arlo_user->Certificate->Level;
                            if($_GET['redirect_to'] && !empty($_GET['redirect_to']))
                            {
                                wp_redirect(urldecode($_GET['redirect_to']));
                                exit();
                            }

                            $permissions = ArloApiSingleton::getUserPermissions($user->ID);

                            if($permissions === 1)
                            {
                                wp_redirect('/my-account/useful-resources?login=success');
                                exit();
                            }
                            else
                            {
                                wp_redirect('/my-account?login=success');
                                exit();
                            }


                        }
                        else
                        {
                            wp_logout();
                            $response->error = 'There has been a problem verifying your account, and we are unable to log you in at this time. Please contact Team Teach to resolve this problem.';
                        }
                    }
                    else
                    {
                        wp_logout();
                        $response->error = 'There has been a problem finding your account, and we are unable to log you in at this time. Please contact Team Teach to resolve this problem.';
                    }
                }
            }
            else
            {
                if(array_key_exists('incorrect_password', $user->errors) || array_key_exists('invalid_email', $user->errors))
                {
                    $response->error = 'Email or password is incorrect, you can try resetting your password below via the forgotten password link. Or contact Team Teach if you think this is incorrect.';
                }
                else
                {
                    $response->error = 'There has been a problem while trying to login, please try again later.';
                }
            }

        }

        return $response;
    }

    public function userMatchesArlo($arloUser)
    {
        $matches = true;

        //if($this->first_name != $arloUser->Contact->FirstName) $matches = false;
        //if($this->last_name != $arloUser->Contact->LastName) $matches = false;
        if(strtolower(stripslashes($this->email)) != strtolower($arloUser->Contact->Email)) $matches = false;

        return $matches;
    }


}