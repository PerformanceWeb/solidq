<?php

namespace ArloAPI\Auth;

class Client
{
    protected $credentials;
    protected $url;
    protected $ssl;
    private static $base_url = ARLO_AUTH_API_BASE_URL;

    public function __construct($portal, $username, $password, $ssl = true)
    {
        $this->credentials = $username . ':' . $password;
        $this->url = 'https://' . $portal . '.arlo.co' . self::$base_url;
        $this->ssl = $ssl;
    }

    private function constructQueryString($params)
    {
        $url = '';
        $loop = count($params);
        $i = 1;
        foreach ($params as $key => $item)
        {
            $loop2 = count($item);
            $url .= $key . ' ';
            $i2 = 1;
            foreach ($item as $key2 => $item2)
            {
                $and = ($i2 != $loop2) ? "and " : "";
                $item2 = (is_string($item2) ? "'" . $item2 . "'" : $item2);
                $url .= $key2 . " " . $item2 . " " .  $and;
                $i2++;
            }
            if($i != $loop) $url .= "and ";
            $i++;
        }

        return rawurlencode(rtrim($url));
    }

    private function curlRequest($endpoint, $query = '')
    {
        $url = $this->url . $endpoint . $query;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $this->credentials);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->ssl);
        $response = curl_exec($ch);
        if(curl_errno($ch))
        {
            curl_close($ch);
            throw new \Exception(curl_error($ch));
        }
        curl_close($ch);
        return $response;
    }

    public function Contacts($params = [])
    {
        try 
        {
            return $this->curlRequest('contacts?expand=Contact&filter=', $this->constructQueryString($params));
        }
        catch(\Exception $e)
        {
            return 'Sorry there has been a problem trying to verify your details at this time, please try again later';
        }

    }

    public function getContactById($id)
    {
        try
        {
            $params = [
                'ContactID' => [
                    'eq'    => (int)$id
                ]
            ];
            $filter = $this->constructQueryString($params);

            $user = new \SimpleXMLElement($this->curlRequest('contacts?expand=contact,contact/customfields&filter=', $filter));

            if(!is_null($user) && !empty($user->Link[0]->Contact) && !is_null($user->Link[0]->Contact->Link[1]->CustomFields) && !empty($user->Link[0]->Contact->Link[1]->CustomFields))
            {
                return $this->mapUserToObject($user);
            }
            else
            {
                return false;
            }
        }
        catch(\Exception $e)
        {
            return 'Sorry there has been a problem trying to verify your details at this time, please try again later';
        }
    }

    public function getContactForVerification($fist_name, $last_name, $email)
    {
        try
        {
            $params = [
                'FirstName' => [
                    'eq'    => $fist_name
                ],
                'LastName' => [
                    'eq'    => $last_name
                ],
                'Email' => [
                    'eq'    => $email
                ]
            ];

            $filter = $this->constructQueryString($params);

            $user = new \SimpleXMLElement($this->curlRequest('contacts?expand=contact,contact/customfields&filter=', $filter));

            if(!is_null($user) && !empty($user->Link[0]->Contact) && !is_null($user->Link[0]->Contact->Link[1]->CustomFields) && !empty($user->Link[0]->Contact->Link[1]->CustomFields))
            {
                return $this->mapUserToObject($user);
            }
            else
            {
                return false;
            }
        }
        catch(\Exception $e)
        {
            return 'Sorry there has been a problem trying to verify your details at this time, please try again later';
        }
    }

    private function mapUserToObject($user)
    {
        $certificateNumber = false;
        $certificateExpiry = false;
        $certificateLevel = false;
        foreach ($user->children()->Link->children()->Contact->Link[1]->CustomFields->children() as $certificateDetail)
        {
            if($certificateDetail->Name == 'licensenumber') $certificateNumber = (string)$certificateDetail->Value->String;
            if($certificateDetail->Name == 'certificateexpirydate') $certificateExpiry = (string)$certificateDetail->Value->Date;
            if($certificateDetail->Name == 'levelofcertification2') $certificateLevel = (string)$certificateDetail->Value->String;
        }

        if($certificateNumber && $certificateExpiry && $certificateLevel)
        {
            return (object)[
                'Contact' => (object)[
                    'ID'        => (string)$user->Link[0]->Contact->ContactID,
                    'FirstName' => (string)$user->Link[0]->Contact->FirstName,
                    'LastName'  => (string)$user->Link[0]->Contact->LastName,
                    'Email'     => (string)$user->Link[0]->Contact->Email
                ],
                'Certificate'   => (object)[
                    'Number'    => $certificateNumber,
                    'Expiry'    => $certificateExpiry,
                    'Level'     => $certificateLevel
                ]
            ];
        }
        else
        {
            return false;
        }
    }

    public function getCustomFieldsForResource($resource, $resourceId)
    {
        return new \SimpleXMLElement($this->curlRequest($resource.'/'.$resourceId.'/customfields'));
    }
}