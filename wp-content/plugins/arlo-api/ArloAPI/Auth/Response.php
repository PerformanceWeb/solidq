<?php

namespace ArloAPI\Auth;

class Response
{
    public $form;
    public $error;
    public $success;
    public $validation;
    public $level;

    /**
     * Response constructor.
     * @param $form
     * @param $error
     * @param $success
     * @param $validation
     * @param $level
     */
    public function __construct($form = null, $error = null, $success = null, $validation = null, $level = null)
    {
        $this->form = $form;
        $this->error = $error;
        $this->success = $success;
        $this->validation = $validation;
        $this->level = $level;
    }
}