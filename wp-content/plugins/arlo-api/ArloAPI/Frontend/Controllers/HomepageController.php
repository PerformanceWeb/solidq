<?php

namespace ArloAPI\Frontend\Controllers;

use ArloAPI;
use ArloAPI\Client;

class HomepageController {

    private $client;
    private $region;
    private $regionList = [
        'us' => 'United States',
        'uk' => 'United Kingdom',
        'at' => 'Austria',
        'se' => 'Sweden',
        'no' => 'Norway',
        'it' => 'Italy',
        'de' => 'Germany',
        'dk' => 'Denmark',
        'sp' => 'Spain'
    ];
    public $timeFormat;

    public function __construct($region)
    {
        $transport = new ArloAPI\Transports\Wordpress();
        $transport->setRequestTimeout(30);
        $transport->setUseNewUrlStructure(get_option('arlo_new_url_structure') == 1);
        $this->client = new Client('lucient', $transport, ARLO_API_PLUGIN_VERSION);

        $this->region = $region;
        $this->timeFormat = (in_array($this->region, ['us']) || is_null($this->region) || $this->region == '' ? 'Time12Hr' : 'Time');
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function getCategories($parentId, $fields = ['CategoryId', 'ParentCategoryId', 'Name', 'Description', 'Footer'])
    {
        return $this->client->Categories()->getCategoriesByParentId($fields, $parentId);
    }

    public function getUpcomingEvents($dateIntervals, $limit, $fields = ['EventID,', 'EventTemplateID', 'Name', 'StartDateTime', 'EndDateTime', 'Location', 'PlacesRemaining', 'IsFull', 'AdvertisedOffers', 'RegistrationInfo', 'EndTimeZoneAbbr', 'Description'])
    {
        if($this->region === 'us' || $this->region !== 'at')
        {
            $allUpcomingEvents = [];
            $found = false;
            foreach ($dateIntervals as $dateInterval)
            {
                $upcomingEvents = $this->client->EventSearch()->search(null, null, null, $fields, $limit, 0, null, $this->region, null, null);
                if($upcomingEvents->Count > 1)
                {
                    $found = true;
                    break;
                }
            }

            if(!$found)
                $upcomingEvents = $this->client->EventSearch()->search(null, null, null, $fields, $limit, 0, null, $this->region, null, null);

            if($upcomingEvents->Count > 0)
            {
                foreach ($upcomingEvents->Items as $upcomingEvent)
                {
                    $upcomingEvent->Region = $this->region;
                }
                $allUpcomingEvents = $upcomingEvents->Items;
                usort($allUpcomingEvents, function ($a, $b) {
                    if ($a->StartDateTime == $b->StartDateTime)
                        return 0;
                    return ($a->StartDateTime < $b->StartDateTime) ? -1 : 1;
                });
            }
            return $allUpcomingEvents;
        }
        else
        {
            $allUpcomingEvents = [];
            foreach ($this->regionList as $id => $region)
            {
                $found = false;
                foreach ($dateIntervals as $dateInterval)
                {
                    $upcomingEvents = $this->client->EventSearch()->search(null, null, null, $fields, null, null, null, $id, null, null);
                    if($upcomingEvents->Count > 0)
                    {
                        $found = true;
                        foreach ($upcomingEvents->Items as $event)
                        {
                            $event->Region = $id;
                            array_push($allUpcomingEvents, $event);
                        }
                        break;
                    }
                }
                if(!$found)
                {
                    $upcomingEvents = $this->client->EventSearch()->search(null, null, null, $fields, null, null, null, $id, null, null);
                    if($upcomingEvents->Count > 0)
                    {
                        $found = true;
                        foreach ($upcomingEvents->Items as $event)
                        {
                            $event->Region = $id;
                            array_push($allUpcomingEvents, $event);
                        }
                    }
                }
            }
            if(!empty($allUpcomingEvents))
            {
                usort($allUpcomingEvents, function ($a, $b) {
                    if ($a->StartDateTime == $b->StartDateTime)
                        return 0;
                    return ($a->StartDateTime < $b->StartDateTime) ? -1 : 1;
                });
                $allUpcomingEvents = array_slice($allUpcomingEvents, 0, $limit);
                usort($allUpcomingEvents, function ($a, $b) {
                    if ($a->StartDateTime == $b->StartDateTime)
                        return 0;
                    return ($a->StartDateTime < $b->StartDateTime) ? -1 : 1;
                });
            }
            return $allUpcomingEvents;
        }
    }

    public function getHighlightedEvents($limit, $limitPerRegion, $tags, $fields = ['EventID,', 'EventTemplateID', 'Name', 'StartDateTime', 'EndDateTime', 'Location', 'PlacesRemaining', 'IsFull', 'AdvertisedOffers', 'RegistrationInfo', 'EndTimeZoneAbbr'])
    {
        if($this->region === 'us' || $this->region !== 'at')
        {
            $allHighlightedEvents = [];
            $highlightedEvents = $this->client->EventSearch()->search(null, null, null, $fields, null, null, null, $this->region, null, $tags);
            if ($highlightedEvents->Count < 1)
                $highlightedEvents = $this->client->EventSearch()->search(null, null, null, $fields, $limit, 0, null, $this->region);

            if($highlightedEvents->Count > 0)
            {
                foreach ($highlightedEvents->Items as $highlightedEvent)
                {
                    $highlightedEvent->Region = $this->region;
                }
                $allHighlightedEvents = $highlightedEvents->Items;
            }

            usort($allHighlightedEvents, function ($a, $b) {
                if ($a->StartDateTime == $b->StartDateTime)
                    return 0;
                return ($a->StartDateTime < $b->StartDateTime) ? -1 : 1;
            });

            return $allHighlightedEvents;

        }
        else
        {
            $allHighlightedEvents = [];
            foreach ($this->regionList as $id => $region)
            {
                $highlightedEvent = $this->client->EventSearch()->search(null, null, null, $fields, $limitPerRegion, null, null, $id, null, $tags);
                if($highlightedEvent->Count > 0)
                {
                    foreach ($highlightedEvent->Items as $event)
                    {
                        $event->Region = $id;
                        array_push($allHighlightedEvents, $event);
                    }
                }
            }

            usort($allHighlightedEvents, function ($a, $b) {
                if ($a->StartDateTime == $b->StartDateTime)
                    return 0;
                return ($a->StartDateTime < $b->StartDateTime) ? -1 : 1;
            });

            return $allHighlightedEvents;
        }
    }
}