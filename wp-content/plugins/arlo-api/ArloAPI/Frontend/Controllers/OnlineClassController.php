<?php

namespace ArloAPI\FrontEnd\Controllers;

use ArloAPI;
use ArloAPI\Client;

class OnlineClassController
{
    private $client;
    private $eventId;
    private $region;
    private $regionList = [
        'us' => 'United States',
        'uk' => 'United Kingdom',
        'at' => 'Austria',
        'se' => 'Sweden',
        'no' => 'Norway',
        'it' => 'Italy',
        'de' => 'Germany',
        'dk' => 'Denmark',
        'sp' => 'Spain'
    ];

    public function getRegion()
    {
        return $this->region;
    }

    public function __construct($urlRegion, $sessionRegion, $standardRegion, $eventId)
    {
        $transport = new ArloAPI\Transports\Wordpress();
        $transport->setRequestTimeout(30);
        $transport->setUseNewUrlStructure(get_option('arlo_new_url_structure') == 1);
        $this->client = new Client('lucient', $transport, ARLO_API_PLUGIN_VERSION);

        $this->eventId = $eventId;
        if($urlRegion)
        {
            $this->region = $urlRegion;
        }
        elseif($sessionRegion)
        {
            $this->region = $sessionRegion;
        }
        else
        {
            $this->region = $standardRegion;
        }
    }

    public function getEvent($fields = ['OnlineActivityID', 'Name', 'Summary', 'Description', 'TemplateID', 'DeliveryDescription', 'RegistrationInfo', 'AdvertisedOffers', 'Tags'])
    {
        return $this->client->OnlineActivitySearch()->search($this->eventId, null, $fields, null, null, null, $this->region)->Items[0];
    }

    public function getEventTemplate($eventTemplateId, $fields = ['TemplateID', 'Name', 'Code', 'RegisterInterestUri', 'RegisterPrivateInterestUri', 'Description', 'AdvertisedDuration', 'BestAdvertisedOffers', 'Tags'])
    {
        return $this->client->EventTemplates()->getTemplate($eventTemplateId, $fields);
    }
}