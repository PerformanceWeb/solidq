<?php

namespace ArloAPI\FrontEnd\Controllers;

use ArloAPI;
use ArloAPI\Client;
use ArloApiSingleton;

class ClassController
{
    public $timeFormat;
    public $bookButtonText;
    public $bookButtonStyle;
    private $client;
    private $eventId;
    private $region;
    private $websiteRegion;
    private $regionList = [
        'us' => 'United States',
        'uk' => 'United Kingdom',
        'at' => 'Austria',
        'se' => 'Sweden',
        'no' => 'Norway',
        'it' => 'Italy',
        'de' => 'Germany',
        'dk' => 'Denmark',
        'sp' => 'Spain'
    ];

    public function getRegion()
    {
        return $this->region;
    }

    public function __construct($urlRegion, $sessionRegion, $standardRegion, $eventId)
    {
        $transport = new ArloAPI\Transports\Wordpress();
        $transport->setRequestTimeout(30);
        $transport->setUseNewUrlStructure(get_option('arlo_new_url_structure') == 1);
        $this->client = new Client('lucient', $transport, ARLO_API_PLUGIN_VERSION);

        $this->eventId = $eventId;
        $this->websiteRegion = $standardRegion;
        if($urlRegion)
        {
            $this->region = $urlRegion;
        }
        elseif($sessionRegion)
        {
            $this->region = $sessionRegion;
        }
        else
        {
            $this->region = $standardRegion;
        }

        $this->timeFormat = (in_array($this->region, ['us']) || is_null($this->region) || $this->region == '' ? 'Time12Hr' : 'Time');
        $this->bookButtonText = (in_array($this->region, ['us']) || is_null($this->region) || $this->region == '' ? 'Request More Information' : 'Register');
        $this->bookButtonStyle = (in_array($this->region, ['us']) || is_null($this->region) || $this->region == '' ? 'style="font-size: 1em"' : '');
    }

    public function getEvent($fields = ['EventID,', 'EventTemplateID', 'Name', 'StartDateTime', 'EndDateTime', 'Location', 'PlacesRemaining', 'IsFull', 'AdvertisedOffers', 'RegistrationInfo', 'Tags', 'Presenters', 'Categories', 'StartTimeZoneAbbr', 'EndTimeZoneAbbr', 'SessionsDescription', 'Sessions', 'IsConfirmed'])
    {
        return $this->client->EventSearch()->search($this->eventId, null, null,$fields, null, null, null, $this->region)->Items[0];
    }

    public function getEventTemplate($eventTemplateId, $fields = ['TemplateID', 'Name', 'Code', 'RegisterInterestUri', 'RegisterPrivateInterestUri', 'Description', 'AdvertisedDuration', 'BestAdvertisedOffers', 'Tags', 'AdvertisedPresenters'])
    {
        return $this->client->EventTemplateSearch()->search($eventTemplateId, null, $fields, null, null, $this->websiteRegion)->Items[0];
    }

    public function getCustomField($event, $field, $fieldType, $default = null)
    {
        return ArloApiSingleton::getCustomField(ArloApiSingleton::get_auth_instance()->getCustomFieldsForResource('events', $event->EventID), $field, $fieldType, $default);
    }
}