<?php

namespace ArloAPI\FrontEnd\Controllers;

use ArloAPI;
use ArloAPI\Client;
use ArloApiSingleton;

class CourseController
{
    public $eLearning = false;
    private $client;
    private $templateId;
    private $region;
    private $regionList = [
        'us' => 'United States',
        'uk' => 'United Kingdom',
        'at' => 'Austria',
        'se' => 'Sweden',
        'no' => 'Norway',
        'it' => 'Italy',
        'de' => 'Germany',
        'dk' => 'Denmark',
        'sp' => 'Spain'
    ];

    public function __construct($region, $templateId)
    {
        $transport = new ArloAPI\Transports\Wordpress();
        $transport->setRequestTimeout(30);
        $transport->setUseNewUrlStructure(get_option('arlo_new_url_structure') == 1);
        $this->client = new Client('lucient', $transport, ARLO_API_PLUGIN_VERSION);

        $this->region = $region;
        $this->templateId = $templateId;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function getEventTemplate($fields = ['TemplateID', 'Name', 'Code', 'RegisterInterestUri', 'RegisterPrivateInterestUri', 'Description', 'AdvertisedDuration', 'BestAdvertisedOffers', 'Tags', 'AdvertisedPresenters'])
    {
        return $this->client->EventTemplateSearch()->search($this->templateId, null, $fields, null, null, $this->region)->Items[0];
    }

    public function getEvents($limit = 6, $fields = ['EventID,', 'EventTemplateID', 'Name', 'StartDateTime', 'StartTimeZoneAbbr', 'EndDateTime', 'Location', 'PlacesRemaining', 'IsFull', 'AdvertisedOffers', 'RegistrationInfo', 'Presenters'])
    {
        if($this->region === 'us' || $this->region !== 'at')
        {
            return $this->client->EventSearch()->search(null, $this->templateId, null, $fields, $limit, 0, null, $this->region)->Items;
        }
        else
        {
            $allEvents = [];
            foreach ($this->regionList as $id => $region)
            {
                $events = $this->client->EventSearch()->search(null, $this->templateId, null, $fields, $limit, 0, null, $id);
                if($events->Count > 0)
                {
                    foreach ($events->Items as $event)
                    {
                        if($id === 'us')
                        {
                            if($event->Location->Name !== 'Online')
                            {
                                if(!property_exists($event->Location, 'IsOnline'))
                                    continue;
                            }
                        }

                        $event->Region = $id;
                        array_push($allEvents, $event);
                    }
                }
            }

            if(!empty($allEvents))
            {
                usort($allEvents, function($a, $b) {
                    if ($a->StartDateTime == $b->StartDateTime)
                        return 0;
                    return ($a->StartDateTime < $b->StartDateTime) ? -1 : 1;
                });
            }

            return $allEvents;
        }
    }

    public function getOnlineEvents($limit = 6, $fields = ['OnlineActivityID', 'Name', 'Summary', 'Description', 'TemplateID', 'DeliveryDescription', 'RegistrationInfo', 'AdvertisedOffers'])
    {
        if($this->region === 'us' || $this->region !== 'at')
        {
            $allEvents = [];
            $events = $this->client->OnlineActivitySearch()->search(null, $this->templateId, $fields, $limit, 0, null, $this->region);
            if($events->Count > 0)
            {
                $this->eLearning = true;
                foreach ($events->Items as $event)
                {
                    $event->Region = $this->region;
                    array_push($allEvents, $event);
                }
            }


            return $allEvents;
        }
        else
        {
            $allEvents = [];
            foreach ($this->regionList as $id => $region)
            {
                $events = $this->client->OnlineActivitySearch()->search(null, $this->templateId, $fields, $limit, 0, null, $id);
                if($events->Count > 0)
                {
                    $this->eLearning = true;
                    foreach ($events->Items as $event)
                    {
                        $event->Region = $id;
                        array_push($allEvents, $event);
                    }
                }
            }

            return $allEvents;
        }
    }

    public function getPresenters($events)
    {
        $allPresenters = [];
        if(!empty($events))
        {
            foreach ($events as $event)
            {
                if($presenters = ArloApiSingleton::getCoursePresenters($event))
                {
                    foreach ($presenters as $presenter)
                    {
                        if(!in_array($presenter, $allPresenters))
                            array_push($allPresenters, $presenter);
                    }
                }
            }
        }
        return $allPresenters;
    }



}