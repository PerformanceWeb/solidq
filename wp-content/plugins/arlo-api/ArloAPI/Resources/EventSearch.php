<?php

namespace ArloAPI\Resources;

// load main Transport class for extending
require_once 'Resource.php';

// now use it
use ArloAPI\Resources\Resource;

class EventSearch extends Resource
{
	protected $apiPath = '/resources/eventsearch';

	public function search($event_id = null, $template_id = null, $category_id = null, $fields = array(), $count = 20, $skip=0, $group_by = null, $region = null, $facets = null, $tag_search = null, $dateMax = null) {
		$templates = $template_id;
		$categories = $category_id;
		$tags = $tag_search;
		$filter = '';
		
		if(is_array($templates)) {
			$templates = '[' . implode(',', $templates) . ']';
		}

        if(is_array($categories)) {
            $categories = '[' . implode(',', $categories) . ']';
        }

        if(is_array($tags)) {
            $tags = '[' . implode(',', $tags) . ']';
        }
		
		$data = array(
			'fields=' . implode(',', $fields),
			'top=' . $count,
			'includeTotalCount=true',
			'skip=' . $skip,
			'format=json'
		);
		
		if (!is_null($region)) {
			$data[] = 'region=' . $region;
		}
		
		if(!is_null($templates)) {
			$filter .= 'filter=templateid=' . $templates;
		}

        if(!is_null($event_id)) {
            if($filter === '')
            {
                $filter .= 'filter=eventID=' . $event_id;
            }
            else
            {
                $filter .= ',eventID=' . $event_id;
            }

        }

        if(!is_null($categories)) {
            if($filter === '')
            {
                $filter .= 'filter=templatecategoryid=' . $categories;
            }
            else
            {
                $filter .= ',templatecategoryid=' . $categories;
            }

        }

        if(!is_null($tags)) {
            if($filter === '')
            {
                $filter .= 'filter=tag=' . $tags;
            }
            else
            {
                $filter .= ',tag=' . $tags;
            }
        }

        if(!is_null($dateMax))
        {
            if($filter === '')
            {
                $filter .= 'filter=startmax=' . $dateMax;
            }
            else
            {
                $filter .= ',startmax=' . $dateMax;
            }
        }

        if($filter !== '') {
            $data[] = $filter;
        }

        if($facets) {
            $facetsSearch = 'facets=';
            foreach ($facets as $facet => $params)
            {
                $facetsSearch .= $facet;
                if(is_array($params))
                {
                    $facetsSearch .= '(';
                    foreach ($params as $key => $value)
                    {
                        $facetsSearch .= $key.'='.$value;
                    }
                    $facetsSearch .=')';
                }
                $facetsSearch .= ',';
            }
            $facetsSearch = rtrim($facetsSearch, ',');
            $data[] = $facetsSearch;
        }
				
		$results = $this->request(implode('&', $data));
		
		$ordered = [];
		
		if($group_by && in_array($group_by, $fields)) {
		
			foreach($results->Items as $result) {
				$key = $result->{$group_by};
			
				if(!isset($output[$key])) $output[$key] = [];
			
				$ordered[$key][] = $result;
			}
			
			$results->Items = $ordered;
		}
		
		return $results;
	}
	
	/* Helper functions */
	
	/**
	 * getEvent
	 *
	 * A wrapper for search() providing an easy way to return all events
	 *
	 * @param array $fields
	 *
	 * @return array
	 */
	/*public function getAllEvents($fields=array()) {
		return $this->search(null, $fields, 1000, null);
	}*/
	
	/**
	 * getAllEvents
	 *
	 * A wrapper for search() providing an easy way to return all events
	 *
	 * @param array $fields
	 *
	 * @return array
	 */
	public function getAllEvents($fields=array(), $regions = array()) {
		$maxCount = 100;
		
		if (!(is_array($regions) && count($regions))) {
			$regions = [''];
		}
		
		foreach($regions as $region) {
			$result = $this->search(null, $fields, $maxCount, null, 0, $region);
			
			$items[$region] = $result->Items;
			
			// get items over and above the max 100 imposed by the API
			// Dirty... but is a limitation of the public API
			if($result->TotalCount > $maxCount) {
				$iterate = ceil($result->TotalCount/$maxCount)-1;// we've already gone once - minus 1
				
				for($i=1;$i<=$iterate;$i++) {
					$items[$region] = array_merge($items[$region], $this->search(null, $fields, $maxCount, null, $i*$maxCount, $region)->Items);
				}
			}		
		}				
	
		
		return $items;
	}
	
	/**
	 * getAllEventsByTemplateIDs
	 *
	 * A wrapper for search() providing an easy way to return all events by template IDs
	 *
	 * @param array $ids
	 * @param array $fields
	 *
	 * @return array
	 */
	public function getAllEventsByTemplateIDs($ids, $fields=array()) {
		return $this->search($ids, $fields, 1000, null)->Items;
	}
}