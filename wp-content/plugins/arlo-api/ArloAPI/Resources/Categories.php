<?php

namespace ArloAPI\Resources;

// load main Transport class for extending
require_once 'Resource.php';

// now use it
use ArloAPI\Resources\Resource;

class Categories extends Resource
{
	protected $apiPath = '/resources/eventtemplatecategories/';
	
	public function search($fields = array(), $count = 20, $parent_ids = []) {
		$data = array(
			'fields=' . implode(',', $fields),
			'top=' . $count,
			'format=json'
		);

		if(!empty($parent_ids))
		{
		    array_push($data, implode(',', $parent_ids));
        }
		
		$results = $this->request(implode('&', $data));
		
		return $results;
	}

	public function getCategory($id, $fields=array()) {
		$this->__set('api_path', $this->apiPath . $id);

		$data = array(
            'fields=' . implode(',', $fields),
            'format=json'
        );

        $results = $this->request(implode('&', $data));
		
		return $results;
	}

	public function getAllCategories($fields=array()) {
		return $this->search($fields, 1000)->Items;
	}

    public function getAllCategoryIdsForParent($parent_id = false)
    {
        $categories = $this->getAllCategories(['CategoryId', 'ParentCategoryId']);
        $resultCategories = [];

        foreach ($categories as $key => $category)
        {
            if($parent_id === false)
            {
                if(!$category->ParentCategoryID)
                {
                    $resultCategories[] = $category->CategoryID;
                }
            }
            else
            {
                if($category->ParentCategoryID == $parent_id)
                {
                    $resultCategories[] = $category->CategoryID;
                }
            }
        }

        return $resultCategories;
	}

    public function getAllParentCategories($fields=array(), $parent_id = false, $allowed_categories=array())
    {
        $categories = $this->getAllCategories($fields);
        $resultCategories = [];
        $parent_id_array = [];

        if($categories)
        {
            foreach ($categories as $key => $category)
            {
                $parent_id_array[$category->ParentCategoryID][$category->CategoryID] = $category->Name;


                if($parent_id == false)
                {
                    if(!$category->ParentCategoryID)
                    {
                        $category->HasChildren = null;
                        $resultCategories[] = $category;
                    }
                }
                else
                {
                    if($category->ParentCategoryID == $parent_id)
                    {
                        $category->HasChildren = false;
                        $resultCategories[] = $category;
                    }
                }
            }

            foreach ($resultCategories as $resultCategory)
            {
                if(array_key_exists($resultCategory->CategoryID, $parent_id_array))
                {
                    $resultCategory->HasChildren = true;
                }
            }

            foreach ($resultCategories as $key => $resultCategory)
            {
                if(!empty($allowed_categories) && !in_array($resultCategory->CategoryID, $allowed_categories)) unset($resultCategories[$key]);
            }
        }
        return $resultCategories;
	}

    public function getCategoriesByParentId($fields=array(), $parent_id = 1)
    {
        $categories = $this->getAllCategories($fields);
        $resultCategories = [];
        $parent_id_array = [];

        if($categories)
        {
            foreach ($categories as $key => $category)
            {
                $parent_id_array[$category->ParentCategoryID][$category->CategoryID] = $category->Name;

                if($category->ParentCategoryID == $parent_id)
                {
                    $category->Children = null;
                    $resultCategories[] = $category;
                }
            }

            foreach ($resultCategories as $resultCategory)
            {
                if(array_key_exists($resultCategory->CategoryID, $parent_id_array))
                {
                    $resultCategory->Children = $parent_id_array[$resultCategory->CategoryID];
                }
            }
        }

        return $resultCategories;
	}

    public function getChildCategoriesByParentId($fields = array(), $parent_id = 1)
    {
        $categories = $this->getAllCategories($fields);
        $resultCategories = [];
        $parent_id_array = [];

        if($categories)
        {
            foreach ($categories as $key => $category)
            {
                $parent_id_array[$category->ParentCategoryID][$category->CategoryID] = $category->Name;

                if($category->CategoryID == $parent_id)
                {
                    $resultCategories['CategoryID'] = $category->CategoryID;
                    $resultCategories['Name'] = $category->Name;
                    $resultCategories['Children'] = [];
                }
            }

            if(array_key_exists($parent_id, $parent_id_array))
            {
                foreach ($parent_id_array[$parent_id] as $categoryId => $categoryName)
                {
                    $resultCategories['Children'][$categoryId] = $categoryName;
                    if (array_key_exists($categoryId, $parent_id_array))
                    {
                        foreach ($parent_id_array[$categoryId] as $categoryId2 => $categoryName2)
                        {
                            $resultCategories['Children'][$categoryId2] = $categoryName2;
                        }
                    }
                }
            }
        }

        return $resultCategories;
	}

    public function getCategoriesByParentIds($fields=array(), $parent_ids=array())
    {
        $resultCategories = [];

        foreach ($parent_ids as $parent_id)
        {
            $categories = $this->getCategoriesByParentId($fields, $parent_id);
            if($categories)
            {
                foreach ($categories as $category)
                {
                    array_push($resultCategories, $category);
                }
            }
        }

        return $resultCategories;
	}

    public function getParentCategory($fields=array(), $category_id = 1)
    {
        $categories = $this->getAllCategories($fields);
        $parent_category_id = false;

        if($categories)
        {
            foreach ($categories as $category)
            {
                if($category->CategoryID == $category_id)
                {
                    $parent_category_id = $category->ParentCategoryID;
                    break;
                }
            }

            if($parent_category_id)
            {
                foreach ($categories as $category)
                {
                    if($category->CategoryID == $parent_category_id)
                    {
                        return $category;
                    }
                }
            }
        }
	}

    public function getChildCategoriesForParentIds($fields, $parent_ids)
    {
        $categories = $this->search($fields, 1000, $parent_ids)->Items;

        if($categories)
        {
            foreach ($categories as $key => $category)
            {
                if(!in_array($category->ParentCategoryID, $parent_ids))
                {
                    unset($categories[$key]);
                }
            }

            foreach ($categories as $key => $category)
            {
                if(strpos(strtolower($category->Name), 'level one') !== false)
                {
                    $item = $categories[$key];
                    unset($categories[$key]);
                    break;
                }
            }

            if(isset($item))
            {
                array_unshift($categories, $item);
            }
        }

        return $categories;
	}
}