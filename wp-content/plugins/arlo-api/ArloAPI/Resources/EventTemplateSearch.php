<?php

namespace ArloAPI\Resources;

// load main Transport class for extending
require_once 'Resource.php';

// now use it
use ArloAPI\Resources\Resource;

class EventTemplateSearch extends Resource
{
	protected $apiPath = '/resources/eventtemplatesearch';

	public function search($template_id = null, $category_id = null, $fields = array(), $count = 20, $skip = 0, $region = null, $facets = null) {
	    $filter = '';
		$data = array(
			'fields=' . implode(',', $fields),
			'top=' . $count,
			'includeTotalCount=true',
			'skip=' . $skip,
			'format=json'
		);


        $templates = $template_id;
        if(is_array($templates)) {
            $templates = '[' . implode(',', $templates) . ']';
        }

        $categories = $category_id;
        if(is_array($categories)) {
            $categories = '[' . implode(',', $categories) . ']';
        }
		
		if (!empty($region)) {
			$data[] = 'region=' . $region;
		}

		if($facets) {
		    $facetsSearch = 'facets=';
            foreach ($facets as $facet => $params)
            {
                $facetsSearch .= $facet;
                if(is_array($params))
                {
                    $facetsSearch .= '(';
                    foreach ($params as $key => $value)
                    {
                        $facetsSearch .= $key.'='.$value;
                    }
                    $facetsSearch .=')';
                }
                $facetsSearch .= ',';
		    }
            $facetsSearch = rtrim($facetsSearch, ',');
            $data[] = $facetsSearch;
        }

        if(!is_null($templates)) {
            $filter = 'filter=templateid=' . $templates;
        }

        if(!is_null($categories)) {
            if($filter === '')
            {
                $filter = 'filter=categoryid=' . $categories;
            }
            else
            {
                $filter = ',categoryid=' . $categories;
            }
        }

        if($filter !== '') {
            $data[] = $filter;
        }
		
		$results = $this->request(implode('&', $data));
		
		return $results;
	}
	
	/* Helper functions */
	
	/**
	 * getAllEventTemplates
	 *
	 * A wrapper for search() providing an easy way to return all events
	 *
	 * @param array $fields
	 *
	 * @return array
	 */
	public function getAllEventTemplates($fields = array(), $regions = array()) {
		$maxCount = 20;
		
		if (!(is_array($regions) && count($regions))) {
			$regions = [''];
		}		
		
		foreach($regions as $region) {
			$result = $this->search(null, $fields, $maxCount, 0, $region);
				
			$items[$region] = $result->Items;
			
			// get items over and above the max 200 imposed by the API
			// Dirty... but is a limitation of the public API
			if($result->TotalCount > $maxCount) {
				$iterate = ceil($result->TotalCount/$maxCount)-1;// we've already gone once - minus 1
				
				for($i=1;$i<=$iterate;$i++) {
					$items[$region] = array_merge($items[$region], $this->search(null, $fields, $maxCount, $i*$maxCount, $region)->Items);
				}
			}
			
		}
		
		return $items;
	}

    public function getAllEventTemplatesBySortOrder($fields = array(), $regions = array())
    {
        $templates = $this->getAllEventTemplates($fields, $regions);

        if($templates)
        {
            foreach ($templates[''] as $template)
            {
                foreach ($template->Description->ContentFields as $contentField)
                {
                    if(str_replace(' ', '-', strtolower($contentField->FieldName)) == 'sort-order')
                    {
                        $template->SortOrder = strip_tags($contentField->Content->Text);
                        break;
                    }
                }
            }


            usort($templates[''], function($a, $b) {
                return $a->SortOrder <=> $b->SortOrder;
            });
        }
        return $templates;
	}
}