<?php

namespace ArloAPI\Resources;

// load main Transport class for extending
require_once 'Resource.php';

// now use it
use ArloAPI\Resources\Resource;

class EventTemplates extends Resource
{
    protected $apiPath = '/resources/eventtemplates/';

    public function getTemplate($id, $fields=array(), $region = null) {
        $this->__set('api_path', $this->apiPath . $id);

        $data = array(
            'fields=' . implode(',', $fields),
            'format=json'
        );

        $results = $this->request(implode('&', $data));

        return $results;
    }
}