<?php

namespace ArloAPI\Resources;

// load main Transport class for extending
require_once 'Resource.php';

// now use it
use ArloAPI\Resources\Resource;
use ArloAPI\Auth\Response;

class Registrations extends Resource
{
	protected $apiPath = '/resources/events/{EventID}/registrations/?region=uk';

	public function Register($request)
    {
        $response = new Response();
        $validation = $this->validateInput($request);

        if(is_array($validation))
        {
            $response->error = 'Please check the validation messages below';
            $response->validation = $validation;
        }
        else
        {
            $post_data = [
                'Contact' => [
                    'FirstName' => $this->data['FirstName'],
                    'LastName'  => $this->data['LastName'],
                    'Email'     => $this->data['Email'],
                    'Mobile'    => $this->data['Mobile']
                ],
                'Employment' => [
                    'OrganisationName'  => $this->data['OrganisationName'],
                    'Position'          => $this->data['Position']
                ],
                'Comments'  => $this->data['Comments']
            ];
            //$post_data = json_encode($post_data);

            $this->__set('api_path', str_replace('{EventID}', $request['EventID'], $this->apiPath));

            try
            {
                $success = $this->request(null, $post_data);

                if(is_object($success) && $success->EventID == $request['EventID'])
                {
                    $response->success = 'Thank you for registering your interest in this event';
                }
            }
            catch(\Exception $e)
            {
                $response->error = $e->getMessage();
            }
        }

        return $response;
    }

    private function validateInput($request)
    {
        $validated = true;
        if(isset($request['OrganisationName']))
        {
            $this->data['OrganisationName'] = trim(filter_var($request['OrganisationName'], FILTER_SANITIZE_STRING));
        }

        if(isset($request['FirstName']) && !empty($request['FirstName']))
        {
            $this->data['FirstName'] = trim(filter_var($request['FirstName'], FILTER_SANITIZE_STRING));
        }
        else
        {
            if(!is_array($validated)) $validated = [];
            $validated['FirstName'] = 'First Name is required';
        }

        if(isset($request['LastName']) && !empty($request['LastName']))
        {
            $this->data['LastName'] = trim(filter_var($request['LastName'], FILTER_SANITIZE_STRING));
        }
        else
        {
            if(!is_array($validated)) $validated = [];
            $validated['LastName'] = 'Last Name is required';
        }

        if(isset($request['Position']))
        {
            $this->data['Position'] = trim(filter_var($request['Position'], FILTER_SANITIZE_STRING));
        }

        if(isset($request['Mobile']) && !empty($request['Mobile']))
        {
            if(!is_numeric($request['Mobile']))
            {
                if(!is_array($validated)) $validated = [];
                $validated['Mobile'] = 'Must only contain numeric characters';
            }
            else
            {
                $this->data['Mobile'] = trim(filter_var($request['Mobile'], FILTER_SANITIZE_STRING));
            }
        }

        if(isset($request['Email']) && !empty($request['Email']))
        {
            if(!filter_var($request['Email'],FILTER_VALIDATE_EMAIL))
            {
                if(!is_array($validated)) $validated = [];
                $validated['Email'] = 'Must be a valid email address';
            }
            else
            {
                $this->data['Email'] = trim(filter_var($request['Email'], FILTER_SANITIZE_EMAIL));
            }
        }
        else
        {
            if(!is_array($validated)) $validated = [];
            $validated['Email'] = 'Email is required';
        }

        if(isset($request['Comments']))
        {
            $this->data['Comments'] = trim(filter_var($request['Comments'], FILTER_SANITIZE_STRING));
        }

        return $validated;
    }
}