<?php
/**
 * The WordPress Plugin Boilerplate.
 *
 * A foundation off of which to build well-documented WordPress plugins that
 * also follow WordPress Coding Standards and PHP best practices.
 *
 * @package   Arlo_API
 * @author    Kloc <enquiries@kloc.co.uk>
 * @license   GPL-2.0+
 * @link      https://kloc.co.uk
 *
 * @wordpress-plugin
 * Plugin Name:       Arlo API
 * Description:       Wrapper for Arlo's API
 * Version:           1.0
 * Author:            Kloc
 * Author URI:        https://kloc.co.uk
 * Text Domain:       arlo-api
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

/*----------------------------------------------------------------------------*
 * Constants
 *----------------------------------------------------------------------------*/

define('ARLO_API_PLUGIN_PREFIX', 'arlo-api');
define('ARLO_API_PLUGIN_NAME', 'Arlo API');
define('ARLO_API_PLUGIN_DIR', plugin_dir_path( __FILE__ ));
define('ARLO_API_PLUGIN_URL', plugin_dir_url( __FILE__ ));
define('ARLO_API_PLUGIN_VERSION', 1.0);
define('ARLO_AUTH_API_BASE_URL', '/api/2012-02-01/auth/resources/');
define('ARLO_AUTH_API_USERNAME', 'externalservices@lucient.com');
define('ARLO_AUTH_API_PASSWORD', '4p*@&T39jF4*pQ35yu5a');
define('ARLO_ADMIN_EMAIL', '');
define('ARLO_GOOGLE_API_KEY', '');
define('ARLO_POSTCODE_SEARCH', 'true');
define('ARLO_SEARCH_RADIUS', [2, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]);
define('ARLO_DEFAULT_COUNTRY', 'United Kingdom');
define('ARLO_COUNTRIES', [
    'United Kingdom'    => 'gb',
//    'Ireland'           => 'ie',
//    'France'            => 'fr',
//    'Italy'             => 'it',
//    'Germany'           => 'de',
//    'Malta'             => 'mt',
//    'Switzerland'       => 'ch',
//    'Australia'         => 'au',
//    'New Zealand'       => 'nz',
//    'China'             => 'cn',
//    'Hong Kong'         => 'hk',
//    'Canada'            => 'ca',
//    'USA'               => 'us',
//    'Falkland Islands'  => 'fk',
//    'Cayman Islands'    => 'ky',
//    'Jamaica'           => 'jm',
]);


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

// load API files
require_once(plugin_dir_path( __FILE__ ) . 'ArloAPI/Client.php');
require_once(plugin_dir_path( __FILE__ ) . 'ArloAPI/Auth/Client.php');
require_once(plugin_dir_path( __FILE__ ) . 'ArloAPI/Auth/User.php');
require_once(plugin_dir_path( __FILE__ ) . 'ArloAPI/Auth/Response.php');
require_once(plugin_dir_path( __FILE__ ) . 'ArloAPI/Transports/Wordpress.php');
require_once(plugin_dir_path( __FILE__ ) . 'ArloApiSingleton.php');