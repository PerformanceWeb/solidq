<?php

class CourseSearch
{
    private $category;
    private $course;
    private $date_from;
    private $date_to;
    private $location;

    /**
     * CourseSearch constructor.
     * @param $category
     * @param $course
     * @param $date_from
     * @param $date_to
     * @param $location
     */
    public function __construct($category = null, $course = null, $date_from = null, $date_to = null, $location = null)
    {
        $this->category = $category;
        $this->course = $course;
        $this->date_from = $date_from;
        $this->date_to = $date_to;
        $this->location = $location;
    }

    private function sanitizeInput()
    {
        if(!is_null($this->category)) $this->category = filter_var($this->category, FILTER_SANITIZE_NUMBER_INT);
        if(!is_null($this->course)) $this->course = filter_var($this->course,FILTER_SANITIZE_NUMBER_INT);
        if(!is_null($this->date_from)) $this->date_from = date('d/m/Y', $this->date_from);
        if(!is_null($this->date_to)) $this->date_to = date('d/m/Y', $this->date_to);
        //if(!is_null($this->location))
    }




}