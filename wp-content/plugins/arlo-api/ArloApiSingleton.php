<?php

class ArloApiSingleton
{
    protected static $instance = NULL;

    /**
     * Creates a new instance if there isn't one.
     *
     * @wp-hook init
     * @return object
     */
    public static function get_instance()
    {
        $transport = new ArloAPI\Transports\Wordpress();
        $transport->setRequestTimeout(30);
        $transport->setUseNewUrlStructure(get_option('arlo_new_url_structure') == 1);
        return new ArloAPI\Client('lucient', $transport, ARLO_API_PLUGIN_VERSION);

    }

    public static function get_auth_instance()
    {
        return new ArloAPI\Auth\Client('lucient', ARLO_AUTH_API_USERNAME, ARLO_AUTH_API_PASSWORD, false);
    }

    public static function newUserInstance($form_type = null, $first_name = null, $last_name = null, $email = null, $password = null, $password_confirm = null)
    {
        return new ArloAPI\Auth\User($form_type, $first_name, $last_name, $email, $password, $password_confirm);
    }


    public static function closedCategoryId($category)
    {
        if(!is_null($category->Children))
        {
            foreach ($category->Children as $key => $child)
            {
                if(strpos(strtolower($child), 'closed') !== false)
                {
                    return $key;
                }
            }
        }
    }

    public static function openCategoryId($category)
    {
        if(!is_null($category->Children))
        {
            foreach ($category->Children as $key => $child)
            {
                if(strpos(strtolower($child), 'open') !== false)
                {
                    return $key;
                }
            }
        }
    }

    public static function hasTemplateContentSection($template, $content_section)
    {
        $response = false;

        foreach ($template->Description->ContentFields as $contentField)
        {
            if(str_replace(' ', '-', strtolower($contentField->FieldName)) == $content_section)
            {
                $response = true;
                break;
            }
        }
        return $response;
    }

    public static function templateContentSection($template, $content_section, $strip_tags = false, $limit = false)
    {
        $contentSection = false;
        foreach ($template->Description->ContentFields as $contentField)
        {
            if(str_replace(' ', '-', strtolower($contentField->FieldName)) == $content_section)
            {
                $contentSection = ($strip_tags) ? strip_tags($contentField->Content->Text) : $contentField->Content->Text;

                if($limit !== false && strlen($contentSection) >= $limit)
                {
                    $pos = strpos($contentSection, ' ', $limit);
                    $contentSection = substr($contentSection, 0, $pos);
                }
                break;
            }
        }
        return $contentSection;
    }

    public static function getEventDescription($event, $strip_tags = false, $limit = false)
    {
        $description = ($strip_tags) ? strip_tags($event->Description->Text) : $event->Description->Text;
        if($limit !== false && strlen($description) >= $limit)
        {
            $pos = strpos($description, ' ', $limit);
            $description = substr($description, 0, $pos);
        }
        return $description;
    }

    public static function getRegisterInterestLink($template)
    {
        if(strtolower($template->Tags[0]) == 'open') return $template->RegisterInterestUri;
        if(strtolower($template->Tags[0]) == 'closed') return $template->RegisterPrivateInterestUri;
    }

    public static function getDateParts($date_string)
    {
        $date = new DateTime($date_string);
        return $dateParts = (object)[
            'Date'          => $date->format('d'),
            'Month'         => $date->format('M'),
            'FullMonth'         => $date->format('F'),
            'Year'          => $date->format('Y'),
            'Time'          => $date->format('H:i'),
            'Time12Hr'      => $date->format('h:ia'),
            'DayMonth'      => $date->format('dS M'),
            'DayMonthYear'  => $date->format('dS M Y'),
            'MonthDayYear'  => $date->format('M d, Y')
        ];
    }

    public static function getTimezoneText($timezoneAbbreviation)
    {
        switch ($timezoneAbbreviation)
        {
            case 'EST':
            case 'EDT':
                return 'Eastern time';
            case 'PST':
            case 'PDT':
                return 'Pacific time';
            case 'CST':
            case 'CT':
            case 'CDT':
                return 'Central time';
            default:
                return $timezoneAbbreviation;
        }
    }

    public static function getEventHeaderDate($date_string_start, $date_string_end)
    {
        $startDate = new DateTime($date_string_start);
        $endDate = new DateTime($date_string_end);
        if($startDate->format('M') === $endDate->format('M'))
        {
            return $startDate->format('M').' '.$startDate->format('d').' - '.$endDate->format('d').', '.$startDate->format('Y');
        }
        else
        {
            return $startDate->format('M d, Y').' - '.$endDate->format('M d, Y');
        }
    }

    public static function getEventDuration($start_date_string, $end_date_string)
    {
        $startDate = new DateTime($start_date_string);
        $endDate = new DateTime($end_date_string);

        return $endDate->diff($startDate)->format("%a");
    }

    public static function renderEventAddress($location, $type)
    {
        $address = '';

        if($type == 'short')
        {
            if($location->Suburb) $address .= $location->Suburb . ', ';
            $address .= $location->City;
        }

        if($type == 'long')
        {
            if($location->VenueName) $address .= $location->VenueName;
            if($location->StreetLine1) $address .= ', ' . $location->StreetLine1;
            if($location->Suburb) $address .= ', ' . $location->Suburb;
            if($location->State) $address .= ', ' . $location->State;
            if($location->PostCode) $address .= ', ' . $location->PostCode;
        }

        return $address;
    }

    public static function getTemplateCategoriesAsString($template)
    {
        $categories = '';
        if($template->Categories)
        {
            foreach ($template->Categories as $category)
            {
                $categories .= $category->CategoryID . ',';
            }

            $categories = rtrim($categories, ',');
        }

        return $categories;
    }

    public static function formatSearchData($request)
    {
        $searchData = (object)[];
        if(isset($request['templateCategoryId']) && !empty($request['templateCategoryId']))
        {
            $searchData->templateCategoryId = $request['templateCategoryId'];
        }

        if(isset($request['templateId']) && !empty($request['templateId']))
        {
            $searchData->templateId = $request['templateId'];
        }

        if(isset($request['templatePageId']) && !empty($request['templatePageId']))
        {
            $searchData->templatePageId = $request['templatePageId'];
        }

        if(isset($request['registerInterestLink']) && !empty($request['registerInterestLink']))
        {
            $searchData->registerInterestLink = $request['registerInterestLink'];
        }

        if(isset($request['templateName']) && !empty($request['templateName']))
        {
            $searchData->templateName = $request['templateName'];
        }

        if(isset($request['dates']) && !empty($request['dates']))
        {
            $dates = explode(' - ', $request['dates']);
            $searchData->startDate = $dates[0];
            $searchData->endDate = $dates[1];
        }

        if(isset($request['state']) && !empty($request['state']))
        {
            $searchData->state = '[' . implode(',', ARLO_REGIONS[$request['state']]) . ']';
        }

        if(isset($request['location_search']) && !empty($request['location_search']) && isset($request['location_country']) && !empty($request['location_country']))
        {
            $searchData->locationSearch = $request['location_search'] . ' ' . $request['location_country'];
        }

        if(isset($request['radius']) && !empty($request['radius']))
        {
            $searchData->radius = $request['radius'];
        }

        return $searchData;
    }

    /**
     * @param $user_id
     * @return int
     * minus return values indicate missing data or expired certificate
     * -1 missing level
     * -2 missing or expired date
     * 0 default (i.e. level not recognised)
     */
    public static function getUserPermissions($user_id)
    {
        $certifcate_level = get_user_meta($user_id, 'certificate_level', true);
        $certifcate_expiry = get_user_meta($user_id, 'certificate_expiry', true);


        if(!$certifcate_level)
        {
            return -1;
        }

        if(!$certifcate_expiry)
        {
            return -2;
        }

        $expiry = date('Y-m-d', strtotime($certifcate_expiry));
        $today = date('Y-m-d');

        if($expiry < $today)
        {
            return -2;
        }

        switch ($certifcate_level)
        {
            case 'Level One - 6 Hour':
            case 'Level Two - 12 Hour':
            case 'Advanced Modules' :
                return 1;
                break;
            case 'Intermediate Trainer' :
                return 2;
                break;
            case 'Advanced Trainer' :
                return 3;
                break;
            case 'Principal Trainer' :
                return 4;
                break;
            default :
                return 0;
        }
    }

    public static function errorClass($response, $field)
    {
        return ($response->validation[$field]) ? 'active' : '';
    }

    public static function getCountyIcon($countryCode)
    {
        return ARLO_API_PLUGIN_URL . 'assets/country_icons/'.$countryCode.'.svg';
    }

    public static function getSearchCategories($parentIds)
    {
        $searchCategories = [];
        $client = self::get_instance();

        foreach ($parentIds as $parentId)
        {
            $searchCategories[] = $client->Categories()->getChildCategoriesByParentId(['ParentCategoryId', 'CategoryId', 'Name'], $parentId);
        }

        // All of the positive behavior management courses need to be grouped into one category this is done here
        $positiveBehaviorManagementGroupedCategory = [];
        $positiveBehaviorManagementGroupedCategory['Name'] = 'Positive Behaviour Management';
        $positiveBehaviorManagementGroupedCategory['CategoryID'] = null;
        $positiveBehaviorManagementGroupedCategory['Children'] = [];

        foreach ($searchCategories as $key => $searchCategory)
        {
            if(strpos($searchCategory['Name'], 'Positive Behaviour Management') !== false)
            {
                $children = $searchCategory['Children'];
                $children[$searchCategory['CategoryID']] = $searchCategory['Name'];
                $merge = $positiveBehaviorManagementGroupedCategory['Children'] + $children;
                $positiveBehaviorManagementGroupedCategory['Children'] = $merge;
                unset($searchCategories[$key]);
            }
        }

        array_push($searchCategories, $positiveBehaviorManagementGroupedCategory);

        return $searchCategories;
    }

    public static function renderSearchCategories($search_categories, $data, $with_brackets = false)
    {
        $html = '';
        foreach ($search_categories as $search_category)
        {
            $value = '';
            $dataAttr = '';
            $label = $search_category['Name'];
            if(empty($search_category['Children']))
            {
                $value = $search_category['CategoryID'];
                $dataAttr = $search_category['CategoryID'];
            }
            else
            {
                $value .= '[';
                if($search_category['CategoryID'] != null)
                {
                    $value .= $search_category['CategoryID'] .',';
                    $dataAttr .= $search_category['CategoryID'] .',';
                }

                foreach ($search_category['Children'] as $id => $child)
                {
                    $value .= $id .',';
                    $dataAttr .= $id .',';
                }
                $value = rtrim($value, ',');
                $dataAttr = rtrim($dataAttr, ',');
                $value .= ']';
            }

            $selected = ($data['templateCategoryId'] == $value) ? 'selected' : '';
            $html .= '<option value="'.$value.'" data-categories="'.$dataAttr.'" '.$selected.'>'.$label.'</option>';
        }
        return $html;
    }

    public static function getPlacesRemainingLabelClasses($placesRemaining, $bgColour, $offers)
    {
        $classes = '';
        $earlyBird = false;
        foreach ($offers as $offer)
        {
            if ($offer->IsDiscountOffer)
            {
                $earlyBird = true;
                break;
            }
        }

        if(!is_null($placesRemaining))
        {
            if ($placesRemaining <= 2)
            {
                $classes .= 'label-type--places_two label-text--white';
            }
            elseif ($placesRemaining <= 10)
            {
                $classes .= 'label-type--places_ten';
                if ($bgColour === 'white')
                {
                    $classes .= ' label-text--black';
                }
                else
                {
                    $classes .= ' label-text--white';
                }
            }
        }
        else
        {
            if($earlyBird)
            {
                $classes .= 'label-type--offer_earlybird label-text--white';
            }
            else
            {
                $classes .= 'label-type--places_ten';
                if ($bgColour === 'white')
                {
                    $classes .= ' label-text--black';
                }
                else
                {
                    $classes .= ' label-text--white';
                }
            }

        }

        return $classes;
    }

    public static function getPlacesRemainingText($placesRemaining, $offers)
    {
        $earlyBird = false;
        foreach ($offers as $offer)
        {
            if ($offer->IsDiscountOffer)
            {
                $earlyBird = true;
                break;
            }
        }

        if($earlyBird)
            return $offer->Label ?: 'Early bird offer';

        if(!is_null($placesRemaining))
            return $placesRemaining.' Places left';

        return false;
    }

    public static function getCourseTagsByPrefix($event, $prefix)
    {
        $tags = [];

        if(empty($event->Tags))
            return false;

        foreach ($event->Tags as $tag)
        {
            $tagMatch = strtolower($tag);
            if(strpos($tagMatch, $prefix) !== false)
            {
                $tags[] = ucwords(ltrim($tagMatch, $prefix));
            }
        }
        return $tags;
    }

    public static function getCoursePresenters($event, $isTemplate = false)
    {
        $presenters = [];
        $key = [];
        $fieldName = ($isTemplate ? 'AdvertisedPresenters' : 'Presenters');
        if(!$event->{$fieldName} || empty($event->{$fieldName}))
            return false;

        $presentersArray = get_field('course_presenters', 'option');
        if(!empty($presentersArray))
        {
            foreach ($event->{$fieldName} as $eventPresenter)
            {
                foreach ($presentersArray as $presenter)
                {
                    if ($eventPresenter->Name == $presenter['name'] && (empty($key) || !array_key_exists($eventPresenter->Name, $key)))
                    {
                        $key[] = $eventPresenter->Name;
                        $presenters[] = (object)[
                            'Name'      => $presenter['name'],
                            'Position'  => $presenter['position'],
                            'Image'     => $presenter['image'],
                            'Bio'       => $presenter['description']
                        ];
                    }
                }
            }
        }
        return $presenters;
    }

    public static function getClassPriceString($offers, $region)
    {
        $offerPrice = '';
        $standardPrice = '';
        $standardPriceFloat = 0.00;
        $vatExclusive = (!in_array($region, ['us']) && !is_null($region) && $region != '');
        foreach ($offers as $offer)
        {
            if($offer->IsDiscountOffer)
            {
                if($vatExclusive)
                {
                    if(property_exists($offer->OfferAmount, 'TaxRate'))
                    {
                        $offerPrice = $offer->OfferAmount->CurrencyCode.' '.number_format($offer->OfferAmount->AmountTaxExclusive, 2).' <small class="no-strike">(excl. '.$offer->OfferAmount->TaxRate->ShortName.')</small>';
                    }
                    else
                    {
                        $offerPrice = $offer->OfferAmount->CurrencyCode.' '.number_format($offer->OfferAmount->AmountTaxExclusive, 2).' <small class="no-strike">('.getArloTaxName($region).' Free)</small>';
                    }

                }
                else
                {
                    $offerPrice = $offer->OfferAmount->FormattedAmountTaxInclusive;
                }
            }
            elseif($offer->OfferAmount->AmountTaxInclusive > $standardPriceFloat)
            {
                $standardPriceFloat = $offer->OfferAmount->AmountTaxInclusive;
                if($vatExclusive)
                {
                    if(property_exists($offer->OfferAmount, 'TaxRate'))
                    {
                        $standardPrice = $offer->OfferAmount->CurrencyCode.' '.number_format($offer->OfferAmount->AmountTaxExclusive, 2).' <small class="no-strike">(excl. '.$offer->OfferAmount->TaxRate->ShortName.')</small>';
                    }
                    else
                    {
                        $standardPrice = $offer->OfferAmount->CurrencyCode.' '.number_format($offer->OfferAmount->AmountTaxExclusive, 2).' <small class="no-strike">('.getArloTaxName($region).' Free)</small>';
                    }
                }
                else
                {
                    $standardPrice = $offer->OfferAmount->FormattedAmountTaxInclusive;
                }
            }
        }
        return ($offerPrice !== '') ? '<small>'.$standardPrice.'</small>'.' '.$offerPrice : $standardPrice;
    }

    public static function getCourseHeaderImage($courseName)
    {
        $images = get_field('course_images', 'option');
        foreach ($images as $image)
        {
            $name = $imageName = strtolower(str_replace([":", ",", "-", "'", "&", " "], '', $image['course_name']));
            $courseName = strtolower(str_replace([":", ",", "-", "'", "&", " "], '', $courseName));
            if($courseName === $name)
                return $image['image']['url'];
        }
    }

    public static function getTopicHeaderImage($topicName, $postId)
    {
        $images = get_field('header_background_images', $postId);
        foreach ($images as $image)
        {
            $name = $imageName = strtolower(str_replace([":", ",", "-", "'", "&", " "], '', $image['topic_name']));
            $topicName = strtolower(str_replace([":", ",", "-", "'", "&", " "], '', $topicName));
            if($topicName === $name)
                return $image['image']['url'];
        }
    }

    public static function getUniqueValuesFromArray($array, $key)
    {
        return array_unique(array_column($array, $key));
    }

    public static function getEventUrl($event, $region, $eLearning = false)
    {
        $siteSuffix = str_replace('/', '', get_blog_details()->path) ?: '';
        if($siteSuffix)
            $siteSuffix = '/'.$siteSuffix;
        return ($eLearning ? $siteSuffix.'/training/e-class/' : $siteSuffix.'/training/class/').($eLearning ? $event->OnlineActivityID : $event->EventID).'/'.strtolower(str_replace([' ', '&', ',', ':', '(', ')'], ['-', 'and', '', '', '', ''], $event->Name)).($region != '' && !is_null($region) ? '/'.$region : '');
    }

    public static function getTemplateUrl($template, $isEvent = false)
    {
        $siteSuffix = str_replace('/', '', get_blog_details()->path) ?: '';
        if($siteSuffix)
            $siteSuffix = '/'.$siteSuffix;

        if($isEvent)
            return $siteSuffix.'/training/course/'.$template->EventTemplateID.'/'.strtolower(str_replace([' ', '&', ',', ':', '(', ')'], ['-', 'and', '', '', '', ''], $template->Name));

        return $siteSuffix.'/training/course/'.$template->TemplateID.'/'.strtolower(str_replace([' ', '&', ',', ':', '(', ')'], ['-', 'and', '', '', '', ''], $template->Name));
    }

    public static function getTopicUrl($topic)
    {
        $siteSuffix = str_replace('/', '', get_blog_details()->path) ?: '';
        if($siteSuffix)
            $siteSuffix = '/'.$siteSuffix;

        return $siteSuffix.'/training/topic/'.$topic->CategoryID.'/'.strtolower(str_replace([' ', '&', ',', ':'], ['-', 'and', '', ''], $topic->Name));
    }

    public static function getEventLocation($location)
    {
        if(property_exists($location, 'IsOnline') || $location->Name == 'Online')
            return 'Live Online';

        $locationString = '';
        $locationString .= $location->City;
        if(property_exists($location, 'State'))
            $locationString .= ', '.$location->State;
        $locationString .= ', '.$location->Country;
        return $locationString;
    }

    public static function getCustomField($customFields, $field, $fieldType, $default = false)
    {
        if(!property_exists($customFields, 'Field'))
            return $default;

        foreach ($customFields->Field as $item)
        {
            if($item->Name == $field)
                return !empty($item->Value->{$fieldType}) ? $item->Value->{$fieldType} : $default;
        }

        return $default;
    }

    public static function getEventSearchDateIntervals($firstInterval, $increments, $iterations)
    {
        $intervals = [];
        $now = new DateTime();
        $now->setTime(11,30);
        $now->add(DateInterval::createFromDateString($firstInterval));
        $intervals[] = urlencode($now->format('Y-m-d\TH:i:s.0000000P'));

        for ($i = 1; $i <= $iterations; $i++)
        {
            $now->add(DateInterval::createFromDateString($increments));
            $intervals[] = urlencode($now->format('Y-m-d\TH:i:s.0000000P'));
        }
        return $intervals;
    }

    /**
     * Not accessible from the outside.
     */
    protected function __construct() {}
}