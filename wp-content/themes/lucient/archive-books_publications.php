<?php
$freeBookText = get_field('free_book_text', 'option');
$post_id = false;
if(is_home())
    $post_id = get_option('page_for_posts');
?>

<?php get_header(); ?>
<div id="blog" class="books">
    <div class="container-fluid standard-header no-bg">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p><?php the_field('books_section_text', 'option'); ?></p>
                    <h1><?php the_field('books_title_text', 'option'); ?></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid section">
        <div class="container">
            <div class="row">
                <div class="col loading-spinner white">
<!--                    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>-->
                    <div></div>
                </div>
            </div>
            <div class="row books-list-container">
            </div>
            <div class="row justify-content-center">
                <div class="col">
                    <div class="nav-buttons">
                        <button type="button" class="btn btn-text--black btn-underline btn-underline--purple previous smooth-scroll" data-target=".standard-header"><?php the_field('books_previous_button_text', 'option'); ?></button>
                        <button type="button" class="btn btn-text--black btn-underline btn-underline--purple next smooth-scroll" data-target=".standard-header"><?php the_field('books_next_button_text', 'option'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_template_part('template-parts/newsletter'); ?>
<?php get_footer(); ?>


