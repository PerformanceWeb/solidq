<?php
$site = get_blog_details();

if(is_page('region'))
    $_SESSION['regionId'] = get_query_var('arloregion');

if(!is_page('region') && !is_page('course') && !is_page('class') && !is_page('e-class'))
    unset($_SESSION['regionId']);
?>

<!DOCTYPE html>
<html class="" <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
    <?php if(is_page('course')) : ?>
        <title><?php echo ucwords(str_replace('-', ' ', get_query_var('arlocoursename'))); ?> | <?php bloginfo('name'); ?></title>
    <?php else : ?>
        <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
    <?php endif; ?>
    <?php if(is_single() && get_post_type() == 'post') : ?>
        <meta property="og:title" content="<?php the_title(); ?>">
        <meta property="og:image" content="<?php echo get_the_post_thumbnail_url(); ?>">
        <meta property="og:description" content="<?php echo strip_tags(get_the_excerpt()); ?>">
        <meta property="og:url" content="<?php get_permalink(); ?>">
        <meta name="twitter:card" content="summary_large_image" />
    <?php endif; ?>
    <?php wp_head(); ?>
</head>
<body>
<?php if($site->blogname === 'Lucient - WorldWide' || $site->blogname === 'Lucient - Worldwide') : ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src=https://www.googletagmanager.com/ns.html?id=GTM-PKW5RK8
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php else : ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T5LQKRH"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php endif; ?>
<div id="header" class="<?php echo ($GLOBALS['headerBgImage']) ? 'transparent' : ''; echo (is_404() ? ' black-bg' : '') ?>">
    <div class="container-fluid">
        <div class="row">
            <div class="col no-margin">
                <nav class="navbar navbar-expand-lg navbar-light" role="navigation">
                    <a class="navbar-brand" href="<?php echo home_url(); ?>">
                        <img class="img-fluid logo" src="<?php echo get_field('logo', 'option')['url'];?>" alt="<?php echo get_field('logo', 'option')['alt']; ?>"/>
                    </a>
                    <button class="navbar-toggler collapsed position-relative" type="button">
                        <!--                            <span class="navbar-toggler-icon"></span>-->
                        <span> </span>
                        <span> </span>
                        <span> </span>
                    </button>
                    <div id="menus" class="collapse navbar-collapse show">
                        <?php dynamic_sidebar('sidebar_1') ?>
                        <?php
                        wp_nav_menu(array(
                            'theme_location'  => 'primary',
                            'depth'           => 3, // 1 = no dropdowns, 2 = with dropdowns.
                            'container'       => 'div',
                            'container_id'    => 'main-menu',
                            'menu_class'      => 'navbar-nav',
                            'walker'          => new Custom_Walker_Nav_Menu()
                        ));
                        ?>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>