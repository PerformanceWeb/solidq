<?php
$featuredImage = get_the_post_thumbnail_url(null, 'full');
?>

<?php get_header(); ?>
<!-- TEMP GRAVITY FORMS STYLES -->
<style>
    .gform_wrapper input[type="submit"] {
        cursor: pointer;
        background: #a17df6;
        padding-left: 25px;
        padding-right: 25px;
        border-radius: 5px;
        border: 0;
        color: #fff;
        font-family: 'Aventa SemiBold', sans-serif;
        font-weight: 300;
        font-size: 20px;
        font-style: normal;
        text-align: center;
    }
    .gform_wrapper input[type="checkbox"] {
        width: auto;
        margin-top: 9px;
        margin-bottom: 10px;
    }
    .gf_checkbox_inline input[type="checkbox"] {
        display: inline;
    }
    .gf_checkbox_inline label {
        display: inline;
    }
    .gform_wrapper input, .gform_wrapper select, .gform_wrapper textarea {
        border: 1px solid #a17df6;
    }
</style>
<div id="blog-post" class="book">
    <div class="container-fluid standard-header no-bg">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1><?php the_title(); ?></h1>
                    <div class="post-details">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid post-content">
        <div class="container">
            <div class="row">
                <div class="col-md-4 side-content">
                    <img src="<?php echo $featuredImage; ?>" alt="<?php the_title(); ?>" class="img-fluid featured-image">
                    <p><?php echo the_field('language'); ?></p>
                    <p><?php echo the_field('author'); ?></p>
                </div>
                <div class="col-md-8">
                    <?php the_content(); ?>
                </div>
            </div>
<!--            <div class="row">-->
<!--                <div class="offset-md-4 col-md-8">-->
<!--                    <div class="socialicons socialicons-colour--offblack">-->
<!--                        <a href="https://twitter.com/Lucient_Data" target="_blank"><i class="fab fa-twitter-square" aria-hidden="true"></i></a>-->
<!--                        <a href="https://www.facebook.com/LucientData" target="_blank"><i class="fab fa-facebook-square" aria-hidden="true"></i></a>-->
<!--                        <a href="https://www.linkedin.com/company/lucientdata/" target="_blank"><i class="fab fa-linkedin" aria-hidden="true"></i></a>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
        </div>
    </div>
</div>


<?php get_template_part('template-parts/latest_news'); ?>
<?php get_template_part('template-parts/newsletter'); ?>

<script type="text/javascript">
    $(document).ready(function () {
        var  $social = $('.addtoany_share_save_container').clone();
        $social.css({'margin': '0px'});
        $('.post-details').append($social);
    });
</script>

<?php get_footer(); ?>
