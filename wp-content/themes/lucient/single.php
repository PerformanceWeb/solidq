<?php

?>

<?php get_header(); ?>
<?php if(have_posts()) : ?>
    <?php while(have_posts()) : the_post(); ?>

    <div id="blog-post">
        <div class="container-fluid standard-header no-bg">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h1><?php the_title(); ?></h1>
                        <div class="post-details">
                            <div>
                                <p><?php echo get_the_date('d F Y'); ?></p>
                                <p>By: <?php echo get_the_author_meta('first_name') . ' ' . get_the_author_meta('last_name'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid post-content">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php get_template_part('template-parts/latest_news'); ?>
    <?php get_template_part('template-parts/newsletter'); ?>

    <?php endwhile; ?>
<?php endif; ?>

<script type="text/javascript">
    $(document).ready(function () {
        var  $social = $('.addtoany_share_save_container').clone();
        $social.css({'margin': '0px'});
        $('.post-details').append($social);
    });
</script>

<?php get_footer(); ?>
