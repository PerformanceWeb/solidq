<?php

global $wp;
$url = home_url() . '/' . $wp->request;

$useSmallCallToAction = get_field('careers_use_small_call_to_action', 'option');
$useLargeCallToAction = get_field('careers_use_large_call_to_action', 'option');
$useLatestNews = get_field('careers_use_latest_news', 'option');
?>

<?php get_header(); ?>

<!-- TEMP GRAVITY FORMS STYLES -->
<style>
    .gform_wrapper input[type="submit"] {
        cursor: pointer;
        background: #a17df6;
        padding-left: 25px;
        padding-right: 25px;
        border-radius: 5px;
        border: 0;
        color: #fff;
        font-family: 'Aventa SemiBold', sans-serif;
        font-weight: 300;
        font-size: 20px;
        font-style: normal;
        text-align: center;
    }
    .gform_wrapper input[type="checkbox"] {
        width: auto;
        margin-top: 9px;
        margin-bottom: 10px;
    }
    input, select, textarea {
        border: 1px solid #a17df6;
    }
</style>

<div id="career">
    <div class="container-fluid standard-header mobile-left-align no-bg">
        <div class="container slim">
            <div class="row">
                <div class="col">
                    <p><?php the_field('careers_header_small_title', 'option'); ?></p>
                    <h1><?php the_title(); ?></h1>
                    <div class="post-details">
                        <div class="text">
                            <h4><?php the_title(); ?></h4>
                            <p class="location"><i class="fas fa-map-marker-alt"></i><?php the_field('location'); ?></p>
                            <p class="address"><?php the_field('address'); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid section post-content">
        <div class="container slim">
            <div class="row justify-content-center">
                <div class="col">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
<!--    <div class="container-fluid section application-form">-->
<!--        <div class="container slim">-->
<!--            <div class="row">-->
<!--                <div class="col no-margin">-->
<!--                    <h2>--><?php //the_field('careers_application_form_title', 'option'); ?><!--</h2>-->
<!--                </div>-->
<!--            </div>-->
<!--            --><?php //echo do_shortcode('[pardot-form id="41429" querystring="form_url='.$url.'"]'); ?>
<!--        </div>-->
<!--    </div>-->
</div>
<?php if($useSmallCallToAction) :
    $buttonClass = getItemColourClass(get_field('careers_small_call_to_action_button_border_colour', 'option'), 'btn-outline'); ?>
    <div class="container-fluid section calltoaction calltoaction-text--center">
        <div class="container" style="background-image: url(<?php echo get_field('careers_small_call_to_action_background', 'option')['url'];?>);">
            <div class="row">
                <div class="col">
                    <h2><?php the_field('careers_small_call_to_action_title', 'option'); ?></h2>
                    <p><?php the_field('careers_small_call_to_action_text', 'option'); ?></p>
                    <a href="<?php the_field('careers_small_call_to_action_button_link', 'option'); ?>" class="btn btn-text--white btn-outline <?php echo $buttonClass;?>"><?php the_field('careers_small_call_to_action_button_text', 'option'); ?></a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if($useLatestNews) : ?>
    <?php get_template_part('template-parts/latest_news'); ?>
<?php endif; ?>

<?php if($useLargeCallToAction) :
    $buttonClass = getItemColourClass(get_field('careers_large_call_to_action_button_border_colour', 'option'), 'btn-outline'); ?>
    <div class="container-fluid section calltoaction calltoaction-text--left" style="background-image: url(<?php echo get_field('careers_large_call_to_action_background', 'option')['url'];?>);">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2><?php the_field('careers_large_call_to_action_title', 'option'); ?></h2>
                    <p><?php the_field('careers_large_call_to_action_text', 'option'); ?></p>
                    <a href="<?php the_field('careers_large_call_to_action_button_link', 'option'); ?>" class="btn btn-text--white btn-outline <?php echo $buttonClass;?>"><?php the_field('careers_large_call_to_action_button_text', 'option'); ?></a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<script type="text/javascript">
    $(document).ready(function () {
        var  $social = $('.addtoany_share_save_container').clone();
        $social.css({'margin': '0px'});
        $('.post-details').append($social);
    });
</script>

<?php get_footer(); ?>
