<?php
$date = new DateTime(str_replace('/', '-', get_field('date')));
$mapRequired = get_field('at_physical_location');
?>

<?php get_header(); ?>

<!-- TEMP GRAVITY FORMS STYLES -->
<style>
    .gform_wrapper input[type="submit"] {
        cursor: pointer;
        background: #a17df6;
        padding-left: 25px;
        padding-right: 25px;
        border-radius: 5px;
        border: 0;
        color: #fff;
        font-family: 'Aventa SemiBold', sans-serif;
        font-weight: 300;
        font-size: 20px;
        font-style: normal;
        text-align: center;
    }
    .gform_wrapper input[type="checkbox"] {
        width: auto;
        margin-top: 9px;
        margin-bottom: 10px;
    }
    input, select, textarea {
        border: 1px solid #a17df6;
    }
</style>

<div id="event">
    <div class="container-fluid standard-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1><?php the_title(); ?></h1>
                    <div class="post-details">
                        <div class="text">
                            <p><?php echo $date->format('d F Y'); ?></p>
                            <p><?php the_field('location'); ?></p>
                            <p><?php the_field('language'); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid post-content section">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-6">
                    <h2><?php the_field('what_is_this_for_title'); ?></h2>
                    <?php the_field('what_is_this_for'); ?>
                    <?php if(get_field('register_button_link') && get_field('register_button_text')) : ?>
                        <a href="<?php the_field('register_button_link'); ?>" class="btn btn-text--white btn-solid btn-solid--purple" target="_blank"><?php the_field('register_button_text'); ?></a>
                    <?php endif; ?>
                    <h3><?php the_field('event_details_title'); ?></h3>
                    <?php the_field('event_details') ;?>
                    <div style="visibility: hidden;"><?php the_content(); ?></div>
<!--                    <div class="socialicons socialicons-colour--offblack hide-mobile">-->
<!--                        <a href="https://twitter.com/Lucient_Data" target="_blank"><i class="fab fa-twitter-square" aria-hidden="true"></i></a>-->
<!--                        <a href="https://www.facebook.com/LucientData" target="_blank"><i class="fab fa-facebook-square" aria-hidden="true"></i></a>-->
<!--                        <a href="https://www.linkedin.com/company/lucientdata/" target="_blank"><i class="fab fa-linkedin" aria-hidden="true"></i></a>-->
<!--                    </div>-->
                </div>
                <div class="col-lg-4 col-xl-4">
                    <div class="map-container <?php echo (!$mapRequired) ? 'no-map' : ''; ?>">
                        <?php if($mapRequired) : ?>
                            <div class="google-map" data-zoom="16">
                                <div class="marker" data-lat="<?php echo get_field('map')['lat']; ?>" data-lng="<?php echo get_field('map')['lng']; ?>"></div>
                            </div>
                            <p class="location"><?php the_field('location_title'); ?></p>
                            <p class="address"><?php the_field('address'); ?></p>
                        <?php endif; ?>
                        <p class="whats-included--title"><?php the_field('whats_included_title'); ?></p>
                        <div class="whats-included--container">
                            <?php if(have_rows('whats_included_repeater')) : ?>
                                <?php while(have_rows('whats_included_repeater')) : the_row(); ?>
                                    <div class="whats-included--item">
                                        <img src="<?php echo get_sub_field('icon')['url']; ?>" alt="<?php echo get_sub_field('icon')['alt']; ?>">
                                        <p><?php the_sub_field('text'); ?></p>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                        <?php if(get_field('register_button_link') && get_field('register_button_text')) : ?>
                            <a href="<?php the_field('register_button_link'); ?>" target="_blank" class="btn btn-text--white btn-solid btn-solid--purple" target="_blank"><?php the_field('register_button_text'); ?></a>
                        <?php endif; ?>
                        <?php if(get_field('whats_included_free_text')) : ?>
                            <?php the_field('whats_included_free_text'); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--    <div id="registerform" class="container-fluid section registerforevent">-->
<!--        <div class="container">-->
<!--            <div class="row justify-content-center">-->
<!--                <div class="col-md-6 no-margin">-->
<!--                    <h2>--><?php //the_field('register_form_title'); ?><!--</h2>-->
<!--                </div>-->
<!--            </div>-->
<!--            --><?php //echo do_shortcode('[contact-form-7 id="313" title="Register for event"]'); ?>
<!--        </div>-->
<!--    </div>-->
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var  $social = $('.addtoany_share_save_container').clone();
        $social.css({'margin': '0px'});
        $('.post-details').append($social);
    });
</script>

<?php get_footer(); ?>
