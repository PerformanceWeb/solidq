<?php
$GLOBALS['headerBgImage'] = '';
show_admin_bar(false);

//Support for post thumbnails
add_theme_support('post-thumbnails');
//Support responsive embeds
add_theme_support('responsive-embeds');

//Stop WordPress from adding HTML line breaks in posts (affects bootstrap)
remove_filter('the_content', 'wpautop');

//Arlo API instance
$client = ArloApiSingleton::get_instance();

// Allow the upload of .svg files through media browser
function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function start_session()
{
    if(!session_id())
    {
        session_start();
    }
}
add_action('init', 'start_session', 1);

function registerBootstrapNavWalker()
{
    require_once get_template_directory() . '/class-custom-navwalker.php';
}
add_action('after_setup_theme', 'registerBootstrapNavWalker');

//Load jQuery
function jQuery_script()
{
    wp_deregister_script('jquery');
    wp_register_script('jquery', "https://code.jquery.com/jquery-3.3.1.min.js", false, null);
    wp_enqueue_script('jquery');
}
add_action("init", "jQuery_script");

// Load stylesheet
function stylesheets()
{
    wp_register_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', array(), 'all');
    wp_register_style('main', get_template_directory_uri() . '/style.css', array(), '1.3.12');
    //wp_register_style('select2-css', 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css', array(), 'all');
    wp_enqueue_style('fonts');
    wp_enqueue_style('bootstrap');
    //wp_enqueue_style('select2-css');
    wp_enqueue_style('main');
}
add_action('wp_enqueue_scripts', 'stylesheets');

// Load site js
function javascript()
{
    wp_register_script('main-js', get_template_directory_uri() . '/public/js/site.js?' . rand(), array('jquery'), null, true);
    wp_localize_script('main-js', 'vars', ['ajax_url' => admin_url('admin-ajax.php'),'event_post_per_page' => get_field('event_items_per_page', 'option'), 'books_post_per_page' => get_field('book_items_per_page', 'option'), 'blog_posts_per_page' => get_field('blog_items_per_page', 'option'), 'multi_site' => get_blog_details()]);
    wp_register_script('slick-js', get_template_directory_uri() . '/public/js/slick.js', array('jquery'), false, true);
    wp_register_script('popper-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array('jquery'), false, true);
    wp_register_script('bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', array('jquery', 'popper-js'), false, true);
    wp_register_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCZZg94rIlD1TJ37KuqQJZOB4BvdTUaKfU');
    wp_register_script('fontawesome', 'https://kit.fontawesome.com/d035be76df.js', array(), false, true);
    wp_register_script('js-cookie', 'https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js', array(), false, true);
    wp_register_script('moment-js', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js', array(), false, true);
    wp_register_script('moment-timezone-js', 'https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.32/moment-timezone-with-data.min.js', array(), false, true);
    //wp_register_script('select2-js', 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js', array('jquery'), false, true);
    wp_enqueue_script('popper-js');
    wp_enqueue_script('slick-js');
    wp_enqueue_script('bootstrap-js');
    wp_enqueue_script('fontawesome');
    wp_enqueue_script('js-cookie');
    wp_enqueue_script('moment-js');
    wp_enqueue_script('moment-timezone-js');
    //wp_enqueue_script('select2-js');
    wp_enqueue_script('main-js');
    wp_enqueue_script('google-maps');
}
add_action('wp_enqueue_scripts', 'javascript');

//function deferScripts($tag, $handle, $src)
//{
//    $defer = array(
//        'google-maps'
//    );
//    if (in_array($handle, $defer))
//    {
//        return '<script src="'. $src .'" type="text/javascript"></script>' . "\n";
//    }
//
//    return $tag;
//}
//add_filter('script_loader_tag', 'deferScripts', 10, 3);


//Register nav menus
function register_my_menus()
{
    register_nav_menus(array(
        'primary' => __('Primary Menu', 'Lucient'),
        'footer_1' => __('Footer Menu 1', 'Lucient'),
        'footer_2' => __('Footer Menu 2', 'Lucient'),
        'footer_3' => __('Footer Menu 3', 'Lucient'),
        'footer_4' => __('Footer Menu 4', 'Lucient'),
        'footer_5' => __('Footer Menu 5', 'Lucient'),
        'footer_6' => __('Footer Menu 6', 'Lucient'),
        'menu_404' => __('404 Page Menu', 'Lucient'),

    ));
}
add_action('init', 'register_my_menus');

function customRewriteRule()
{
    add_rewrite_tag('%arlocat%', '([^&]+)');
    add_rewrite_tag('%arlocatname%', '([^&]+)');
    add_rewrite_tag('%arloclass%', '([^&]+)');
    add_rewrite_tag('%arloclassname%', '([^&]+)');
    add_rewrite_tag('%arloeclass%', '([^&]+)');
    add_rewrite_tag('%arloeclassname%', '([^&]+)');
    add_rewrite_tag('%arlocourse%', '([^&]+)');
    add_rewrite_tag('%arlocoursename%', '([^&]+)');
    add_rewrite_tag('%arloregion%', '([^&]+)');
    add_rewrite_tag('%arlodirectregion%', '([^&]*)');
    add_rewrite_tag('%arloedirectregion%', '([^&]*)');

    add_rewrite_rule('^training/topic/([^/]+)/([^/]+)/?', 'index.php?page_id=1549&arlocat=$matches[1]&arlocatname=$matches[2]', 'top');
    add_rewrite_rule('^training/course/([^/]+)/([^/]+)/?', 'index.php?page_id=1546&arlocourse=$matches[1]&arlocoursename=$matches[2]', 'top');
    add_rewrite_rule('^training/class/([^/]+)/([^/]+)(/([^/]+))?/?', 'index.php?page_id=1545&arloclass=$matches[1]&arloclassname=$matches[2]&arlodirectregion=$matches[4]', 'top');
//    add_rewrite_rule('^training/class/([^/]+)/([^/]+)/?', 'index.php?page_id=1388&arloclass=$matches[1]&arloclassname=$matches[2]', 'top');
    add_rewrite_rule('^training/e-class/([^/]+)/([^/]+)(/([^/]+))?/?', 'index.php?page_id=1547&arloeclass=$matches[1]&arloeclassname=$matches[2]&arloedirectregion=$matches[4]', 'top');
    add_rewrite_rule('^training/region/([^/]+)/?', 'index.php?page_id=1548&arloregion=$matches[1]', 'top');
}
add_action('init', 'customRewriteRule');

function getImg($filename)
{
    return get_template_directory_uri() . '/public/images/' . $filename;
}

function loadWidget($widget)
{
    if (is_active_sidebar($widget))
    {
        dynamic_sidebar($widget);
    }
}

function getItemColourClass($field, $type)
{
    switch($type)
    {
        case 'panel' :
            if($field == 'purple')
                return 'panel-color--purple';
            if($field == 'white')
                return 'panel-color--white';
            if($field == 'deepblack')
                return 'panel-color--deepblack';
            break;
        case 'btn-outline' :
            if($field == 'purple')
                return 'btn-outline--purple';
            if($field == 'white')
                return 'btn-outline--white';
            break;
        case 'calltoaction' :
            if($field == 'full')
                return 'calltoaction-text--left';
            if($field == 'small')
                return 'calltoaction-text--center';
            break;
    }
}

function getSocialIconClass($field)
{
    switch($field)
    {
        case 'twitter' :
            return 'fab fa-twitter-square';
        case 'facebook' :
            return 'fab fa-facebook-square';
        case 'linkedin' :
            return 'fab fa-linkedin';
        case 'instagram' :
            return 'fab fa-instagram-square';
        case 'plus':
            return 'fas fa-plus-square';
    }
}


//ACF Global settings field
if (function_exists('acf_add_options_page'))
{
    acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Header Settings',
        'menu_title'	=> 'Header',
        'parent_slug'	=> 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Footer Settings',
        'menu_title'	=> 'Footer',
        'parent_slug'	=> 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Cookie Notice Settings',
        'menu_title'	=> 'Cookie Notice',
        'parent_slug'	=> 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> '404 Page Settings',
        'menu_title'	=> '404 Page',
        'parent_slug'	=> 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Training Settings',
        'menu_title'	=> 'Training',
        'parent_slug'	=> 'theme-general-settings',
    ));
}

function my_acf_init()
{
    acf_update_setting('google_api_key', 'AIzaSyCZZg94rIlD1TJ37KuqQJZOB4BvdTUaKfU');
}
add_action('acf/init', 'my_acf_init');


function addBootstrapImageClass($class)
{
    $class .= ' img-fluid';
    return $class;
}
add_filter('get_image_tag_class','addBootstrapImageClass');

add_filter('render_block', 'addBootstrapImageClassGutenberg', 10, 2);
function addBootstrapImageClassGutenberg($block_content, $block) {
    if ('core/image' !== $block['blockName']) {
        return $block_content;
    }
    return preg_replace('/(class="wp-image-)/', 'class="img-fluid wp-image-', $block_content);
}



add_action('widgets_init', function() {

    register_sidebar(array(
        'name'          => 'Sidebar',
        'id'            => 'sidebar_1',
        'before_widget' => '<div class="language_switcher">',
        'after_widget'  => '</div>'
    ));
    register_widget('Language_Switcher_Widget');

});

function multilingualpress_get_translations()
{
    $args = \Inpsyde\MultilingualPress\Framework\Api\TranslationSearchArgs::forContext(new \Inpsyde\MultilingualPress\Framework\WordpressContext())
        ->forSiteId(get_current_blog_id())
        ->includeBase();

    $translations = \Inpsyde\MultilingualPress\resolve(
        \Inpsyde\MultilingualPress\Framework\Api\Translations::class
    )->searchTranslations($args);

    return $translations;
}

class Language_Switcher_Widget extends WP_Widget {

    public function __construct() {
        $widget_ops = array(
            'classname' => 'language_switcher',
            'description' => 'Language Switcher',
        );

        parent::__construct('language_switcher', 'Language Switcher', $widget_ops);
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];

        $translations = array_reverse(multilingualpress_get_translations());
        if (!$translations)
            echo '';


        $output = '';
        $items = '';
        foreach ($translations as $translation)
        {
            $language = $translation->language();
            $iso_name = $language->isoName();
            $locale = $language->locale();
            $native_name = $language->nativeName();
            $url = $translation->remoteUrl();

            if(strtolower($iso_name)  === 'german')
                $native_name = 'Deutsch (D/A/CH)';
			
			if(strtolower($locale) === 'en_us')
                $native_name = 'English (North America)';

            if(strtolower($locale) === 'en_001')
                $native_name = 'English (Worldwide)';

            if(strtolower($locale) === 'es_419')
                $native_name = 'Español (América Latina)';

            if ($url)
                $items .= '<li><a href="'.$url.'">'.$native_name.'</a></li>';
        }

        if($items)
        {
            $output .= '<p class="language-switcher-toggle">Language (Region)</p>';
            $output .= '<ul id="multilingualpress-language-switcher">';
            $output .= $items;
            $output .= '</ul>';
            $output .= '<script type="text/javascript">';
            $output .= "jQuery('.language-switcher-toggle').click(function() { jQuery('#multilingualpress-language-switcher').slideToggle(); });";
            $output .= '</script>';
        }

        echo $output;
        echo $args['after_widget'];
    }
}

add_action('wp_ajax_nopriv_get_events', 'getEvents');
add_action('wp_ajax_get_events', 'getEvents');
function getEvents()
{
    $itemsPerPage = get_field('event_items_per_page', 'option');
    $args = [
        'post_type'         => 'events',
        'posts_per_page'    => $itemsPerPage,
        'offset'            => $_POST['offset'],
        'orderby'           => 'date',
        'order'             => $_POST['orderDirection'],
        'meta_query'        => [
            'relation' => 'AND',
            [
                'key' => 'date',
                'value' => date('Ymd'),
                'compare' => $_POST['masterDate']
            ]
        ]
    ];

    if((isset($_POST['date']) && $_POST['date'] != '') || (isset($_POST['search']) && $_POST['search'] != '') || (isset($_POST['location']) && $_POST['location'] != '') || (isset($_POST['language']) && $_POST['language'] != ''))
    {

        if (isset($_POST['date']) && $_POST['date'] != '')
        {
            $date = new DateTime(filter_var($_POST['date'], FILTER_SANITIZE_STRING));
            $dateQuery = [
                'key' => 'date',
                'value' => $date->format('Ymd'),
                'compare' => '='
            ];
            array_push($args['meta_query'], $dateQuery);
        }
        if (isset($_POST['search']) && $_POST['search'] != '')
        {
            $args['s'] = filter_var($_POST['search'], FILTER_SANITIZE_STRING);
        }
        if (isset($_POST['location']) && $_POST['location'] != '')
        {
            $locationQuery = [
                'key' => 'location',
                'value' => filter_var($_POST['location'], FILTER_SANITIZE_STRING),
                'compare' => '='
            ];
            array_push($args['meta_query'], $locationQuery);
        }
        if (isset($_POST['language']) && $_POST['language'] != '')
        {
            $languageQuery = [
                'key' => 'language',
                'value' => filter_var($_POST['language'], FILTER_SANITIZE_STRING),
                'compare' => '='
            ];
            array_push($args['meta_query'], $languageQuery);
        }
    }
    $events = new WP_Query($args);

    echo json_encode([
        'total' => $events->found_posts,
        'count' => $events->post_count,
        'html'  => renderEventList($events)
    ]);
    die();
}

function renderEventList($events)
{
    $registerText = get_field('event_list_register_button_text', 'option');
    $noEventsMessage = get_field('no_events_found_message', 'option');
    $html = '';
    if($events->have_posts()) :
       while($events->have_posts()) :
            $events->the_post();
            $date = new DateTime(str_replace('/', '-', get_field('date')));
            $html .= '<div class="eventitem eventitem-size--large">';
                $html .= '<div class="event-title">';
                    $html .= '<h4>'.get_the_title().'</h4>';
                $html .= '</div>';
                $html .= '<div class="event-details">';
                    $html .= '<p class="date">'.$date->format('d F Y').'</p>';
                    $html .= '<p class="time">'.get_field('time').'</p>';
                    $html .= '<p class="location">'.get_field('location').'</p>';
                $html .= '</div>';
                $html .= '<div class="event-description">';
                    $html .= '<p>'.get_the_excerpt().'</p>';
                $html.= '</div>';
                $html .= '<div class="event-button">';
                    $html .='<a href="'.get_permalink().'" class="btn btn-solid btn-text--white btn-solid--purple">'.$registerText.'</a>';
                $html .= '</div>';
            $html .= '</div>';
        endwhile;
        wp_reset_postdata();
    else :
        $html .= '<div class="no-events-message"><h3 class="text-center">'.$noEventsMessage.'</h3></div>';
    endif;
    return $html;
}


add_action('wp_ajax_nopriv_get_books', 'getBooks');
add_action('wp_ajax_get_books', 'getBooks');
function getBooks()
{
    $itemsPerPage = get_field('book_items_per_page', 'option');
    $args = [
        'post_type'         => 'books_publications',
        'posts_per_page'    => $itemsPerPage,
        'offset'            => $_POST['offset'],
        'orderby'           => 'menu_order',
        'order'             => 'ASC',
    ];

    $books = new WP_Query($args);

    echo json_encode([
        'total' => $books->found_posts,
        'count' => $books->post_count,
        'html'  => renderBooksList($books)
    ]);
    die();
}

function renderBooksList($books)
{
    $html = '';
    if($books->have_posts()) :
        while($books->have_posts()) :
            $books->the_post();
            $featuredImage = get_the_post_thumbnail_url(null, 'full');
            $freeBookText = get_field('free_book_text', 'option');
            $html .= '<div class="col-md-4">';
                $html .= '<div class="news-item">';
                    if(get_field('free')) :
                        $html .= '<p class="free">'.$freeBookText.'</p>';
                    endif;
                    $html .='<a href="'.get_the_permalink().'"><img src="'.$featuredImage.'" alt="'.get_the_title().'" class="img-fluid"></a>';
                    if(!empty($tags)) :
                        $html .= '<p class="label">'.$tags[0]->name.'</p>';
                    endif;
                    $html .= '<h4><a href="'.get_the_permalink().'">'.get_the_title().'</a></h4>';
                    //$html .= '<h4>'.get_the_title().'</h4>';
                    $html .= '<p>'.get_the_excerpt().'</p>';
                $html .='</div>';
            $html .='</div>';
        endwhile;
        wp_reset_postdata();
    endif;

    return $html;
}

add_action('wp_ajax_nopriv_get_blogs', 'getBlogs');
add_action('wp_ajax_get_blogs', 'getBlogs');
function getBlogs()
{
    $itemsPerPage = get_field('blog_items_per_page', 'option');
    $businessArgs = [
        'post_type'         => 'post',
        'category_name'     => 'business',
        'posts_per_page'    => $itemsPerPage/2,
        'offset'            => $_POST['offset']/2
    ];
    $technicalArgs = [
        'post_type'         => 'post',
        'category_name'     => 'technical',
        'posts_per_page'    => $itemsPerPage/2,
        'offset'            => $_POST['offset']/2
    ]; 

    $businessPosts = new WP_Query($businessArgs);
    $technicalPosts = new WP_Query($technicalArgs);

    echo json_encode([
        'total' => $businessPosts->found_posts + $technicalPosts->found_posts,
        'count' => $businessPosts->post_count + $technicalPosts->post_count,
        'html'  => [
            'business' => renderBlogsList($businessPosts, 'business')['business'],
            'technical' => renderBlogsList($technicalPosts, 'technical')['technical']
        ]
    ]);
    die();
}

function renderBlogsList($posts, $postCategory)
{
    $businessHtml = '';
    $technicalHtml = '';
    $post_id = get_option('page_for_posts');
    $noBusinessPostsMessage = get_field('no_business_posts_message', $post_id);
    $noTechnicalPostsMessage = get_field('no_technical_posts_message', $post_id);

    if($posts->have_posts()) :
        while($posts->have_posts()) :
            $posts->the_post();
            $featuredImage = get_the_post_thumbnail_url(null, 'full');
            $excerpt = get_the_excerpt();
            $title = get_the_title();
            $link = get_the_permalink();
            $tags = wp_get_post_terms(get_the_ID(), 'post_tag');
            $mainTag = get_field('tag');
            $category = get_the_category()[0]->slug;

            if($category == 'business') :
                $businessHtml .= '<div class="col-lg-4">';
                    $businessHtml .= '<div class="news-item">';
                        if($featuredImage) :
                            $businessHtml .= '<img src="'.$featuredImage.'" alt="'.$title.'" class="img-fluid">';
                        endif;
                        if(!is_null($mainTag) && $mainTag != '') :
                            $businessHtml .= '<p class="label">'.$mainTag.'</p>';
                        elseif(!empty($tags)) :
                            $businessHtml .= '<p class="label">'.$tags[0]->name.'</p>';
                        endif;
                        $businessHtml .= '<h4><a href="'.$link.'">'.$title.'</a></h4>';
                        $businessHtml .= '<p>'.$excerpt.'</p>';
                    $businessHtml .= '</div>';
                $businessHtml .= '</div>';
            elseif($category == 'technical') :
                $technicalHtml .= '<div class="col-lg-4">';
                    $technicalHtml .= '<div class="news-item">';
                        if($featuredImage) :
                            $technicalHtml .= '<img src="'.$featuredImage.'" alt="'.$title.'" class="img-fluid">';
                        endif;
                        if(!is_null($mainTag) && $mainTag != '') :
                            $technicalHtml .= '<p class="label">'.$mainTag.'</p>';
                        elseif(!empty($tags)) :
                            $technicalHtml .= '<p class="label">'.$tags[0]->name.'</p>';
                        endif;
                        $technicalHtml .= '<h4><a href="'.$link.'">'.$title.'</a></h4>';
                        $technicalHtml .= '<p>'.$excerpt.'</p>';
                    $technicalHtml .= '</div>';
                $technicalHtml .= '</div>';
            endif;
        endwhile;
        wp_reset_postdata();
    else :
        if($postCategory == 'business')
            $businessHtml = '<div class="col"><h3 class="no-blogs-message">'.$noBusinessPostsMessage.'</h3></div>';
        if($postCategory == 'technical')
            $technicalHtml = '<div class="col"><h3 class="no-blogs-message">'.$noTechnicalPostsMessage.'</h3></div>';
    endif;

    return [
        'business'  => $businessHtml,
        'technical' => $technicalHtml
    ];
}

function yoastToBottom() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoastToBottom');

function filterByAuthor() {
    $params = array(
        'name' => 'author',
        'show_option_all' => 'All authors'
    );

    if (isset($_GET['user']))
        $params['selected'] = $_GET['user'];

    wp_dropdown_users($params);
}

add_action('restrict_manage_posts', 'filterByAuthor');

function getEventLocationList() {
    global $wpdb;
    $currentBlogId = get_current_blog_id();
    $blog_prefix = $wpdb->get_blog_prefix($currentBlogId);

    $locations = $wpdb->get_results("SELECT DISTINCT meta_value FROM {$blog_prefix}postmeta WHERE meta_key = 'location'");
    return $locations;
}

function getArloRegionFields($regions, $regionCode)
{
    foreach ($regions as $region)
    {
        if($region['arlo_region_code'] === $regionCode)
            return $region;
    }
}

function redirect404()
{
    global $wp_query;
    $wp_query->set_404();
    status_header(404);
    get_template_part(404);
    exit();
}

function getArloRegionsForLanguage()
{
    $siteSuffix = str_replace('/', '', get_blog_details()->path) ?: 'us';
    return ARLO_LANGUAGE_REGIONS[$siteSuffix]['arlo_regions'];
}

function getArloRegionList($key = null)
{
    $regions = [
        'us' => 'United States',
        'uk' => 'United Kingdom',
        'at' => 'Austria',
        'se' => 'Sweden',
        'no' => 'Norway',
        'it' => 'Italy',
        'de' => 'Germany',
        'dk' => 'Denmark',
        'sp' => 'Spain'
    ];

    if($key)
        return $regions[$key];

    return $regions;
}

function getArloTaxName($region)
{
    switch($region)
    {
        case 'de':
        case 'at':
            return 'UST';
        case 'uk':
        case 'sp':
            return 'VAT';
        case 'dk':
        case 'se':
        case 'no':
            return 'MOMS';
        case 'it':
            return 'IVA';
    }
}

function getArloEvents()
{
    if($_GET['region'] && preg_match('/^[a-zA-Z]{2}$/', $_GET['region']) && is_numeric($_GET['template_id']))
    {
        $_SESSION['regionId'] = $_GET['region'];
        $tempClient = ArloApiSingleton::get_instance();
        $events = $tempClient->EventSearch()->search(null, $_GET['template_id'], null, ['EventID', 'Name', 'StartDateTime', 'Location', 'PlacesRemaining', 'IsFull'], 6, 0, null, $_GET['region']);
        $onlineEvents = $tempClient->OnlineActivitySearch()->search(null, $_GET['template_id'], ['OnlineActivityID', 'Name', 'Description', 'RegistrationInfo'], 6, 0, null, $_GET['region']);
        $html = '';
        $eventsFound = false;

        if ($events && $events->Count > 0)
        {
            $eventsFound = true;
            foreach ($events->Items as $event)
            {
                if (!$event->IsFull)
                {
                    $html .= '<div class="upcoming-course">';
                    $html .= '<a href="' . ArloApiSingleton::getEventUrl($event, $_SESSION['regionId']) . '" class="">';
                    $html .= '<p>' . ArloApiSingleton::getDateParts($event->StartDateTime)->MonthDayYear . '</p>';
                    $html .= '<p>' . ArloApiSingleton::getEventLocation($event->Location) . ' | ' . ArloApiSingleton::getCustomField(ArloApiSingleton::get_auth_instance()->getCustomFieldsForResource('events', $event->EventID), 'deliverylanguage', 'String', 'English') . '</p>';
                    $html .= '</a>';
                    $html .= '</div>';
                }
            }
        }

        if ($onlineEvents && $onlineEvents->Count > 0)
        {
            $eventsFound = true;
            foreach ($onlineEvents->Items as $onlineTemplate)
            {
                $html .= '<div class="upcoming-course">';
                $html .= '<a href="' . ArloApiSingleton::getEventUrl($onlineTemplate, $_SESSION['regionId'], true) . '" class="">';
                $html .= '<p>' . $onlineTemplate->Name . ' (e-learning)</p>';
                $html .= '</a>';
                $html .= '</div>';
            }
        }

        if(!$eventsFound)
            $html .= '<p> No events have been found for this region</p>';

        echo $html;
        die();
    }
    echo 'error';
    die();
}
add_action('wp_ajax_nopriv_get_arlo_events', 'getArloEvents');
add_action('wp_ajax_get_arlo_events', 'getArloEvents');

add_filter('multilingualpress.hreflang_translations', function($translations) {
    foreach($translations as $lang => $url)
    {
        if ($lang === 'es-419')
        {
            $newCode = substr($lang, 0, strpos($lang, '-'));
            $translations[$newCode] = $url;
            unset($translations[$lang]);
        }

        if ($lang === 'en-001')
        {
            $newCode = substr($lang, 0, strpos($lang, '-'));
            $translations[$newCode] = $url;
            unset($translations[$lang]);
        }
    }
    return $translations;
});

function postTypeCanonicalLinks($canonical)
{
    if(is_home())
        return get_field('blog_canonical_url', 'option') ?: $canonical;

    if(is_post_type_archive('careers'))
        return get_field('careers_canonical_url', 'option') ?: $canonical;

    if(is_post_type_archive('events'))
        return get_field('events_canonical_url', 'option') ?: $canonical;

    if(is_post_type_archive('case_studies'))
        return get_field('case_studies_canonical_url', 'option') ?: $canonical;

    if(is_post_type_archive('books_publications'))
        return get_field('books_publications_canonical_url', 'option') ?: $canonical;

    return $canonical;
}
add_filter('wpseo_canonical', 'postTypeCanonicalLinks');

function mucd_primary_table_to_copy($primary_tables) {
    $primary_tables[] = 'yoast_seo_links';
    $primary_tables[] = 'yoast_primary_term';
    $primary_tables[] = 'yoast_migrations';
    $primary_tables[] = 'yoast_indexable_hierarchy';
    $primary_tables[] = 'yoast_indexable';
    $primary_tables[] = 'ewwwio_images';
    $primary_tables[] = 'ewwwio_queue';
    $primary_tables[] = 'mlp_content_relations';
    $primary_tables[] = 'mlp_languages';
    $primary_tables[] = 'mlp_relationships';
    $primary_tables[] = 'mlp_site_relations';
    $primary_tables[] = 'pmxi_files';
    $primary_tables[] = 'pmxi_hash';
    $primary_tables[] = 'pmxi_history';
    $primary_tables[] = 'pmxi_images';
    $primary_tables[] = 'pmxi_imports';
    $primary_tables[] = 'pmxi_posts';
    $primary_tables[] = 'pmxi_templates';
    $primary_tables[] = 'wpmm_subscribers';
    $primary_tables[] = 'wpmailsmtp_tasks_meta';
    $primary_tables[] = 'mlp_site_relations';
    $primary_tables[] = 'mlp_relationships';
    $primary_tables[] = 'mlp_languages';
    $primary_tables[] = 'mlp_content_relations';
    $primary_tables[] = 'gf_form_view';
    $primary_tables[] = 'gf_form_revisions';
    $primary_tables[] = 'gf_form_meta';
    $primary_tables[] = 'gf_form';
    $primary_tables[] = 'gf_entry_notes';
    $primary_tables[] = 'gf_entry_meta';
    $primary_tables[] = 'gf_entry';
    $primary_tables[] = 'gf_draft_submissions';
    $primary_tables[] = 'gf_addon_payment_transaction';
    $primary_tables[] = 'gf_addon_payment_callback';
    $primary_tables[] = 'gf_addon_feed';
    $primary_tables[] = 'actionscheduler_logs';
    $primary_tables[] = 'actionscheduler_groups';
    $primary_tables[] = 'actionscheduler_claims';
    $primary_tables[] = 'actionscheduler_actions';
    return $primary_tables;
}

add_filter('mucd_default_primary_tables_to_copy', 'mucd_primary_table_to_copy');