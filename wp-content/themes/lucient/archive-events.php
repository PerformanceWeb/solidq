<?php
$GLOBALS['headerBgImage'] = true;
get_header();
$locations = getEventLocationList();
$siteSuffix = str_replace('/', '', get_blog_details()->path) ?: 'en';
$placeHolders = [
    'en'    => [
        'search'    => 'Keyword',
        'location'  => 'Location',
        'language'  => 'Language'
    ],
    'es'    => [
        'search'    => 'Palabra clave',
        'location'  => 'Ubicación',
        'language'  => 'Lenguaje'
    ],
    'de'    => [
        'search'    => 'Keywords',
        'location'  => 'Standort ',
        'language'  => 'Sprache'
    ],
    'it'    => [
        'search'    => 'Keywords',
        'location'  => 'Sede',
        'language'  => 'Lingua'
    ]
];
 ?>
<div class="container-fluid standard-header" style="background-image: url('<?php echo get_field('background_image', 'option')['url']; ?>');">
    <div class="container">
        <div class="row">
            <div class="col">
                <p><?php the_field('small_title', 'option'); ?>
                <h1><?php the_field('large_title', 'option'); ?></h1>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid section">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="events-list-header">
                    <h2><?php the_field('upcoming_events_title', 'option'); ?></h2>
                    <div class="event-date-selector">
                        <button type="button" class="btn btn-text--black btn-underline btn-underline--purple inactive" data-search="past"><?php the_field('past_events_label', 'option'); ?></button>
                        <button type="button" class="btn btn-text--black btn-underline btn-underline--purple" data-search="future"><?php the_field('upcoming_events_label', 'option'); ?></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="event-form-filter">
                    <button type="button" class="btn btn-text--purple btn-outline btn-outline--purple toggle-filters hide-desktop"><?php the_field('toggle_event_filter_button_text', 'option'); ?><span class="minus"></span></button>
                    <div class="event-form-filter-container">
                        <form id="event-filter">
                            <input type="hidden" name="action" id="action" value="get_events">
                            <input type="date" name="date" class="grey" placeholder="Date">
                            <input type="text" name="search" class="grey" placeholder="<?php echo $placeHolders[$siteSuffix]['search'];?>">
                            <select name="location" class="grey-outline">
                                <option value=""><?php echo $placeHolders[$siteSuffix]['location'];?></option>
                                <?php foreach ($locations as $location) : ?>
                                <option value="<?php echo $location->meta_value; ?>"><?php echo $location->meta_value; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <select name="language" class="grey-outline">
                                <option value=""><?php echo $placeHolders[$siteSuffix]['language'];?></option>
                                <option value="English">English</option>
                                <option value="German">German</option>
                                <option value="Spanish">Spanish</option>
                            </select>
                            <input type="submit" class="btn btn-text--purple btn-outline btn-outline--purple" value="<?php the_field('filter_events_button_text', 'option'); ?>">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid section eventslist">
    <div class="container">
        <div class="row">
            <div class="col loading-spinner white">
                <div></div>
<!--                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>-->
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="event-list-container">
                </div>
                <div class="load-more-button">
                    <button type="button" class="btn btn-text--black btn-underline btn-underline--purple"><?php the_field('load_more_events_button_text', 'option'); ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_template_part('template-parts/latest_news');
$buttonClass = getItemColourClass(get_field('events_call_to_action_button_border_colour', 'option'), 'btn-outline');
?>

<div class="container-fluid section calltoaction calltoaction-text--left" style="background-image: url(<?php echo get_field('events_call_to_action_background_image', 'option')['url'];?>);">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2><?php the_field('events_call_to_action_title', 'option'); ?></h2>
                <p><?php the_field('events_call_to_action_text', 'option'); ?></p>
                <a href="<?php the_field('events_call_to_action_button_link', 'option'); ?>" class="btn btn-text--white btn-outline <?php echo $buttonClass;?>"><?php the_field('events_call_to_action_button_text', 'option'); ?></a>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
