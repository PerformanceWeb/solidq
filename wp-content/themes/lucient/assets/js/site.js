var headerHeight = 0,
    clicks = {},
    width = 0,
    eventMasterDate = '>',
    eventOffset = 0,
    eventPostsPerPage = vars.event_post_per_page,
    eventPostsVisible = 0,
    bookOffset = 0,
    bookPostsPerPage = vars.books_post_per_page,
    bookPostsVisible = 0,
    blogOffset = 0,
    blogPostsPerPage = vars.blog_posts_per_page,
    blogPostsVisible = 0;

(function($) {
    
    $(document).ready(function() {
        mobileMenu();
        desktopSubMenu();
        mobileSubMenu();
        mobileFooterMenus();
        faqs();
        initMobileSlider();
        initCaseStudySlider();
        positionCaseStudySliderArrows();
        squaresSize();
        eventItemWidth();
        toggleEventFilters();
        getEvents();
        triggerGetEvents();
        getBooks('next');
        triggerGetBooks();
        getBlogs('next');
        triggerGetBlogs();
        smoothScroll();
        setFormHiddenInputs();
        filterCaseStudies();
        toggleLocationDetails();
        toggleTabbedAccordions();
        cookieNotice();
        setIframeHeight();
        $('.google-map').each(function(){
            var map = initMap($(this));
        });
        $('.location:last').addClass('last');
    });
    
    $(window).resize(function() {
        initMobileSlider();
        positionCaseStudySliderArrows();
        filterCaseStudiesFeaturedPost();
        if($(window).width() > 992) {
            $('.event-form-filter-container').show();
            $('.event-form-filter .toggle-filters span').removeClass('plus').addClass('minus');
            var offset = $('#main-menu #menu-main-menu > li.menu-item-has-children.training-menu').offset().left;
            $('#main-menu #menu-main-menu > li.menu-item-has-children.training-menu .sub-menu.lvl-0').css({'left' : '-'+offset+'px'});
        }
    });
    
    $(window).scroll(function() {
        headerBgColour();
    });
    
})(jQuery);

function mobileMenu() {
    $(document).on('click', '.navbar-toggler', function() {
        if($(this).hasClass('collapsed')) {
            $('#menus').addClass('visible');
            $(this).removeClass('collapsed');
            $('html').addClass('push');
            $('#header').addClass('push');
        } else {
            $('#menus').removeClass('visible');
            $(this).addClass('collapsed');
            $('html').removeClass('push');
            $('#header').removeClass('push');
        }
    });
}

function desktopSubMenu() {
    $('#menu-main-menu li.popular a').prepend('<span>Popular course! </span>');

    $(document).on('mouseenter', '#main-menu #menu-main-menu > li.menu-item-has-children', function () {
        var windowWidth = $(window).width();
        if(windowWidth > 992) {
            var subMenuHeight = 0;
            if($(this).hasClass('training-menu')) {
                var offset = $(this).offset().left;
                $(this).find('.sub-menu.lvl-0').css({'left' : '-'+offset+'px'});
                $(this).find('.sub-menu.lvl-1').each(function() {
                    if($(this).outerHeight() > subMenuHeight) {
                        var additional = 60;
                        if(windowWidth < 1400) additional = 200;
                        if(windowWidth < 1100) additional = 250;
                        subMenuHeight = $(this).outerHeight() + additional;
                    }
                });
            }
            $('#header').addClass('hover');
            if(subMenuHeight < 1) {
                subMenuHeight = $(this).find('.sub-menu').outerHeight();
            }
            if (headerHeight == 0) {
                headerHeight = $('#header').outerHeight();
            }
            var newHeaderHeight = headerHeight + subMenuHeight;
            newHeaderHeight -= 40;
            if($('#header').hasClass('transparent') && !$('#header').hasClass('bg-coloured')) {
                $('#header').addClass('sub-menu-coloured');
                $('#header').clearQueue().stop().delay(50).animate({height: newHeaderHeight + 'px'}, 200);
            } else {
                $('#header').clearQueue().stop().animate({height: newHeaderHeight + 'px'}, 200);
            }
        }
    });

    $(document).on('mouseleave', '#main-menu #menu-main-menu > li.menu-item-has-children', function () {
        if($(window).width() > 992) {
            $('#header').removeClass('hover');
            $('#header').animate({height: headerHeight + 'px'}, 200);
            if($('#header').hasClass('transparent') && !$('#header').hasClass('bg-coloured')) {
                $('#header').queue(function() {
                    setTimeout(function(){
                        var $that = $(this);
                        $that.removeClass('sub-menu-coloured');
                        $that.dequeue();
                    }, 10);
                });
            }
            setTimeout(function() {
                if(!$('#header').hasClass('hover')) {
                    $('#header').removeClass('sub-menu-coloured');
                }
            }, 100)
        }
    });
}

function mobileSubMenu() {
    $(document).on('click', '#main-menu li.menu-item-has-children > a', function(e) {
        if($(window).width() < 992) {
            e.preventDefault();
            var key = $(this).parents('li').attr('id'),
                url = $(this).attr('href'),
                $that = $(this);
            if(key in clicks) {
                if(clicks[key] == 2) {
                    clicks[key] = 0;
                } else {
                    clicks[key]++;
                }
            } else {
                clicks[key] = 1;
            }
            if(clicks[key] == 1) {
                $('#main-menu li.menu-item-has-children .sub-menu').each(function() {
                    if($(this).has($that).length < 1 && $(this).css('display') == 'block') {
                        $(this).slideUp();
                        clicks[$(this).parents('li').attr('id')] = 0;
                    }
                });
                $(this).parent('li').find('> .sub-menu').slideDown();
            }
            if(clicks[key] == 2) {
                if(url == '#' || url == '' || typeof url === 'undefined') {
                    clicks[key] = 0;
                    $(this).parent('li').find('> .sub-menu').slideUp();
                } else {
                    window.location = url;
                }
            }
        }
    });
}

function mobileFooterMenus() {
    $(document).on('click', '#footer .footer-menus h4', function() {
        if($(window).width() < 992) {
            if($(this).find('span').hasClass('plus')) {
                $(this).find('span').removeClass('plus').addClass('minus');
                $(this).next('div').slideDown();
            } else {
                $(this).find('span').removeClass('minus').addClass('plus');
                $(this).next('div').slideUp();
            }
        }
    });
}

function faqs() {
    $(document).on('click', '.faq .faq-question', function() {
        if($(this).find('span').hasClass('plus')) {
            $('.faq .faq-question').each(function() {
                if($(this).find('span').hasClass('minus')) {
                    $(this).find('span').removeClass('minus').addClass('plus');
                    $(this).next('p').slideUp();
                }
            });
            $(this).find('span').removeClass('plus').addClass('minus');
            $(this).next('p').slideDown();
        } else {
            $(this).find('span').removeClass('minus').addClass('plus');
            $(this).next('p').slideUp();
        }
    });
}

function initMobileSlider() {
     if($(window).width() < 768) {
         $('.mobile-slider').each(function() {
             var vPos = $(this).attr('data-arrow-pos');
             $(this).slick({
                 dots: false,
                 prevArrow: '<img src="/wp-content/themes/lucient/public/images/sliderPrev.png" class="slick-prev" style="top: '+vPos+'%"/>',
                 nextArrow: '<img src="/wp-content/themes/lucient/public/images/sliderNext.png" class="slick-next" style="top: '+vPos+'%"/>'
             });
         });
     } else {
         if($('.mobile-slider').hasClass('slick-initialized')){
             $('.mobile-slider').slick('unslick');
         }
     }
}

function initCaseStudySlider() {
    $('.case-study-slider .slider').slick({
        dots: false,
        prevArrow: '<img src="/wp-content/themes/lucient/public/images/arrowLeft.png" class="slick-prev"/>',
        nextArrow: '<img src="/wp-content/themes/lucient/public/images/arrowRightPurple.png" class="slick-next"/>',
        appendArrows: $('.case-study-slider .arrows')
    });
}

function positionCaseStudySliderArrows() {
    var baseElement = $('.case-study-slider .slider .slide.slick-current .text');
    if(baseElement.length > 0) {
        if ($(window).width() > 992) {
            var top = baseElement.position().top,
              height = baseElement.outerHeight(),
              bottom = top + height + 120,
              left = baseElement.offset().left;
            $('.case-study-slider .arrows').css({
                'left': left + 'px',
                'top': bottom + 'px',
                'transform': 'translate(0, 0)',
                'opacity': 1
            })
        } else {
            $('.case-study-slider .arrows').css({
                'left': '50%',
                'top': 'calc(100% - 70px)',
                'transform': 'translate(-50%, 0)',
                'opacity': 1
            })
        }
    }
}

function squaresSize() {
    $('.squares-bg').each(function() {
        if($(this).hasClass('square-side--right')) {
            $(this).find('.square-left').css({
                'top' : getRandomInt(0, 30)+'%',
                'left' : getRandomInt(-100, -85)+'%',
                'width' : getRandomInt(10, 40)+'%',
                'height' : getRandomInt(10, 40)+'%'
            });
            $(this).find('.square-right').css({
                'top' : getRandomInt(75, 80)+'%',
                'left' : getRandomInt(75, 85)+'%',
                'width' : getRandomInt(20, 30)+'%',
                'height' : getRandomInt(10, 20)+'%'
            });
        }
        if($(this).hasClass('square-side--left')) {
            $(this).find('.square-left').css({
                'top' : getRandomInt(75, 80)+'%',
                'left' : getRandomInt(45, 55)+'%',
                'width' : getRandomInt(20, 30)+'%',
                'height' : getRandomInt(10, 20)+'%'
            });
            $(this).find('.square-right').css({
                'top' : getRandomInt(0, 30)+'%',
                'left' : getRandomInt(150, 170)+'%',
                'width' : getRandomInt(10, 40)+'%',
                'height' : getRandomInt(10, 40)+'%'
            });
        }
    });
}

function eventItemWidth() {
    $('.latestevents .eventitem-size--small').each(function() {
        if($(this).outerWidth() > width) {
            width = $(this).outerWidth();
        }
    });
    $('.latestevents .eventitem-size--small').css({'width' : width+'px'});
}

function toggleEventFilters() {
    $(document).on('click', '.event-form-filter .toggle-filters', function() {
        var $that = $(this);
        $('.event-form-filter-container').slideToggle(400, function() {
            if($('.event-form-filter-container').css('display') == 'none') {
                $that.children('span').removeClass('minus').addClass('plus');
            } else {
                $that.children('span').removeClass('plus').addClass('minus');
            }
        });
    });
}

function getEvents(filters) {
    if (document.location.href.indexOf('events') > -1 || document.location.href.indexOf('eventi') > -1 || document.location.href.indexOf('eventos') > -1) {
        if (typeof filters === 'undefined') {
            var filters = {};
            filters['action'] = 'get_events';
            filters['masterDate'] = eventMasterDate;
            filters['offset'] = eventOffset;
        } else {
            filters += '&masterDate=' + eventMasterDate;
            filters += '&offset=' + eventOffset;
        }
        if (eventOffset <= 0) {
            $('.event-list-container').html('');
        }
        $('.load-more-button').hide();
        $('.loading-spinner > div').fadeIn(300);
        $.ajax({
            url: vars.ajax_url,
            method: 'POST',
            data: filters,
        })
          .done(function (resp) {
              var data = $.parseJSON(resp);
              eventPostsVisible += parseInt(data.count);
              $('.loading-spinner > div').fadeOut(300, function () {
                  if (eventOffset > 0) {
                      $('.event-list-container').append(data.html);
                  } else {
                      $('.event-list-container').html(data.html);
                  }
                  $('.load-more-button').show();
                  if (parseInt(eventPostsVisible) == parseInt(data.total)) {
                      $('.load-more-button button').addClass('disabled').attr('disabled', true);
                  } else {
                      $('.load-more-button button').removeClass('disabled').attr('disabled', false);
                  }
              });
          })
          .fail(function (resp) {

          });
    }
}

function getBooks(type) {
    if (document.location.href.indexOf('books_publications') > -1 || document.location.href.indexOf('libri_pubblicazioni') > -1 || document.location.href.indexOf('bucher_publikationen') > -1 || document.location.href.indexOf('libros_publicaciones') > -1) {
        if (eventOffset <= 0) {
            $('.books-list-container').html('');
        }
        $('.nav-buttons').hide();
        $('.loading-spinner > div').fadeIn(300);
        $.ajax({
            url: vars.ajax_url,
            method: 'POST',
            data: 'action=get_books&offset=' + bookOffset,
        })
          .done(function (resp) {
              var data = $.parseJSON(resp);
              if(type == 'next') {
                  bookPostsVisible += parseInt(data.count);
              }
              if(type == 'previous') {
                  bookPostsVisible -= parseInt(data.count);
              }
              $('.loading-spinner > div').fadeOut(300, function () {
                  $('.books-list-container').html(data.html);
                  $('.nav-buttons').show();
                  if (parseInt(bookPostsVisible) >= parseInt(data.total)) {
                      $('.nav-buttons button.next').addClass('disabled').attr('disabled', true);
                  } else {
                      $('.nav-buttons button.next').removeClass('disabled').attr('disabled', false);
                  }
                  if (parseInt(bookOffset) == 0) {
                      $('.nav-buttons button.previous').addClass('disabled').attr('disabled', true);
                  } else {
                      $('.nav-buttons button.previous').removeClass('disabled').attr('disabled', false);
                  }
              });
          })
          .fail(function (resp) {

          });
    }
}

function getBlogs(type) {
    if (document.location.href.indexOf('blog') > -1 ) {
        if (blogOffset <= 0) {
            $('.posts-list-container--business').html('');
            $('.posts-list-container--technical').html('');
        }
        $('.nav-buttons').hide();
        $('.loading-spinner > div').fadeIn(300);
        $.ajax({
            url: vars.ajax_url,
            method: 'POST',
            data: 'action=get_blogs&offset=' + blogOffset,
        })
          .done(function (resp) {
              var data = $.parseJSON(resp);
              if(type == 'next') {
                  blogPostsVisible += parseInt(data.count);
              }
              if(type == 'previous') {
                  blogPostsVisible -= parseInt(data.count);
              }
              $('.loading-spinner > div').fadeOut(300, function () {
                  $('.posts-list-container--business').html(data.html.business);
                  $('.posts-list-container--technical').html(data.html.technical);
                  $('.nav-buttons').show();
                  if (parseInt(blogPostsVisible) >= parseInt(data.total)) {
                      $('.nav-buttons button.next').addClass('disabled').attr('disabled', true);
                  } else {
                      $('.nav-buttons button.next').removeClass('disabled').attr('disabled', false);
                  }
                  if (parseInt(blogOffset) == 0) {
                      $('.nav-buttons button.previous').addClass('disabled').attr('disabled', true);
                  } else {
                      $('.nav-buttons button.previous').removeClass('disabled').attr('disabled', false);
                  }
              });
          })
          .fail(function (resp) {

          });
    }
}

function triggerGetEvents() {
    $('#event-filter').on('submit', function(e) {
        e.preventDefault();
        var filters = $(this).serialize();
        eventOffset = 0;
        eventPostsVisible = 0;
        getEvents(filters);
    });
    $(document).on('click', '.event-date-selector button', function() {
        var type = $(this).attr('data-search');
        if(type == 'future') {
            eventMasterDate = '>';
        } else if(type == 'past') {
            eventMasterDate = '<';
        }
        $('.event-date-selector button').toggleClass('inactive');
        eventOffset = 0;
        eventPostsVisible = 0;
        getEvents();
    });
    $(document).on('click', '.load-more-button button:not(.disabled)', function() {
        eventOffset += parseInt(eventPostsPerPage);
        getEvents();
    });
}

function triggerGetBooks() {
    $(document).on('click', '#blog.books .nav-buttons button', function() {
        var type = '';
        if($(this).hasClass('next')) {
            bookOffset += parseInt(bookPostsPerPage);
            type = 'next';
        }
        if($(this).hasClass('previous')) {
            bookOffset -= parseInt(bookPostsPerPage)
            type = 'previous';
        }
        getBooks(type);
    });
}

function triggerGetBlogs() {
    $(document).on('click', '#blog .nav-buttons button', function() {
        var type = '';
        if($(this).hasClass('next')) {
            blogOffset += parseInt(blogPostsPerPage);
            type = 'next';
        }
        if($(this).hasClass('previous')) {
            blogOffset -= parseInt(blogPostsPerPage);
            type = 'previous';
        }
        getBlogs(type);
    });
}

function initMap($el) {
    // Find marker elements within map.
    var $markers = $el.find('.marker');

    // Create gerenic map.
    var mapArgs = {
        zoom        : $el.data('zoom') || 16,
        mapTypeId   : google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map($el[0], mapArgs);

    // Add markers.
    map.markers = [];
    $markers.each(function(){
        initMarker($(this), map);
    });

    // Center map based on markers.
    centerMap(map);

    // Return map instance.
    return map;
}

function initMarker($marker, map) {

    // Get position from marker.
    var lat = $marker.data('lat');
    var lng = $marker.data('lng');
    var latLng = {
        lat: parseFloat(lat),
        lng: parseFloat(lng)
    };

    // Create marker instance.
    var marker = new google.maps.Marker({
        position : latLng,
        map: map
    });

    // Append to reference for later use.
    map.markers.push(marker);

    // If marker contains HTML, add it to an infoWindow.
    if($marker.html()){

        // Create info window.
        var infowindow = new google.maps.InfoWindow({
            content: $marker.html()
        });

        // Show info window when marker is clicked.
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
        });
    }
}


function centerMap( map ) {
    // Create map boundaries from all map markers.
    var bounds = new google.maps.LatLngBounds();
    map.markers.forEach(function(marker){
        bounds.extend({
            lat: marker.position.lat(),
            lng: marker.position.lng()
        });
    });

    // Case: Single marker.
    if(map.markers.length == 1){
        map.setCenter(bounds.getCenter());
        // Case: Multiple markers.
    } else {
        map.fitBounds(bounds);
    }
}

function smoothScroll() {
    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
    });

    $(document).on('click', '.smooth-scroll', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($(this).attr('data-target')).offset().top
        }, 500);
    });
}

function setFormHiddenInputs() {
    if($('#registerform input[name="event-name"]').length > 0){
        $('#registerform input[name="event-name"]').val($('#event .standard-header h1').text());
    }
    if($('.application-form input[name="jobname"]').length > 0){
        $('.application-form input[name="jobname"]').val($('#career .standard-header h1').text());
    }
}

function filterCaseStudies() {
    $(document).on('click', '.filter-container button.filter-posts', function() {
        var targetTag = $('.filter-container select[name="filter"] option:selected').attr('value')
            windowWidth = $(window).width();
        if(targetTag === 'all') {
            $('#casestudies .post-container').attr('style', '');
        } else {
            $('#casestudies .post-container').each(function () {
                var postTags = $(this).attr('data-tags').split('|');
                if ($.inArray(targetTag, postTags) < 0) {
                    if($(this).hasClass('hide-mobile-flex') || $(this).hasClass('hide-desktop-flex')) {
                        if(windowWidth > 992 && !$(this).hasClass('hide-desktop-flex')) {
                            $(this).attr('style', 'display: none !important');
                        } else if(windowWidth < 992 && !$(this).hasClass('hide-mobile-flex')) {
                            $(this).attr('style', 'display: none !important');
                        }
                    } else {
                        $(this).hide();
                    }
                } else {
                    if($(this).hasClass('hide-mobile-flex') || $(this).hasClass('hide-desktop-flex')) {
                        if(windowWidth > 992 && !$(this).hasClass('hide-desktop-flex')) {
                            $(this).attr('style', 'display: flex !important');
                        } else if(windowWidth < 992 && !$(this).hasClass('hide-mobile-flex')) {
                            $(this).attr('style', 'display: flex !important');
                        }
                    } else {
                        if(windowWidth > 992 && !$(this).hasClass('hide-desktop-flex')) {
                            $(this).show();
                        } else if(windowWidth < 992 && !$(this).hasClass('hide-mobile-flex')) {
                            $(this).show();
                        }
                    }
                }
            });
        }
    });
}

function filterCaseStudiesFeaturedPost() {
    var windowWidth = $(window).width();
    if(windowWidth < 992) {
        $('#casestudies .post-container.hide-mobile-flex').attr('style', '');
    } else {
        $('#casestudies .post-container.hide-desktop-flex').attr('style', '');
    }
}

function toggleLocationDetails() {
    $(document).on('click', '.location .more-info', function() {
        $(this).parents('.container').find('.row.location-details').slideToggle(400, function() {
            if ($(this).is(':visible'))
                $(this).css({'display' : 'flex'});
        });
    });
}

function toggleTabbedAccordions() {
    $(document).on('click', '.tabbedaccordions .tabs p', function() {
        var $target = $('#'+$(this).attr('data-tab'));
        $('.tabbedaccordions .tabs p').removeClass('active');
        $(this).addClass('active');
        $('.tabbedaccordions .tab-content').removeClass('active');
        $target.addClass('active');
    });
}

function headerBgColour() {
    var scroll = $(window).scrollTop(),
        $header = $('#header.transparent');
    if(scroll > 100) {
        $header.addClass('bg-coloured');
    } else {
        $header.removeClass('bg-coloured');
    }
}

function cookieNotice() {
    if(typeof Cookies.get('noticeStatus') == 'undefined') {
        Cookies.set('noticeStatus', 'false', {expires: 365});
    }

    //Show cookie notice
    if(Cookies.get('noticeStatus') == 'false') {
        $('#cookieModal').modal('show');
    }

    $(document).on('click', '#cookieModal .cookie-accept', function () {
        Cookies.set('noticeStatus', 'true', {expires: 365});
        setPardotCookie('true');
        $('#cookieModal').modal('hide');
        location.reload();
    });

    $(document).on('click', '#cookieModal .cookie-settings', function () {
        $('#cookieModal .small-notice').hide();
        $('#cookieModal .large-notice').show();
    });

    $(document).on('click', '#cookieModal .cookie-save-settings', function () {
        var selectedCookies = 'technical|';
        $('.cookie-check').each(function () {
            if($(this).is(':checked')) {
                selectedCookies += $(this).attr('name')+'|';
            }
        });
        selectedCookies = selectedCookies.replace(/\|+$/, '');
        if(selectedCookies.indexOf('marketing') !== -1) {
            setPardotCookie('true');
        } else {
            setPardotCookie('false');
        }
        Cookies.set('noticeStatus', selectedCookies, {expires: 365});
        $('#cookieModal').modal('hide');
        location.reload();
    });

    if(Cookies.get('noticeStatus').indexOf('true') !== -1 || Cookies.get('noticeStatus').indexOf('analytical') !== -1) {
        console.log(vars.multi_site.blogname);
        if(vars.multi_site.blogname === 'Lucient - WorldWide' || vars.multi_site.blogname === 'Lucient - Worldwide') {
            $('head').prepend('<!-- Google Tag Manager -->\n' +
              '<script type="text/javascript" class="cookie-trigger">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':\n' +
              'new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],\n' +
              'j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=\n' +
              '\'https://www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);\n' +
              '})(window,document,\'script\',\'dataLayer\',\'GTM-PKW5RK8\');</script>\n' +
              '<!-- End Google Tag Manager -->\n')
        } else {
            $('head').prepend('<!-- Google Tag Manager -->' +
              '<script type="text/javascript" class="cookie-trigger">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':\n' +
              'new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],\n' +
              'j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=\n' +
              '\'https://www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);\n' +
              '})(window,document,\'script\',\'dataLayer\',\'GTM-T5LQKRH\');</script>' +
              '<!-- End Google Tag Manager -->');
        }

    }
}

function setPardotCookie(value) {
    var allCookies = Cookies.get()
    pardotCookie = false;
    $.each(allCookies, function(key, value) {
        if(key.indexOf('pi_opt_in') !== false) {
            pardotCookie = key;
            return false;
        }
    });
    if(pardotCookie !== false) {
        Cookies.set(pardotCookie, value);
    }
}

function setIframeHeight() {
    window.addEventListener('message', function(e) {
        var $iframe = $('iframe.pardotform');
        var eventName = e.data[0];
        var data = e.data[1] + 25;
        switch(eventName) {
            case 'setHeight':
                $iframe.height(data);
                break;
        }
    }, false);
}


var getUrlParameter = function(url, name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(url);
    if (results==null) {
        return null;
    }
    return decodeURI(results[1]) || 0;
}

var getRandomInt = function(min, max) {
    return Math.random() * (max - min) + min;
}


