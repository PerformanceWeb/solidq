<?php

?>

<?php get_header(); ?>
<div id="case-study">
    <div class="container-fluid standard-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1><?php the_title(); ?></h1>
                    <div class="post-details">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid section post-content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <?php the_content(); ?>
                </div>
            </div>
<!--            <div class="row justify-content-center">-->
<!--                <div class="col-md-8">-->
<!--                    <div class="socialicons socialicons-colour--offblack">-->
<!--                        <a href="https://twitter.com/Lucient_Data" target="_blank"><i class="fab fa-twitter-square" aria-hidden="true"></i></a>-->
<!--                        <a href="https://www.facebook.com/LucientData" target="_blank"><i class="fab fa-facebook-square" aria-hidden="true"></i></a>-->
<!--                        <a href="https://www.linkedin.com/company/lucientdata/" target="_blank"><i class="fab fa-linkedin" aria-hidden="true"></i></a>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
        </div>
    </div>
</div>
<?php
$useRelatedCaseStudies = get_field('use_related_case_study_section', 'option');
if($useRelatedCaseStudies)
    get_template_part('template-parts/case_study_slider');

$buttonClass = getItemColourClass(get_field('case_studies_call_to_action_button_border_colour', 'option'), 'btn-outline');

?>

<div class="container-fluid section calltoaction calltoaction-text--left" style="background-image: url(<?php echo get_field('case_studies_call_to_action_background_image', 'option')['url'];?>);">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2><?php the_field('case_studies_call_to_action_title', 'option'); ?></h2>
                <p><?php the_field('case_studies_call_to_action_text', 'option'); ?></p>
                <a href="<?php the_field('case_studies_call_to_action_button_link', 'option'); ?>" class="btn btn-text--white btn-outline <?php echo $buttonClass;?>"><?php the_field('case_studies_call_to_action_button_text', 'option'); ?></a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var  $social = $('.addtoany_share_save_container').clone();
        $social.css({'margin': '0px'});
        $('.post-details').append($social);
    });
</script>

<?php get_footer(); ?>
