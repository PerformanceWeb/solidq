<?php
$wpQueryBus = new WP_Query(array('post_type' => 'post', 'category_name' => 'business'));
$wpQueryTech = new WP_Query(array('post_type' => 'post', 'category_name' => 'technical'));
$post_id = false;
if(is_home())
    $post_id = get_option('page_for_posts');
?>

<?php get_header(); ?>
<div id="blog">
    <?php get_template_part('template-parts/standard_header'); ?>
    <div class="container-fluid section">
        <div class="container">
            <div class="row">
                <div class="col loading-spinner white">
<!--                    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>-->
                    <div></div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h4 class="section-label"><?php the_field('business_heading', $post_id); ?></h4>
                    <div class="row posts-list-container--business">
                    </div>
                </div>
                <div class="w-100"></div>
                <div class="col">
                    <h4 class="section-label"><?php the_field('technical_heading', $post_id); ?></h4>
                    <div class="row posts-list-container--technical">
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col">
                    <div class="nav-buttons">
                        <button type="button" class="btn btn-text--black btn-underline btn-underline--purple previous smooth-scroll" data-target=".standard-header"><?php the_field('blog_previous_button_text', 'option'); ?></button>
                        <button type="button" class="btn btn-text--black btn-underline btn-underline--purple next smooth-scroll" data-target=".standard-header"><?php the_field('blog_next_button_text', 'option'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_template_part('template-parts/newsletter'); ?>
<?php get_footer(); ?>


