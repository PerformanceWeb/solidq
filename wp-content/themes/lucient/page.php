<?php
$post_id = false;
if(is_home())
    $post_id = get_option('page_for_posts');
if(is_front_page())
{
    $GLOBALS['headerBgImage'] = true;
}
else
{
    $GLOBALS['headerBgImage'] = get_field('use_background_image', $post_id);
}

get_header();

	if (have_posts())
	{
		while (have_posts())
		{
			the_post();
			if(is_front_page())
            {
                get_template_part('template-parts/homepage_header');
            }
			elseif(is_page('about-us') || is_page('chi-siamo') || is_page('ueber-uns') || is_page('sobre-nosotros'))
            {
                get_template_part('template-parts/about_header');
            }
			else
            {
                get_template_part('template-parts/standard_header');
            }

			get_template_part('template-parts/default_loop');
		}
	}

	// get_sidebar();
	get_footer();