    <?php wp_footer(); ?>
    <div id="footer">
        <div class="container-fluid">
            <div class="container">
                <div class="row footer-menus">
<!--                    <div class="col-lg no-margin">-->
<!--                        <h4>--><?php //the_field('footer_menu_1_title', 'option'); ?><!--<span class="plus hide-desktop"></span></h4>-->
<!--                        --><?php
//                        wp_nav_menu(array(
//                            'theme_location'  => 'footer_1',
//                            'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
//                            'container'       => 'div',
//                            'container_id'    => 'footer-menu-1',
//                            'menu_class'      => 'navbar-nav',
//                            'walker'          => new Custom_Walker_Nav_Menu()
//                        ));
//                        ?>
<!--                        <hr class="hide-desktop">-->
<!--                    </div>-->
                    <div class="col-lg no-margin">
                        <h4><?php the_field('footer_menu_2_title', 'option'); ?><span class="plus hide-desktop"></span></h4>
                        <?php
                        wp_nav_menu(array(
                            'theme_location'  => 'footer_2',
                            'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
                            'container'       => 'div',
                            'container_id'    => 'footer-menu-2',
                            'menu_class'      => 'navbar-nav',
                            'walker'          => new Custom_Walker_Nav_Menu()
                        ));
                        ?>
                        <hr class="hide-desktop">
                    </div>
                    <div class="col-lg no-margin">
                        <h4><?php the_field('footer_menu_3_title', 'option'); ?><span class="plus hide-desktop"></span></h4>
                        <?php
                        wp_nav_menu(array(
                            'theme_location'  => 'footer_3',
                            'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
                            'container'       => 'div',
                            'container_id'    => 'footer-menu-3',
                            'menu_class'      => 'navbar-nav',
                            'walker'          => new Custom_Walker_Nav_Menu()
                        ));
                        ?>
                        <hr class="hide-desktop">
                    </div>
                    <div class="col-lg no-margin">
                        <h4><?php the_field('footer_menu_4_title', 'option'); ?><span class="plus hide-desktop"></span></h4>
                        <?php
                        wp_nav_menu(array(
                            'theme_location'  => 'footer_4',
                            'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
                            'container'       => 'div',
                            'container_id'    => 'footer-menu-4',
                            'menu_class'      => 'navbar-nav',
                            'walker'          => new Custom_Walker_Nav_Menu()
                        ));
                        ?>
                        <hr class="hide-desktop">
                    </div>
                    <div class="col-lg no-margin">
                        <h4><?php the_field('footer_menu_5_title', 'option'); ?><span class="plus hide-desktop"></span></h4>
                        <?php
                        wp_nav_menu(array(
                            'theme_location'  => 'footer_5',
                            'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
                            'container'       => 'div',
                            'container_id'    => 'footer-menu-5',
                            'menu_class'      => 'navbar-nav',
                            'walker'          => new Custom_Walker_Nav_Menu()
                        ));
                        ?>
                        <hr class="hide-desktop">
                    </div>
                </div>
                <div class="row socialicons-row">
                    <div class="col-md-6">
                        <div class="footer-logo">
                            <img src="<?php echo get_field('footer_logo', 'option')['url']; ?>" alt="<?php echo get_field('footer_logo', 'option')['alt']; ?>" class="img-fluid hide-mobile">
                            <img src="<?php echo get_field('footer_logo_mobile', 'option')['url']; ?>" alt="<?php echo get_field('footer_logo_mobile', 'option')['alt']; ?>" class="img-fluid hide-desktop">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="socialicons socialicons-colour--offblack">
                            <?php if(have_rows('social_icons', 'option')) : ?>
                                <?php while(have_rows('social_icons', 'option')) : the_row(); ?>
                                    <a href="<?php the_sub_field('link'); ?>" target="_blank">
                                        <i class="<?php echo getSocialIconClass(get_sub_field('icon')); ?>"></i>
                                    </a>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <p class="copyright-text"><?php the_field('copyright_text', 'option'); ?></p>
                    </div>
                    <div class="col">
                        <?php
                        wp_nav_menu(array(
                            'theme_location'  => 'footer_6',
                            'depth'           => 1, // 1 = no dropdowns, 2 = with dropdowns.
                            'container'       => 'div',
                            'container_id'    => 'footer-menu-6',
                            'menu_class'      => 'navbar-nav',
                            'walker'          => new Custom_Walker_Nav_Menu()
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="redirectModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <img src="<?php echo getImg('Lucient-footer-logo.png'); ?>" alt="Lucient logo" class="img-fluid">
                    <p>You have been redirected from solidq.com because SolidQ Global is now Lucient. Welcome to our new website, make yourself at home!</p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="cookieModal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="small-notice">
                        <h3><?php the_field('cookie_notice_title', 'option'); ?></h3>
                        <p><?php the_field('cookie_notice_text', 'option'); ?></p>
                        <div>
                            <button type="button" class="cookie-settings btn btn-outline btn-outline--purple btn-text--black"><?php the_field('select_cookie_button_text', 'option'); ?></button>
                            <button type="button" class="cookie-accept btn btn-solid btn-solid--purple btn-text--white"><?php the_field('accept_cookies_button_text', 'option'); ?></button>
                        </div>
                    </div>
                    <div class="large-notice">
                        <h3><?php the_field('cookie_settings_title', 'option'); ?></h3>
                        <div class="cookie-section">
                            <div class="custom-checkbox">
                                <input class="form-check-input" type="checkbox" id="technical" name="technical" value="" disabled checked>
                                <label for="technical"><?php the_field('technical_cookies_title', 'option'); ?></label>
                            </div>
                            <p><?php the_field('technical_cookies_description', 'option'); ?></p>
                        </div>
                        <div class="cookie-section">
                            <div class="custom-checkbox">
                                <input class="form-check-input cookie-check" type="checkbox" id="analytical" name="analytical" value="analytical">
                                <label for="analytical"><?php the_field('analytical_cookies_title', 'option'); ?></label>
                            </div>
                            <p><?php the_field('analytical_cookies_description', 'option'); ?></p>
                        </div>
                        <div class="cookie-section">
                            <div class="custom-checkbox">
                                <input class="form-check-input cookie-check" type="checkbox" id="marketing" name="marketing" value="marketing">
                                <label for="marketing"><?php the_field('marketing_cookies_title', 'option'); ?></label>
                            </div>
                            <p><?php the_field('marketing_cookies_description', 'option'); ?></p>
                        </div>
                        <button type="button" class="cookie-save-settings btn btn-solid btn-solid--purple btn-text--white"><?php the_field('select_cookie_settings_button_text', 'option'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if(isset($_GET['ref']) && $_GET['ref'] == 'solidq') : ?>
        <script>
            $('#redirectModal').modal('show');
        </script>
    <?php endif; ?>
	<!-- This site is converting visitors into subscribers and customers with OptinMonster - https://optinmonster.com -->
    <script type="text/javascript" src=https://a.omappapi.com/app/js/api.min.js data-account="126464" data-user="114513" async></script>
    <!-- / OptinMonster -->
	
</body>
</html>