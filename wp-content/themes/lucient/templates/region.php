<?php
/**
 * Template Name: Region
 */
$GLOBALS['headerBgImage'] = true;
$region = get_query_var('arloregion');
if(!preg_match('/^[a-zA-Z]{2}$/', $region))
    redirect404();
$acfRegions = get_field('region_header_images');
$regionArray = getArloRegionFields($acfRegions, $region);
$highlightedEvents = $client->EventSearch()->search(null, null, null, ['EventID,', 'EventTemplateID', 'Name', 'StartDateTime', 'EndDateTime', 'Location', 'PlacesRemaining', 'IsFull', 'AdvertisedOffers', 'RegistrationInfo', 'EndTimeZoneAbbr'], 2, 0, null, $region, null, ['Featured', 'FeaturedClass']);
$templates = $client->EventTemplateSearch()->search(null, null, ['TemplateID', 'Name', 'Code', 'RegisterInterestUri', 'RegisterPrivateInterestUri', 'Description', 'AdvertisedDuration', 'BestAdvertisedOffers', 'Tags'], null, null, $region);
if($highlightedEvents->Items < 1)
    $highlightedEvents = $client->EventSearch()->search(null, null, null, ['EventID,', 'EventTemplateID', 'Name', 'StartDateTime', 'EndDateTime', 'Location', 'PlacesRemaining', 'IsFull', 'AdvertisedOffers', 'RegistrationInfo', 'EndTimeZoneAbbr'], 2, 0, null, $region);
get_header() ?>

<div class="container-fluid standard-header" <?php echo 'style="background-image: url('.$regionArray['header_image']['url'].');"'; ?>>
    <div class="container">
        <div class="row">
            <div class="col">
                <p><?php echo $regionArray['header_small_text']; ?></p>
                <h1><?php the_field('header_large_text'); ?></h1>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN: HIGHLIGHTED COURSES -->
<div class="container-fluid section regions training-highlighted-classes">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2><?php the_field('highlighted_classes_title'); ?></h2>
            </div>
        </div>
        <div class="row">
            <?php if($highlightedEvents && $highlightedEvents->Count > 0) : ?>
                <?php foreach ($highlightedEvents->Items as $course) : ?>
                    <?php if(!$course->IsFull) : ?>
                        <div class="col-md">
                            <div class="course course-type--white">
                                <?php if($labelText = ArloApiSingleton::getPlacesRemainingText($course->PlacesRemaining, $course->AdvertisedOffers)) : ?>
                                    <p class="label <?php echo ArloApiSingleton::getPlacesRemainingLabelClasses($course->PlacesRemaining, 'white', $course->AdvertisedOffers); ?>"><?php echo $labelText; ?></p>
                                <?php endif; ?>
                                <h4><?php echo $course->Name; ?></h4>
                                <p><?php echo ArloApiSingleton::getEventLocation($course->Location) . ' | ' .  ArloApiSingleton::getEventHeaderDate($course->StartDateTime, $course->EndDateTime) . ' | ' . ArloApiSingleton::getCustomField(ArloApiSingleton::get_auth_instance()->getCustomFieldsForResource('events', $course->EventID), 'deliverylanguage', 'String', 'English'); ?></p>
                                <h4 class="price"><?php echo ArloApiSingleton::getClassPriceString($course->AdvertisedOffers, $region); ?></h4>
                                <div class="buttons">
                                    <a href="<?php echo ArloApiSingleton::getEventUrl($course, $region); ?>" class="btn btn-solid btn-solid--purple btn-text--white">Learn more</a>
<!--                                    <a href="--><?php //echo ArloApiSingleton::getTemplateUrl($course, true); ?><!--" class="btn btn-underline btn-underline--purple btn-text--black">Learn More</a>-->
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<!-- END: HIGHLIGHTED COURSES -->

<!-- BEGIN: AVAILABLE CLASSES -->
<div class="container-fluid section no-price topic-course-templates">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2><?php the_field('available_classes_title'); ?></h2>
            </div>
        </div>
        <div class="row courses-col-margin-bottom">
            <?php if($templates && $templates->Count > 0) : ?>
                <?php foreach($templates->Items as $template) : ?>
                    <div class="col-lg-4">
                        <div class="course course-type--white">
                            <h4><?php echo $template->Name; ?></h4>
                            <div>
                                <?php if(ArloApiSingleton::hasTemplateContentSection($template, 'description')) : ?>
                                    <p><?php echo ArloApiSingleton::templateContentSection($template, 'description', true, 100); ?>...</p>
                                <?php endif; ?>
                            </div>
<!--                            <h4 class="price">--><?php //echo $template->BestAdvertisedOffers[0]->OfferAmount->FormattedAmountTaxInclusive; ?><!--</h4>-->
                            <a href="<?php echo ArloApiSingleton::getTemplateUrl($template); ?>" class="btn btn-solid btn-solid--purple btn-text--white">More info</a>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<!-- END: AVAILABLE CLASSES -->

<?php get_template_part('template-parts/default_loop'); ?>

<?php get_footer(); ?>
