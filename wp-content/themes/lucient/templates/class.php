<?php
/**
 * Template Name: Class
 */
require_once ARLO_API_PLUGIN_DIR.'/ArloAPI/Frontend/Controllers/ClassController.php';
$GLOBALS['headerBgImage'] = true;
$eventId = get_query_var('arloclass');
$arloDirectRegion = get_query_var('arlodirectregion');
if(!is_numeric($eventId))
    redirect404();
if($arloDirectRegion && !preg_match('/^[a-zA-Z]{2}$/', $arloDirectRegion))
    redirect404();
$controller = new ArloAPI\Frontend\Controllers\ClassController($arloDirectRegion, $_SESSION['regionId'], getArloRegionsForLanguage(), $eventId);
$event = $controller->getEvent();
$eventTemplate = $controller->getEventTemplate($event->EventTemplateID);
$eventCategory = $event->Categories[0]->CategoryID;

$includedTags = ArloApiSingleton::getCourseTagsByPrefix($event, 'included');
$optionTags = ArloApiSingleton::getCourseTagsByPrefix($event, 'option');
$formId = (getArloRegionsForLanguage() === 'us' ? 5 : 5);
$useButton = $controller->getCustomField($event, 'showbutton', 'Boolean');
$customButtonText = $controller->getCustomField($event, 'buttontext', 'String');
$customButtonURL = $controller->getCustomField($event, 'buttonurl', 'String');
$customFormShortcode = $controller->getCustomField($event, 'formshortcode', 'String');
get_header() ?>

<!-- TEMPORARY GRAVITY FORMS STYLES -->
<style>
    .gform_wrapper {
        margin-top: 0px !important;
        margin-bottom: 0px !important;
    }
    .gform_wrapper input[type="submit"] {
        cursor: pointer;
        background: #a17df6;
        padding-left: 25px;
        padding-right: 25px;
        border-radius: 5px;
        border: 0;
        color: #fff;
        font-family: 'Aventa SemiBold', sans-serif;
        font-weight: 300;
        font-size: 20px;
        font-style: normal;
        text-align: center;
    }
    .gform_wrapper input[type="checkbox"] {
        width: auto;
        margin-top: 9px;
        margin-bottom: 10px;
        display: inline !important;
    }
    .gform_wrapper label.gfield_label {
        font-size: 14px !important;
        margin-bottom: 0px;
    }
    input, select, textarea {
        border: 1px solid #a17df6;
        width: 100%;
        margin-bottom: 15px;
    }
    .gform_wrapper h3.gform_title {
        font-size: 20px !important;
        line-height: 23px !important;
        margin-bottom: 10px !important;
        margin-top: 0px !important;
    }
    .gf_checkbox_inline input[type="checkbox"] {
        display: inline;
    }
    .gf_checkbox_inline label {
        display: inline;
    }
    .gform_wrapper span.gform_description {
        font-family: 'Aventa SemiBold', sans-serif;
        font-style: normal;
        font-size: 20px;
        line-height: 23px;
        width: 100% !important;
    }
    @media only screen and (min-width: 641px) {
        .gform_wrapper ul.gform_fields li.gfield {
            padding-right: 0px !important;
        }
    }
</style>

<?php if(is_null($event)) : ?>
    <?php
    require_once ARLO_API_PLUGIN_DIR.'/ArloAPI/Frontend/Controllers/HomepageController.php';
    $homePageController = new ArloAPI\Frontend\Controllers\HomepageController($controller->getRegion());
    $upcomingEvents = $homePageController->getUpcomingEvents(ArloApiSingleton::getEventSearchDateIntervals('2 week', '1 week', 1), 10);
    ?>
    <div class="container-fluid standard-header course-header" <?php echo 'style="background-image: url('.ArloApiSingleton::getCourseHeaderImage($eventTemplate->Name).');"'; ?>>
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1><?php echo $eventTemplate->Name ?></h1>
                    <div class="course-details <?php echo (strtolower($event->Location->Name) == 'online' ? 'online' : 'non-online'); ?>">
                        <?php if($event->SessionsDescription) : ?>
                            <p class="label"><?php echo $event->SessionsDescription; ?></p>
                        <?php elseif($eventTemplate->AdvertisedDuration) : ?>
                            <p class="label"><?php echo $eventTemplate->AdvertisedDuration; ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid section">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2>This class has passed, please check the list of upcoming events below.</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h3>Course details</h3>
                    <div class="course course-type--purple height-auto">
                        <h4><?php echo $eventTemplate->Name; ?></h4>
                        <div>
                            <?php if(ArloApiSingleton::hasTemplateContentSection($eventTemplate, 'description')) : ?>
                                <p><?php echo ArloApiSingleton::templateContentSection($eventTemplate, 'description', true, 100); ?>...</p>
                            <?php endif; ?>
                        </div>
                        <a href="<?php echo ArloApiSingleton::getTemplateUrl($eventTemplate); ?>" class="btn btn-solid btn-solid--purple btn-text--white">More info</a>
                    </div>
                </div>
                <div class="col-md-8">
                    <h3>Upcoming classes</h3>
                    <div class="upcoming-classes">
                        <?php if($upcomingEvents) : ?>
                            <?php foreach ($upcomingEvents as $course) : ?>
                                <?php if(!$course->IsFull) : ?>
                                    <div class="course course-type--alternate">
                                        <div class="date">
                                            <p><?php echo ArloApiSingleton::getDateParts($course->StartDateTime)->Date; ?></p>
                                            <p><?php echo ArloApiSingleton::getDateParts($course->StartDateTime)->FullMonth; ?></p>
                                        </div>
                                        <div class="details">
                                            <h4><?php echo $course->Name; ?></h4>
                                            <p class="time"><?php echo ArloApiSingleton::getDateParts($course->StartDateTime)->{$controller->timeFormat}.' - '.ArloApiSingleton::getDateParts($course->EndDateTime)->{$controller->timeFormat}.' '.$course->EndTimeZoneAbbr.' | '.ArloApiSingleton::getEventLocation($course->Location).' | '.ArloApiSingleton::getCustomField(ArloApiSingleton::get_auth_instance()->getCustomFieldsForResource('events', $course->EventID), 'deliverylanguage', 'String', 'English'); ?></p>
                                            <p><?php echo ArloApiSingleton::getEventDescription($course, true, 100); ?>...</p>
                                            <a href="<?php echo ArloApiSingleton::getEventUrl($course, $course->Region); ?>" class="btn btn-solid btn-solid--purple btn-text--white">Learn more</a>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php else : ?>

<div class="container-fluid standard-header course-header" <?php echo 'style="background-image: url('.ArloApiSingleton::getCourseHeaderImage($eventTemplate->Name).');"'; ?>>
    <div class="container">
        <div class="row">
            <div class="col">
                <h1><?php echo $event->Name ?></h1>
                <div class="course-details <?php echo (strtolower($event->Location->Name) == 'online' ? 'online' : 'non-online'); ?>">
                    <p class="label"><?php echo ArloApiSingleton::getEventHeaderDate($event->StartDateTime, $event->EndDateTime); ?></p>
                    <?php if($event->SessionsDescription) : ?>
                        <p class="label"><?php echo $event->SessionsDescription; ?></p>
                    <?php elseif($eventTemplate->AdvertisedDuration) : ?>
                        <p class="label"><?php echo $eventTemplate->AdvertisedDuration; ?></p>
                    <?php endif; ?>
                    <p class="label"><?php echo ArloApiSingleton::getEventLocation($event->Location) ?></p>
<!--                    <p class="label event-time" data-start-time="--><?php //echo $event->StartDateTime; ?><!--" data-end-time="--><?php //echo $event->EndDateTime; ?><!--">--><?php //echo ArloApiSingleton::getDateParts($event->StartDateTime)->Time; ?><!-- - --><?php //echo ArloApiSingleton::getDateParts($event->EndDateTime)->Time.' '.$event->EndTimeZoneAbbr; ?><!--</p>-->
                    <?php if($language = ArloApiSingleton::getCustomField(ArloApiSingleton::get_auth_instance()->getCustomFieldsForResource('events', $event->EventID), 'deliverylanguage', 'String',  'English')) : ?>
                        <?php if($controller->getRegion() !== 'us') : ?>
                            <p class="label"><?php echo $language; ?></p>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if($controller->getRegion() === 'us' && $event->IsConfirmed) : ?>
                        <p class="label">Confirmed class</p>
                    <?php endif; ?>
<!--                    <a href="--><?php //echo $event->RegistrationInfo->RegisterUri; ?><!--" class="btn btn-solid btn-solid--purple btn-text--white">Reserve Place</a>-->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN: COURSE DETAILS -->
<div class="container-fluid section course-details">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-lg-8">
                <div class="class-description">
                    <?php if(ArloApiSingleton::hasTemplateContentSection($eventTemplate, 'description')) : ?>
                        <?php echo ArloApiSingleton::templateContentSection($eventTemplate, 'description'); ?>
                    <?php endif; ?>
                </div>
                <div class="class-price hide-desktop">
                    <?php if($labelText = ArloApiSingleton::getPlacesRemainingText($event->PlacesRemaining, $event->AdvertisedOffers)) : ?>
                        <p class="label <?php echo ArloApiSingleton::getPlacesRemainingLabelClasses($event->PlacesRemaining, 'purple', $event->AdvertisedOffers); ?>"><?php echo $labelText; ?></p>
                    <?php endif; ?>
                    <h3><?php echo ArloApiSingleton::getClassPriceString($event->AdvertisedOffers, $controller->getRegion()); ?></h3>
                    <?php if($useButton) : ?>
                        <?php if(!is_null($customButtonURL) && !is_null($customButtonText)) : ?>
                            <a href="<?php echo $customButtonURL; ?>" class="price-link btn btn-solid btn-solid--purple btn-text--white" <?php echo $controller->bookButtonStyle; ?>><?php echo $customButtonText; ?></a>
                        <?php else : ?>
                            <a href="<?php echo $event->RegistrationInfo->RegisterUri; ?>" class="price-link btn btn-solid btn-solid--purple btn-text--white" <?php echo $controller->bookButtonStyle; ?>><?php echo $controller->bookButtonText; ?></a>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <?php if($includedTags || $optionTags) : ?>
                    <div class="course-features hide-desktop">
                        <?php if($includedTags) : ?>
                            <h4><?php the_field('whats_included_title'); ?></h4>
                            <?php foreach($includedTags as $tag) : ?>
                                <p><?php echo $tag; ?></p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if($optionTags) : ?>
                            <h4><?php the_field('attendee_options_title'); ?></h4>
                            <?php foreach($optionTags as $tag) : ?>
                                <p><?php echo $tag; ?></p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <div class="register-interest-form-container hide-desktop">
<!--                    <h4>--><?php //the_field('register_interest_form_title'); ?><!--</h4>-->
                    <?php if($customFormShortcode) : ?>
                        <?php echo do_shortcode($customFormShortcode); ?>
                    <?php else : ?>
                        <?php echo do_shortcode('[gravityform id="'.$formId.'" title="false" description="true" ajax="true"]'); ?>
                    <?php endif; ?>
                </div>
                <?php if($event->Sessions) : ?>
                <?php $eventSessions = $event->Sessions; ?>
                <?php usort($eventSessions, function($a, $b) {
                        if ($a->StartDateTime == $b->StartDateTime)
                            return 0;
                        return ($a->StartDateTime < $b->StartDateTime) ? -1 : 1;
                }); ?>
                <?php if($controller->getRegion() !== 'us') : ?>
                    <div class="class-schedule">
                        <h2><?php the_field('delivery_and_schedule_title'); ?></h2>
                        <?php foreach($eventSessions as $session) : ?>
                            <div class="class-session">
                                <div class="header">
                                    <?php if($controller->getRegion() === 'us') : ?>
                                        <h4><?php echo ArloApiSingleton::getDateParts($session->StartDateTime)->FullMonth.' '.ArloApiSingleton::getDateParts($session->StartDateTime)->Date.' - '.ArloApiSingleton::getDateParts($session->EndDateTime)->Date.', '.ArloApiSingleton::getDateParts($session->StartDateTime)->Year; ?></h4>
                                    <?php else : ?>
                                        <h4><?php echo ArloApiSingleton::getDateParts($session->StartDateTime)->DayMonthYear.' - '.ArloApiSingleton::getDateParts($session->EndDateTime)->DayMonthYear; ?></h4>
                                    <?php endif; ?>
                                </div>
                                <p class="name"><?php echo $session->Name; ?></p>
                                <div class="details">
                                    <div class="times">
                                        <p class="event-time" data-start-time="<?php echo $session->StartDateTime; ?>" data-end-time="<?php echo $session->EndDateTime; ?>"><?php echo ArloApiSingleton::getDateParts($session->StartDateTime)->{$controller->timeFormat}; ?> - <?php echo ArloApiSingleton::getDateParts($session->EndDateTime)->{$controller->timeFormat}.' '.($controller->getRegion() === 'us' ? ArloApiSingleton::getTimezoneText($session->EndTimeZoneAbbr) : $session->EndTimeZoneAbbr); ?></p>
                                    </div>
                                    <div class="presenters">
                                        <?php if($session->Presenters && $controller->getRegion() !== 'us') : ?>
                                        <?php foreach($session->Presenters as $presenter) : ?>
                                            <p><i class="fas fa-user"></i> <?php echo $presenter->Name; ?></p>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <?php if($controller->getRegion() !== 'us') : ?>
                                    <p class="location"><i class="fas fa-map-marker-alt"></i> <?php echo ArloApiSingleton::getEventLocation($session->Location); ?></p>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                <?php endif; ?>
                <?php foreach($eventTemplate->Description->ContentFields as $contentField) : ?>
                    <?php if(strtolower($contentField->FieldName) !== 'description') : ?>
                        <div class="class-details">
                            <h2><?php echo $contentField->FieldName; ?></h2>
                            <?php echo $contentField->Content->Text; ?>
                            <?php if(strtolower(str_replace(' ', '-', $contentField->FieldName)) === 'course-outline') : ?>
<!--                                <a href="--><?php //echo $event->RegistrationInfo->RegisterUri; ?><!--" class="btn btn-outline btn-outline--purple btn-text--black">Request Course Outline</a>-->
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <div class="col-lg-3">
                <div class="class-price hide-mobile">
                    <?php if($labelText = ArloApiSingleton::getPlacesRemainingText($event->PlacesRemaining, $event->AdvertisedOffers)) : ?>
                        <p class="label <?php echo ArloApiSingleton::getPlacesRemainingLabelClasses($event->PlacesRemaining, 'purple', $event->AdvertisedOffers); ?>"><?php echo $labelText; ?></p>
                    <?php endif; ?>
                    <h3><?php echo ArloApiSingleton::getClassPriceString($event->AdvertisedOffers, $controller->getRegion()); ?></h3>
                    <?php if($useButton) : ?>
                        <?php if(!is_null($customButtonURL) && !is_null($customButtonText)) : ?>
                            <a href="<?php echo $customButtonURL; ?>" class="price-link btn btn-solid btn-solid--purple btn-text--white" <?php echo $controller->bookButtonStyle; ?>><?php echo $customButtonText; ?></a>
                        <?php else : ?>
                            <a href="<?php echo $event->RegistrationInfo->RegisterUri; ?>" class="price-link btn btn-solid btn-solid--purple btn-text--white" <?php echo $controller->bookButtonStyle; ?>><?php echo $controller->bookButtonText; ?></a>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <div class="register-interest-form-container hide-mobile">
<!--                    <h4>--><?php //the_field('register_interest_form_title'); ?><!--</h4>-->
                    <?php if($customFormShortcode) : ?>
                        <?php echo do_shortcode($customFormShortcode); ?>
                    <?php else : ?>
                        <?php echo do_shortcode('[gravityform id="'.$formId.'" title="false" description="true" ajax="true"]'); ?>
                    <?php endif; ?>
                </div>
                <?php if($includedTags || $optionTags) : ?>
                    <div class="course-features hide-mobile">
                        <?php if($includedTags) : ?>
                            <h4><?php the_field('whats_included_title'); ?></h4>
                            <?php foreach($includedTags as $tag) : ?>
                                <p><?php echo $tag; ?></p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if($optionTags) : ?>
                            <h4><?php the_field('attendee_options_title'); ?></h4>
                            <?php foreach($optionTags as $tag) : ?>
                                <p><?php echo $tag; ?></p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <div class="class-other-options sidebar">
                    <h4><?php the_field('other_options_title'); ?></h4>
                    <p><?php the_field('other_option_contact_text'); ?> <a href="https://lucient.arlo.co/US/contact-us"><?php the_field('other_option_contact_link_text'); ?></a></p>
                    <p><?php the_field('other_option_in_house_text'); ?> <a href="https://lucient.arlo.co/US/register-interest?t=2"><?php the_field('other_option_in_house_link_text'); ?></a></p>
                    <p><a href="<?php echo ArloApiSingleton::getTemplateUrl($eventTemplate); ?>"><?php the_field('other_option_course_link_text'); ?></a> <?php the_field('other_option_date_text'); ?> <a href="<?php echo $eventTemplate->RegisterInterestUri; ?>"><?php the_field('other_option_date_link_text'); ?></a></p>
                </div>
            </div>
        </div>
    </div>
    <?php if($presenters = ArloApiSingleton::getCoursePresenters($event)) : ?>
        <div class="container teaching-course">
            <div class="row">
                <div class="col-lg-8">
                    <h2><?php the_field('teaching_this_course_title'); ?></h2>
                    <?php foreach ($presenters as $presenter) : ?>
                        <div class="course-presenter">
                            <?php if($presenter->Image) : ?>
                                <img src="<?php echo $presenter->Image['url']; ?>" alt="<?php echo $presenter->Image['alt']; ?>" class="img-fluid">
                            <?php endif; ?>
                            <div>
                                <h4><?php echo $presenter->Name; ?></h4>
                                <p><?php echo $presenter->Position; ?></p>
                                <?php if($presenter->Bio) : ?>
                                    <p class="desc"><?php echo $presenter->Bio; ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
<!--    <div class="container course-register-buttons">-->
<!--        <div class="row">-->
<!--            <div class="col-lg-8">-->
<!--                <a href="--><?php //echo $event->RegistrationInfo->RegisterUri; ?><!--" class="btn btn-solid btn-solid--purple btn-text--white">--><?php //echo $controller->bookButtonText; ?><!--</a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="container class-other-options">-->
<!--        <div class="row">-->
<!--            <div class="col-lg-8">-->
<!--                <div>-->
<!--                    <h2>--><?php //the_field('other_options_title'); ?><!--</h2>-->
<!--                    <p>--><?php //the_field('other_option_in_house_text'); ?><!--<a href="--><?php //echo $eventTemplate->RegisterPrivateInterestUri; ?><!--">--><?php //the_field('other_option_in_house_link_text'); ?><!--</a></p>-->
<!--                    <p>--><?php //the_field('other_options_date_text'); ?><!--<a href="--><?php //echo $eventTemplate->RegisterInterestUri; ?><!--">--><?php //the_field('other_option_date_link_text'); ?><!--</a></p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
</div>
<!-- END: COURSE DETAILS -->

<!-- BEGIN: REGISTER INTEREST FORM -->
<!--<div class="container-fluid" id="register-interest-form">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col">-->
<!--                <h2>--><?php //the_field('register_interest_form_title'); ?><!--</h2>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col">-->
<!--                <div class="form-container">-->
<!--                    --><?php //echo do_shortcode('[pardot-form id="41866" querystring="form_url='.$url.'"]'); ?>
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!-- END: REGISTER INTEREST FORM -->

<!-- BEGIN: ANOTHER LOCATION -->
<!--<div class="container-fluid section course-alternative-location">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col">-->
<!--                <h2>--><?php //the_field('looking_for_another_city_title'); ?><!--</h2>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col">-->
<!--                <div class="course-grid">-->
<!--                    <div class="row courses-row">-->
<!--                            <div class="col">Courses</div>-->
<!--                        --><?php //foreach ($eventNames as $eventName) : ?>
<!--                            <div class="col">--><?php //echo $eventName; ?><!--</div>-->
<!--                        --><?php //endforeach; ?>
<!--                    </div>-->
<!--                    --><?php //foreach ($eventLocations as $eventLocation) : ?>
<!--                        <div class="row">-->
<!--                            <div class="col locations-col">--><?php //echo $eventLocation; ?><!--</div>-->
<!--                            --><?php //foreach ($eventNames as $eventName) : ?>
<!--                                <div class="col" id="--><?php //echo strtolower(str_replace(' ', '', $eventName)) .'_'.strtolower(str_replace(' ', '', $eventLocation)); ?><!--"></div>-->
<!--                            --><?php //endforeach; ?>
<!--                        </div>-->
<!--                    --><?php //endforeach; ?>
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!-- END: ANOTHER LOCATION -->

<script type="text/javascript">
    $(document).ready(function() {
        //var otherEvents = <?php //echo json_encode($testEvents) ;?>//,
        var timeIn12HrFormat = <?php echo ($controller->getRegion() === 'us' || is_null($controller->getRegion()) ? 'true' : 'false'); ?>,
            region = '<?php echo $controller->getRegion(); ?>';
        // $.each(otherEvents, function(key, event) {
        //     var id = event.Name.replace(' ', '') + '_' + event.Location.replace(' ', '');
        //     id = id.toLowerCase();
        //     $('.course-grid #'+id).append(event.StartDateTime);
        // });
        $('.class-details').each(function() {
            $(this).find('table').addClass('table');
        })
        $('.event-time').each(function() {
            var startTime = moment.utc($(this).attr('data-start-time')),
              endTime = moment.utc($(this).attr('data-end-time')),
              localStartTime = (timeIn12HrFormat ? moment(startTime).local().format('hh:mma') : moment(startTime).local().format('HH:mm')),
              localEndTime = (timeIn12HrFormat ? moment(endTime).local().format('hh:mma') : moment(endTime).local().format('HH:mm')),
              timeZone = moment.tz(moment.tz.guess()).zoneAbbr(),
              arloTime = $(this).text(),
              localTime = localStartTime + ' - ' + localEndTime + ' ' + timeZone;

            if (arloTime != localTime) {
                if(region == 'us') {
                    $(this).append(' / '+localTime);
                } else {
                    // $(this).parent().append('<p>'+localTime+'</p>');
                    $(this).append(' / '+localTime);
                }

            }
        });

        // $('.label.event-time').on('mouseenter', function() {
        //
        //     if($(this).hasClass('local')) {
        //         $(this).text(arloTime);
        //         $(this).removeClass('local');
        //     } else {
        //        $(this).text(localTime);
        //        $(this).addClass('local');
        //     }
        // });
        // $('.label.event-time').on('mouseleave', function() {
        //
        //     if($(this).hasClass('local')) {
        //         $(this).text(arloTime);
        //         $(this).removeClass('local');
        //     } else {
        //         $(this).text(localTime);
        //         $(this).addClass('local');
        //     }
        // });
    });
</script>

<?php endif; ?>

<?php get_footer(); ?>
