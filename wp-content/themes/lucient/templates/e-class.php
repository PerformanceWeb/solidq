<?php
/**
 * Template Name: E-class
 */
require_once ARLO_API_PLUGIN_DIR.'/ArloAPI/Frontend/Controllers/OnlineClassController.php';

$GLOBALS['headerBgImage'] = true;
$eventId = get_query_var('arloeclass');
$arloDirectRegion = get_query_var('arloedirectregion');
if(!ctype_alnum($eventId))
    redirect404();
if($arloDirectRegion && !preg_match('/^[a-zA-Z]{2}$/', $arloDirectRegion))
    redirect404();
$controller = new ArloAPI\Frontend\Controllers\OnlineClassController($arloDirectRegion, $_SESSION['regionId'], getArloRegionsForLanguage(), $eventId);
$event = $controller->getEvent();
$eventTemplate = $controller->getEventTemplate($event->TemplateID);

$includedTags = ArloApiSingleton::getCourseTagsByPrefix($event, 'included');
$optionTags = ArloApiSingleton::getCourseTagsByPrefix($event, 'option');
get_header() ?>

<div class="container-fluid standard-header course-header" <?php echo 'style="background-image: url('.ArloApiSingleton::getCourseHeaderImage($eventTemplate->Name).');"'; ?>>
    <div class="container">
        <div class="row">
            <div class="col">
                <h1><?php echo $event->Name ?></h1>
                <div class="course-details">
                    <div>
                        <?php if($eventTemplate->AdvertisedDuration) : ?>
                            <p class="label"><?php echo $eventTemplate->AdvertisedDuration; ?></p>
                        <?php endif; ?>
                        <p class="label"><?php echo $event->DeliveryDescription; ?></p>
                    </div>
<!--                    <a href="--><?php //echo $event->RegistrationInfo->RegisterUri; ?><!--" class="btn btn-solid btn-solid--purple btn-text--white">Reserve Place</a>-->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN: COURSE DETAILS -->
<div class="container-fluid section course-details">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-lg-8">
                <div class="class-description">
                    <?php if(ArloApiSingleton::hasTemplateContentSection($eventTemplate, 'description')) : ?>
                        <?php echo ArloApiSingleton::templateContentSection($eventTemplate, 'description'); ?>
                    <?php endif; ?>
                </div>
                <div class="class-price hide-desktop">
                    <p class="label label-type--places_ten label-text--white">E-learning</p>
                    <h3><?php echo ArloApiSingleton::getClassPriceString($event->AdvertisedOffers, $controller->getRegion()); ?></h3>
                    <a href="<?php echo $event->RegistrationInfo->RegisterUri; ?>" class="price-link btn btn-solid btn-solid--purple btn-text--white">Reserve Place</a>
                </div>
                <?php if($includedTags || $optionTags) : ?>
                    <div class="course-features hide-desktop">
                        <?php if($includedTags) : ?>
                            <h4><?php the_field('whats_included_title'); ?></h4>
                            <?php foreach($includedTags as $tag) : ?>
                                <p><?php echo $tag; ?></p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if($optionTags) : ?>
                            <h4><?php the_field('attendee_options_title'); ?></h4>
                            <?php foreach($optionTags as $tag) : ?>
                                <p><?php echo $tag; ?></p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <?php foreach($eventTemplate->Description->ContentFields as $contentField) : ?>
                    <?php if(strtolower($contentField->FieldName) !== 'description') : ?>
                        <div class="class-details">
                            <h2><?php echo $contentField->FieldName; ?></h2>
                            <?php echo $contentField->Content->Text; ?>
                            <?php if(strtolower(str_replace(' ', '-', $contentField->FieldName)) === 'course-outline') : ?>
<!--                                <a href="--><?php //echo $event->RegistrationInfo->RegisterUri; ?><!--" class="btn btn-outline btn-outline--purple btn-text--black">Request Course Outline</a>-->
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <div class="col-lg-3">
                <div class="class-price hide-mobile">
                    <p class="label label-type--places_ten label-text--white">E-learning</p>
                    <h3><?php echo ArloApiSingleton::getClassPriceString($event->AdvertisedOffers, $controller->getRegion()); ?></h3>
                    <a href="<?php echo $event->RegistrationInfo->RegisterUri; ?>" class="price-link btn btn-solid btn-solid--purple btn-text--white"><?php echo (strpos(strtolower($event->Name), 'tsql fundamentals') !== false) ? 'Enroll now' : 'Reserve Place'; ?></a>
                </div>
                <?php if($includedTags || $optionTags) : ?>
                    <div class="course-features hide-mobile">
                        <?php if($includedTags) : ?>
                            <h4><?php the_field('whats_included_title'); ?></h4>
                            <?php foreach($includedTags as $tag) : ?>
                                <p><?php echo $tag; ?></p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if($optionTags) : ?>
                            <h4><?php the_field('attendee_options_title'); ?></h4>
                            <?php foreach($optionTags as $tag) : ?>
                                <p><?php echo $tag; ?></p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php if($presenters = ArloApiSingleton::getCoursePresenters($event)) : ?>
        <div class="container teaching-course">
            <div class="row">
                <div class="col-lg-8">
                    <h2><?php the_field('teaching_this_course_title'); ?></h2>
                    <?php foreach ($presenters as $presenter) : ?>
                        <div class="course-presenter">
                            <?php if($presenter->Image) : ?>
                                <img src="<?php echo $presenter->Image['url']; ?>" alt="<?php echo $presenter->Image['alt']; ?>" class="img-fluid">
                            <?php endif; ?>
                            <div>
                                <h4><?php echo $presenter->Name; ?></h4>
                                <p><?php echo $presenter->Position; ?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
<!--    <div class="container course-register-buttons">-->
<!--        <div class="row">-->
<!--            <div class="col-lg-8">-->
<!--                <a href="--><?php //echo $event->RegistrationInfo->RegisterUri; ?><!--" class="btn btn-solid btn-solid--purple btn-text--white">Reserve Place</a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="container class-other-options">-->
<!--        <div class="row">-->
<!--            <div class="col-md-8">-->
<!--                <div>-->
<!--                    <h2>--><?php //the_field('other_options_title'); ?><!--</h2>-->
<!--                    <p>--><?php //the_field('other_option_in_house_text'); ?><!--<a href="--><?php //echo $eventTemplate->RegisterPrivateInterestUri; ?><!--">--><?php //the_field('other_option_in_house_link_text'); ?><!--</a></p>-->
<!--                    <p>--><?php //the_field('other_options_date_text'); ?><!--<a href="--><?php //echo $eventTemplate->RegisterInterestUri; ?><!--">--><?php //the_field('other_option_date_link_text'); ?><!--</a></p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
</div>
<!-- END: COURSE DETAILS -->

<!-- BEGIN: REGISTER INTEREST FORM -->
<!--<div class="container-fluid" id="register-interest-form">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col">-->
<!--                <h2>--><?php //the_field('register_interest_form_title'); ?><!--</h2>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col">-->
<!--                <div class="form-container">-->
<!--                    --><?php //echo do_shortcode('[pardot-form id="41866" querystring="form_url='.$url.'"]'); ?>
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!-- END: REGISTER INTEREST FORM -->

<!-- BEGIN: ANOTHER LOCATION -->
<!--<div class="container-fluid section course-alternative-location">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col">-->
<!--                <h2>--><?php //the_field('looking_for_another_city_title'); ?><!--</h2>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col">-->
<!--                <div class="course-grid">-->
<!--                    <div class="row courses-row">-->
<!--                            <div class="col">Courses</div>-->
<!--                        --><?php //foreach ($eventNames as $eventName) : ?>
<!--                            <div class="col">--><?php //echo $eventName; ?><!--</div>-->
<!--                        --><?php //endforeach; ?>
<!--                    </div>-->
<!--                    --><?php //foreach ($eventLocations as $eventLocation) : ?>
<!--                        <div class="row">-->
<!--                            <div class="col locations-col">--><?php //echo $eventLocation; ?><!--</div>-->
<!--                            --><?php //foreach ($eventNames as $eventName) : ?>
<!--                                <div class="col" id="--><?php //echo strtolower(str_replace(' ', '', $eventName)) .'_'.strtolower(str_replace(' ', '', $eventLocation)); ?><!--"></div>-->
<!--                            --><?php //endforeach; ?>
<!--                        </div>-->
<!--                    --><?php //endforeach; ?>
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!-- END: ANOTHER LOCATION -->

<script type="text/javascript">
    //var otherEvents = <?php //echo json_encode($testEvents) ;?>//;
    //$.each(otherEvents, function(key, event) {
    //    var id = event.Name.replace(' ', '') + '_' + event.Location.replace(' ', '');
    //    id = id.toLowerCase();
    //    $('.course-grid #'+id).append(event.StartDateTime);
    //});
    //$('.class-details').each(function() {
    //    $(this).find('table').addClass('table');
    //})
</script>

<?php get_footer(); ?>
