<?php
/**
 * Template Name: Topic
 */
require_once ARLO_API_PLUGIN_DIR.'/ArloAPI/Frontend/Controllers/TopicController.php';
$GLOBALS['headerBgImage'] = true;
$categoryId = get_query_var('arlocat');
if(!is_numeric($categoryId))
    redirect404();
$arloRegions = getArloRegionsForLanguage();
$upcomingEventsLimit = ($arloRegions === 'us' ? 3 : 10);
$highlightedEventsLimit = ($arloRegions === 'us' ? 3 : null);
$highlightedEventsTags = ($arloRegions === 'us' ? ['FeaturedClass'] : ['FeaturedWorld']);

$controller = new ArloAPI\Frontend\Controllers\TopicController($arloRegions, $categoryId);
$category = $controller->getCategory();
$courses = $controller->getCourses();
$highlightedEvents = $controller->getHighlightedEvents($highlightedEventsLimit, 2, $highlightedEventsTags);
$upcomingEvents = $controller->getUpcomingEvents(ArloApiSingleton::getEventSearchDateIntervals('2 week', '1 week', 1), $upcomingEventsLimit);

get_header(); ?>

<div class="container-fluid standard-header" <?php echo 'style="background-image: url('.ArloApiSingleton::getTopicHeaderImage($category->Name, get_the_ID()).');"'; ?>>
    <div class="container">
        <div class="row">
            <div class="col">
                <p><?php the_field('header_training_small_title'); ?></p>
                <h1><?php echo $category->Name; ?></h1>
                <p class="hide-mobile"><?php echo strip_tags($category->Description->Text); ?></p>
            </div>
        </div>
    </div>
</div>

<?php if($controller->getRegion() !== 'us') : ?>
    <div class="container-fluid section worldwide-layout">
        <div class="container">
            <div class="row">
                <?php if($highlightedEvents) : ?>
                    <div class="col-md-4">
                        <h2><?php the_field('highlighted_classes_title'); ?></h2>
                    </div>
                <?php endif; ?>
                <div class="col-md-8">
                    <h2><?php the_field('upcoming_classes_title'); ?></h2>
                </div>
            </div>
            <div class="row">
                <?php if($highlightedEvents) : ?>
                    <div class="col-md-4">
                        <div class="highlighted-classes">
                            <?php foreach ($highlightedEvents as $course) : ?>
                                <?php if(!$course->IsFull) : ?>
                                    <div class="course course-type--purple">
                                        <?php if($labelText = ArloApiSingleton::getPlacesRemainingText($course->PlacesRemaining, $course->AdvertisedOffers)) : ?>
                                            <p class="label <?php echo ArloApiSingleton::getPlacesRemainingLabelClasses($course->PlacesRemaining, 'purple', $course->AdvertisedOffers); ?>"><?php echo $labelText; ?></p>
                                        <?php endif; ?>
                                        <h4><?php echo $course->Name; ?></h4>
                                        <?php if($arloRegions == 'us') : ?>
                                            <p><?php echo ArloApiSingleton::getEventLocation($course->Location) . ' | ' .  ArloApiSingleton::getEventHeaderDate($course->StartDateTime, $course->EndDateTime); ?></p>
                                        <?php else : ?>
                                            <p><?php echo ArloApiSingleton::getEventLocation($course->Location) . ' | ' .  ArloApiSingleton::getEventHeaderDate($course->StartDateTime, $course->EndDateTime) . ' | ' . ArloApiSingleton::getCustomField(ArloApiSingleton::get_auth_instance()->getCustomFieldsForResource('events', $course->EventID), 'deliverylanguage', 'String', 'English'); ?></p>
                                        <?php endif; ?>
                                        <h4 class="price"><?php echo ArloApiSingleton::getClassPriceString($course->AdvertisedOffers, $course->Region ?: $arloRegions); ?></h4>
                                        <a href="<?php echo ArloApiSingleton::getEventUrl($course, $course->Region); ?>" class="btn btn-solid btn-solid--purple btn-text--white">Learn more</a>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="<?php echo (!$highlightedEvents ? 'col' : 'col-md-8'); ?>">
                    <div class="upcoming-classes">
                        <?php if($upcomingEvents) : ?>
                            <?php foreach ($upcomingEvents as $course) : ?>
                                <?php if(!$course->IsFull) : ?>
                                    <div class="course course-type--alternate">
                                        <div class="date">
                                            <p><?php echo ArloApiSingleton::getDateParts($course->StartDateTime)->Date; ?></p>
                                            <p><?php echo ArloApiSingleton::getDateParts($course->StartDateTime)->FullMonth; ?></p>
                                        </div>
                                        <div class="details">
                                            <h4><?php echo $course->Name; ?></h4>
                                            <p class="time"><?php echo ArloApiSingleton::getDateParts($course->StartDateTime)->{$controller->timeFormat}.' - '.ArloApiSingleton::getDateParts($course->EndDateTime)->{$controller->timeFormat}.' '.$course->EndTimeZoneAbbr.' | '.ArloApiSingleton::getEventLocation($course->Location).' | '.ArloApiSingleton::getCustomField(ArloApiSingleton::get_auth_instance()->getCustomFieldsForResource('events', $course->EventID), 'deliverylanguage', 'String', 'English') ?></p>
                                            <p><?php echo ArloApiSingleton::getEventDescription($course, true, 100); ?>...</p>
                                            <a href="<?php echo ArloApiSingleton::getEventUrl($course, $course->Region); ?>" class="btn btn-solid btn-solid--purple btn-text--white">Learn more</a>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php else : ?>
    <!-- BEGIN: HIGHLIGHTED COURSES -->
    <?php if($highlightedEvents) : ?>
        <div class="container-fluid section regions training-highlighted-classes">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2><?php the_field('highlighted_classes_title'); ?></h2>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <?php foreach ($highlightedEvents as $event) : ?>
                        <?php if(!$event->IsFull) : ?>
                            <div class="col-md-4 margin-bottom">
                                <div class="course course-type--white">
                                    <?php if($labelText = ArloApiSingleton::getPlacesRemainingText($event->PlacesRemaining, $event->AdvertisedOffers)) : ?>
                                        <p class="label <?php echo ArloApiSingleton::getPlacesRemainingLabelClasses($event->PlacesRemaining, 'white', $event->AdvertisedOffers); ?>"><?php echo $labelText; ?></p>
                                    <?php endif; ?>
                                    <h4><?php echo $event->Name; ?></h4>
                                    <?php if($arloRegions == 'us') : ?>
                                        <p><?php echo ArloApiSingleton::getEventLocation($event->Location) . ' | ' .  ArloApiSingleton::getEventHeaderDate($event->StartDateTime, $event->EndDateTime); ?></p>
                                    <?php else : ?>
                                        <p><?php echo ArloApiSingleton::getEventLocation($event->Location) . ' | ' .  ArloApiSingleton::getEventHeaderDate($event->StartDateTime, $event->EndDateTime) . ' | ' . ArloApiSingleton::getCustomField(ArloApiSingleton::get_auth_instance()->getCustomFieldsForResource('events', $event->EventID), 'deliverylanguage', 'String', 'English'); ?></p>
                                    <?php endif; ?>
                                    <h4 class="price"><?php echo ArloApiSingleton::getClassPriceString($event->AdvertisedOffers, $event->Region ?: $arloRegions); ?></h4>
                                    <div class="buttons">
                                        <a href="<?php echo ArloApiSingleton::getEventUrl($event, $arloRegions); ?>" class="btn btn-solid btn-solid--purple btn-text--white">Learn more</a>
        <!--                                    <a href="--><?php //echo ArloApiSingleton::getTemplateUrl($event, true); ?><!--" class="btn btn-underline btn-underline--purple btn-text--black">Learn More</a>-->
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <!-- END: HIGHLIGHTED COURSES -->
<?php endif; ?>

<!-- BEGIN: AVAILABLE CLASSES -->
<?php if($courses) : ?>
    <div class="container-fluid section no-price topic-course-templates">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2><?php the_field('available_classes_title'); ?></h2>
                </div>
            </div>
            <div class="row courses-col-margin-bottom">
                <?php foreach($courses as $course) : ?>
                    <div class="col-lg-4">
                        <div class="course course-type--white">
                            <h4><?php echo $course->Name; ?></h4>
                            <div>
                                <?php if(ArloApiSingleton::hasTemplateContentSection($course, 'description')) : ?>
                                   <p><?php echo ArloApiSingleton::templateContentSection($course, 'description', true, 100); ?>...</p>
                                <?php endif; ?>
                            </div>
                            <a href="<?php echo ArloApiSingleton::getTemplateUrl($course); ?>" class="btn btn-solid btn-solid--purple btn-text--white">More info</a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<!-- END: AVAILABLE CLASSES -->

<div class="container-fluid section topic-description">
    <div class="container">
        <div class="row">
            <div class="col">
                <?php echo $category->Footer->Text; ?>
            </div>
        </div>
    </div>
</div>

<?php get_template_part('template-parts/default_loop'); ?>

<?php get_footer(); ?>
