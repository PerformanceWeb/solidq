<?php
/**
 * Template Name: Training
 */
require_once ARLO_API_PLUGIN_DIR.'/ArloAPI/Frontend/Controllers/HomepageController.php';
$GLOBALS['headerBgImage'] = true;
$arloRegions = getArloRegionsForLanguage();
$upcomingEventsLimit = ($arloRegions === 'us' ? 3 : 10);
$highlightedEventsLimit = ($arloRegions === 'us' ? '2' : null);
$highlightedEventsTags = ($arloRegions === 'us' ? ['FeaturedClass'] : ['FeaturedWorld']);

$controller = new ArloAPI\Frontend\Controllers\HomepageController($arloRegions);
$categories = $controller->getCategories(7);
$upcomingEvents = $controller->getUpcomingEvents(ArloApiSingleton::getEventSearchDateIntervals('2 week', '1 week', 1), $upcomingEventsLimit);
$highlightedEvents = $controller->getHighlightedEvents($highlightedEventsLimit, 4, $highlightedEventsTags);

get_header() ?>

<?php get_template_part('template-parts/standard_header'); ?>

<?php if($controller->getRegion() !== 'us') : ?>
    <div class="container-fluid section worldwide-layout">
        <div class="container">
            <div class="row">
                <?php if($highlightedEvents) : ?>
                    <div class="col-md-4">
                        <h2><?php the_field('highlighted_classes_title'); ?></h2>
                    </div>
                <?php endif; ?>
                <div class="col-md-8">
                    <h2><?php the_field('upcoming_classes_title'); ?></h2>
                </div>
            </div>
            <div class="row">
                <?php if($highlightedEvents) : ?>
                    <div class="col-md-4">
                        <div class="highlighted-classes">
                            <?php foreach ($highlightedEvents as $course) : ?>
                                <?php if(!$course->IsFull) : ?>
                                    <div class="course course-type--purple">
                                        <?php if($labelText = ArloApiSingleton::getPlacesRemainingText($course->PlacesRemaining, $course->AdvertisedOffers)) : ?>
                                            <p class="label <?php echo ArloApiSingleton::getPlacesRemainingLabelClasses($course->PlacesRemaining, 'purple', $course->AdvertisedOffers); ?>"><?php echo $labelText; ?></p>
                                        <?php endif; ?>
                                        <h4><?php echo $course->Name; ?></h4>
                                        <?php if($arloRegions == 'us') : ?>
                                            <p><?php echo ArloApiSingleton::getEventLocation($course->Location) . ' | ' .  ArloApiSingleton::getEventHeaderDate($course->StartDateTime, $course->EndDateTime); ?></p>
                                        <?php else : ?>
                                            <p><?php echo ArloApiSingleton::getEventLocation($course->Location) . ' | ' .  ArloApiSingleton::getEventHeaderDate($course->StartDateTime, $course->EndDateTime) . ' | ' . ArloApiSingleton::getCustomField(ArloApiSingleton::get_auth_instance()->getCustomFieldsForResource('events', $course->EventID), 'deliverylanguage', 'String', 'English'); ?></p>
                                        <?php endif; ?>
                                        <h4 class="price"><?php echo ArloApiSingleton::getClassPriceString($course->AdvertisedOffers, $course->Region ?: $arloRegions); ?></h4>
                                        <a href="<?php echo ArloApiSingleton::getEventUrl($course, $course->Region); ?>" class="btn btn-solid btn-solid--purple btn-text--white">Learn more</a>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="<?php echo (!$highlightedEvents ? 'col' : 'col-md-8'); ?>">
                    <div class="upcoming-classes">
                        <?php if($upcomingEvents) : ?>
                            <?php foreach ($upcomingEvents as $course) : ?>
                                <?php if(!$course->IsFull) : ?>
                                    <div class="course course-type--alternate">
                                        <div class="date">
                                            <p><?php echo ArloApiSingleton::getDateParts($course->StartDateTime)->Date; ?></p>
                                            <p><?php echo ArloApiSingleton::getDateParts($course->StartDateTime)->FullMonth; ?></p>
                                        </div>
                                        <div class="details">
                                            <h4><?php echo $course->Name; ?></h4>
                                            <p class="time"><?php echo ArloApiSingleton::getDateParts($course->StartDateTime)->{$controller->timeFormat}.' - '.ArloApiSingleton::getDateParts($course->EndDateTime)->{$controller->timeFormat}.' '.$course->EndTimeZoneAbbr.' | '.ArloApiSingleton::getEventLocation($course->Location).' | '.ArloApiSingleton::getCustomField(ArloApiSingleton::get_auth_instance()->getCustomFieldsForResource('events', $course->EventID), 'deliverylanguage', 'String', 'English'); ?></p>
                                            <p><?php echo ArloApiSingleton::getEventDescription($course, true, 100); ?>...</p>
                                            <a href="<?php echo ArloApiSingleton::getEventUrl($course, $course->Region); ?>" class="btn btn-solid btn-solid--purple btn-text--white">Learn more</a>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php else : ?>
    <!-- BEGIN: HIGHLIGHTED COURSES -->
    <div class="container-fluid section training-highlighted-classes us-layout">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2><?php the_field('highlighted_classes_title'); ?></h2>
                </div>
            </div>
            <div class="row justify-content-center">
                <?php if($highlightedEvents) : ?>
                    <?php foreach ($highlightedEvents as $course) : ?>
                        <?php if(!$course->IsFull) : ?>
                            <div class="col-md-4 margin-bottom">
                                <div class="course course-type--purple">
                                    <?php if($labelText = ArloApiSingleton::getPlacesRemainingText($course->PlacesRemaining, $course->AdvertisedOffers)) : ?>
                                        <p class="label <?php echo ArloApiSingleton::getPlacesRemainingLabelClasses($course->PlacesRemaining, 'purple', $course->AdvertisedOffers); ?>"><?php echo $labelText; ?></p>
                                    <?php endif; ?>
                                    <h4><?php echo $course->Name; ?></h4>
                                    <?php if($arloRegions == 'us') : ?>
                                        <p><?php echo ArloApiSingleton::getEventLocation($course->Location) . ' | ' .  ArloApiSingleton::getEventHeaderDate($course->StartDateTime, $course->EndDateTime); ?></p>
                                    <?php else : ?>
                                        <p><?php echo ArloApiSingleton::getEventLocation($course->Location) . ' | ' .  ArloApiSingleton::getEventHeaderDate($course->StartDateTime, $course->EndDateTime) . ' | ' . ArloApiSingleton::getCustomField(ArloApiSingleton::get_auth_instance()->getCustomFieldsForResource('events', $course->EventID), 'deliverylanguage', 'String', 'English'); ?></p>
                                    <?php endif; ?>
                                    <h4 class="price"><?php echo ArloApiSingleton::getClassPriceString($course->AdvertisedOffers, $course->Region ?: $arloRegions); ?></h4>
                                    <a href="<?php echo ArloApiSingleton::getEventUrl($course, $course->Region); ?>" class="btn btn-solid btn-solid--purple btn-text--white">Learn more</a>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!-- END: HIGHLIGHTED COURSES -->

    <!-- BEGIN: UPCOMING CLASSES -->
<!--    <div class="container-fluid section training-upcoming-classes us-layout">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <div class="col">-->
<!--                    <h2>--><?php //the_field('upcoming_classes_title'); ?><!--</h2>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row justify-content-center">-->
<!--                --><?php //if($upcomingEvents) : ?>
<!--                    --><?php //foreach ($upcomingEvents as $course) : ?>
<!--                        --><?php //if(!$course->IsFull) : ?>
<!--                            <div class="col-md-4 margin-bottom">-->
<!--                                <div class="course course-type--white">-->
<!--                                    --><?php //if($labelText = ArloApiSingleton::getPlacesRemainingText($course->PlacesRemaining, $course->AdvertisedOffers)) : ?>
<!--                                        <p class="label --><?php //echo ArloApiSingleton::getPlacesRemainingLabelClasses($course->PlacesRemaining, 'white', $course->AdvertisedOffers); ?><!--">--><?php //echo $labelText; ?><!--</p>-->
<!--                                    --><?php //endif; ?>
<!--                                    <h4>--><?php //echo $course->Name; ?><!--</h4>-->
<!--                                    <div>-->
<!--                                        <p>--><?php //echo ArloApiSingleton::getEventHeaderDate($course->StartDateTime, $course->EndDateTime); ?><!--</p>-->
<!--                                        --><?php //if($arloRegions != 'us') : ?>
<!--                                            <p>--><?php //echo ArloApiSingleton::getCustomField(ArloApiSingleton::get_auth_instance()->getCustomFieldsForResource('events', $course->EventID), 'deliverylanguage', 'English') ?><!--</p>-->
<!--                                        --><?php //endif; ?>
<!--                                        <p>--><?php //echo ArloApiSingleton::getEventLocation($course->Location); ?><!--</p>-->
<!--                                    </div>-->
<!--                                    <h4 class="price">--><?php //echo ArloApiSingleton::getClassPriceString($course->AdvertisedOffers, $course->Region ?: $arloRegions); ?><!--</h4>-->
<!--                                    <a href="--><?php //echo ArloApiSingleton::getEventUrl($course, $course->Region); ?><!--" class="btn btn-solid btn-solid--purple btn-text--white">Learn more</a>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        --><?php //endif; ?>
<!--                    --><?php //endforeach; ?>
<!--                --><?php //endif; ?>
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <!-- END: UPCOMING CLASSES -->
<?php endif; ?>

<!-- BEGIN: AVAILABLE TOPICS -->
<div class="container-fluid section training-topics">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2><?php the_field('available_topics_title'); ?></h2>
            </div>
        </div>
        <div class="row">
            <?php if($categories && !empty($categories)) : ?>
                <?php foreach ($categories as $category) : ?>
                    <?php $slug = str_replace([',', ' '], ['', '_'], strtolower($category->Name)).'_topic_image'; ?>
                    <div class="col-md">
                        <a href="<?php echo ArloApiSingleton::getTopicUrl($category); ?>" class="topic">
                            <img src="<?php echo get_field($slug)['url']; ?>" alt="<?php echo get_field($slug)['alt']; ?>" class="img-fluid">
                            <h4><?php echo $category->Name; ?></h4>
                        </a>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<!-- END: AVAILABLE TOPICS -->

<!-- BEGIN: DELIVERY TYPES -->
<div class="container-fluid section training-delivery-types">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2><?php the_field('delivery_type_title'); ?></h2>
            </div>
        </div>
        <div class="row">
            <?php if(have_rows('delivery_types')) : ?>
                <?php while(have_rows('delivery_types')) : the_row(); ?>
                    <div class="col-md">
                        <div class="panel panel-color--purple">
                            <h3><?php the_sub_field('type'); ?></h3>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<!-- END: DELIVERY TYPES -->

<?php get_template_part('template-parts/default_loop'); ?>

<?php get_footer(); ?>
