<?php
/**
 * Template Name: Course
 */
require_once ARLO_API_PLUGIN_DIR.'/ArloAPI/Frontend/Controllers/CourseController.php';
$GLOBALS['headerBgImage'] = true;
$templateId = get_query_var('arlocourse');
if(!is_numeric($templateId))
    redirect404();

$controller = new \ArloAPI\FrontEnd\Controllers\CourseController($_SESSION['regionId'] ?? getArloRegionsForLanguage(), $templateId);
$eventTemplate = $controller->getEventTemplate();
$events = $controller->getEvents();
$onlineEvents = $controller->getOnlineEvents();
$presenters = $controller->getPresenters($events);

get_header() ?>

<div class="container-fluid standard-header course-header" <?php echo 'style="background-image: url('.ArloApiSingleton::getCourseHeaderImage($eventTemplate->Name).');"'; ?>>
    <div class="container">
        <div class="row">
            <div class="col">
                <h1><?php echo $eventTemplate->Name ?></h1>
                <div class="course-details">
                    <div>
                        <?php if($eventTemplate->AdvertisedDuration) : ?>
                            <p class="label"><?php echo $eventTemplate->AdvertisedDuration; ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN: COURSE DETAILS -->
<div class="container-fluid section course-details">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-lg-8">
                <div class="class-description">
                    <?php if(ArloApiSingleton::hasTemplateContentSection($eventTemplate, 'description')) : ?>
                        <?php echo ArloApiSingleton::templateContentSection($eventTemplate, 'description'); ?>
                    <?php endif; ?>
                </div>
                <?php foreach($eventTemplate->Description->ContentFields as $contentField) : ?>
                    <?php if(strtolower($contentField->FieldName) !== 'description') : ?>
                        <div class="class-details">
                            <h2><?php echo $contentField->FieldName; ?></h2>
                            <?php echo $contentField->Content->Text; ?>
                            <?php if(strtolower(str_replace(' ', '-', $contentField->FieldName)) === 'course-outline') : ?>
<!--                                <a href="#register-interest-form" class="btn btn-outline btn-outline--purple btn-text--black">Request Course Outline</a>-->
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <div class="col-lg-3">
                <?php if(!empty($events) || !empty($onlineEvents)) : ?>
                    <div class="upcoming-courses">
                        <h4><?php the_field('upcoming_classes_title'); ?></h4>
                        <?php foreach ($events as $event) : ?>
                            <?php if(!$event->IsFull) : ?>
                                <div class="upcoming-course">
                                    <a href="<?php echo ArloApiSingleton::getEventUrl($event, $event->Region); ?>" class="">
                                        <p><?php echo ArloApiSingleton::getDateParts($event->StartDateTime)->MonthDayYear; ?></p>
                                        <?php if($controller->getRegion() === 'us') : ?>
                                            <p><?php echo ($event->Location->Name == 'Online' ? 'Live ' : '').$event->Location->Name; ?></p>
                                        <?php else : ?>
                                            <p><?php echo ($event->Location->Name == 'Online' ? 'Live ' : '').$event->Location->Name.', '.$event->StartTimeZoneAbbr.', '.ArloApiSingleton::getCustomField(ArloApiSingleton::get_auth_instance()->getCustomFieldsForResource('events', $event->EventID), 'deliverylanguage', 'String', 'English') ?></p>
                                        <?php endif; ?>

                                    </a>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <?php foreach ($onlineEvents as $onlineEvent) : ?>
                            <?php if(!$onlineEvent->IsFull) : ?>
                                <div class="upcoming-course">
                                    <a href="<?php echo ArloApiSingleton::getEventUrl($onlineEvent, $onlineEvent->Region, true); ?>" class="">
                                        <p><?php echo $onlineEvent->Name ?> (e-learning)</p>
                                    </a>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>

<!--                    <select id="region-selector" class="" name="region-selector">-->
<!--                        <option value="">Select a region</option>-->
<!--                        --><?php //foreach ($arloRegionList as $key => $value) : ?>
<!--                            <option value="--><?php //echo $key; ?><!--">--><?php //echo $value; ?><!--</option>-->
<!--                        --><?php //endforeach; ?>
<!--                    </select>-->
<!--                    <div class="region-classes-container"></div>-->
                    </div>
                <?php endif; ?>
                <div class="class-other-options sidebar">
                    <h4><?php the_field('other_options_title'); ?></h4>
                    <p><?php the_field('other_option_contact_text'); ?> <a href="https://lucient.arlo.co/US/contact-us"><?php the_field('other_option_contact_link_text'); ?></a></p>
                    <p><?php the_field('other_option_in_house_text'); ?> <a href="https://lucient.arlo.co/US/register-interest?t=2"><?php the_field('other_option_in_house_link_text'); ?></a></p>
                    <?php if(!$controller->eLearning) : ?>
                        <p><?php the_field('other_options_date_text'); ?> <a href="<?php echo $eventTemplate->RegisterInterestUri; ?>"><?php the_field('other_option_date_link_text'); ?></a></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <?php if(!empty($presenters)) : ?>
        <div class="container teaching-course">
            <div class="row">
                <div class="col-lg-8">
                    <h2><?php the_field('teaching_this_course_title'); ?></h2>
                    <?php foreach ($presenters as $presenter) : ?>
                        <div class="course-presenter">
                            <?php if($presenter->Image) : ?>
                                <img src="<?php echo $presenter->Image['url']; ?>" alt="<?php echo $presenter->Image['alt']; ?>" class="img-fluid">
                            <?php endif; ?>
                            <div>
                                <h4><?php echo $presenter->Name; ?></h4>
                                <p><?php echo $presenter->Position; ?></p>
                                <?php if($presenter->Bio) : ?>
                                    <p class="desc"><?php echo $presenter->Bio; ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="container course-register-buttons">
        <div class="row">
            <div class="col-lg-8">
<!--                <a href="--><?php //echo $eventTemplate->RegisterInterestUri; ?><!--; ?>" class="btn btn-solid btn-solid--purple btn-text--white">Reserve Place</a>-->
            </div>
        </div>
    </div>
</div>
<!-- END: COURSE DETAILS -->

<!-- BEGIN: REGISTER INTEREST FORM -->
<!--<div class="container-fluid" id="register-interest-form">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col">-->
<!--                <h2>--><?php //the_field('register_interest_form_title'); ?><!--</h2>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col">-->
<!--                <div class="form-container">-->
<!--                    --><?php //echo do_shortcode('[pardot-form id="41866" querystring="form_url='.$url.'"]'); ?>
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!-- END: REGISTER INTEREST FORM -->

<!-- BEGIN: ANOTHER LOCATION -->
<!--<div class="container-fluid section course-alternative-location">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col">-->
<!--                <h2>--><?php //the_field('looking_for_another_city_title'); ?><!--</h2>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col">-->
<!--                <div class="course-grid">-->
<!--                    <div class="row courses-row">-->
<!--                        <div class="col">Courses</div>-->
<!--                        --><?php //foreach ($eventNames as $eventName) : ?>
<!--                            <div class="col">--><?php //echo $eventName; ?><!--</div>-->
<!--                        --><?php //endforeach; ?>
<!--                    </div>-->
<!--                    --><?php //foreach ($eventLocations as $eventLocation) : ?>
<!--                        <div class="row">-->
<!--                            <div class="col locations-col">--><?php //echo $eventLocation; ?><!--</div>-->
<!--                            --><?php //foreach ($eventNames as $eventName) : ?>
<!--                                <div class="col" id="--><?php //echo strtolower(str_replace(' ', '', $eventName)) .'_'.strtolower(str_replace(' ', '', $eventLocation)); ?><!--"></div>-->
<!--                            --><?php //endforeach; ?>
<!--                        </div>-->
<!--                    --><?php //endforeach; ?>
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!-- END: ANOTHER LOCATION -->

<script type="text/javascript">
    //var otherEvents = <?php //echo json_encode($testEvents) ;?>//;
    //$.each(otherEvents, function(key, event) {
    //    var id = event.Name.replace(' ', '') + '_' + event.Location.replace(' ', '');
    //    id = id.toLowerCase();
    //    $('.course-grid #'+id).append(event.StartDateTime);
    //});
    $('.class-details').each(function() {
        $(this).find('table').addClass('table');
    })
    $(document).on('change', 'select#region-selector', function () {
        var region = $(this).val(),
            templateId = <?php echo $eventTemplate->TemplateID; ?>;

        $.ajax({
            url: vars.ajax_url,
            method: 'GET',
            data: 'action=get_arlo_events&region='+ region+'&template_id='+templateId
        })
          .done(function (resp) {
              if(resp != 'error') {
                  $('.region-classes-container').html(resp);
              }
          })
          .fail(function (resp) {
          });
    });
</script>

<?php get_footer(); ?>









