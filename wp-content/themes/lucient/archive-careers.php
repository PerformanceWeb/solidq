<?php
$useBackgroundImage = get_field('careers_use_background_image', 'option');
$useSectionText = get_field('careers_use_section_text', 'option');
$useTitleText = get_field('careers_use_title_text', 'option');
$useIntroText = get_field('careers_use_intro_text', 'option');
$useButton = get_field('careers_use_button', 'option');
$buttonClass = getItemColourClass(get_field('careers_button_border_colour', 'option'), 'btn-outline');
$useSmallCallToAction = get_field('careers_use_small_call_to_action', 'option');
$useLargeCallToAction = get_field('careers_use_large_call_to_action', 'option');
$useLatestNews = get_field('careers_use_latest_news', 'option');
?>

<?php get_header(); ?>
<div id="careers">
    <div class="container-fluid standard-header" <?php echo ($useBackgroundImage) ? 'style="background-image: url('.get_field('careers_background_image', 'option')['url'].');"' : ''; ?>>
        <div class="container">
            <div class="row">
                <div class="col">
                    <?php if($useSectionText) : ?>
                        <p><?php the_field('careers_section_text', 'option'); ?></p>
                    <?php endif; ?>
                    <?php if($useTitleText) : ?>
                        <h1><?php the_field('careers_title_text', 'option'); ?></h1>
                    <?php endif; ?>
                    <?php if($useIntroText) : ?>
                        <p class="hide-mobile"><?php the_field('careers_intro_text', 'option'); ?></p>
                    <?php endif; ?>
                    <?php if($useButton) : ?>
                        <a href="<?php the_field('careers_button_link', 'option'); ?>" class="btn btn-text--white btn-outline <?php echo $buttonClass; ?>"><?php the_field('careers_button_text', 'option'); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid section">
        <div class="container">
            <div class="row post-header">
                <div class="col">
                    <h2 class="text-center"><?php the_field('careers_title', 'option'); ?></h2>
                    <p class="text-center"><?php the_field('careers_intro', 'option'); ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?php if (have_posts()) :
                        while (have_posts()) : the_post(); $postCount++;
                            $excerpt = get_the_excerpt();
                            $title = get_the_title();
                            $link = get_the_permalink();
                            $location = get_field('location');
                            $address = get_field('address'); ?>
                            <div class="jobopportunity jobopportunity-size--large">
                                <div class="details">
                                    <h4><?php echo $title; ?></h4>
                                    <p class="location"><i class="fas fa-map-marker-alt"></i><?php echo $location; ?></p>
                                    <p class="address"><?php echo $address; ?></p>
                                    <p class="text"><?php echo $excerpt; ?></p>
                                </div>
                                <div class="button">
                                    <a href="<?php echo $link; ?>" class="btn btn-text--white btn-solid btn-solid--purple"><?php the_field('apply_now_button_text', 'option'); ?></a>
                                </div>
                            </div>
                        <?php endwhile;
                        wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col text-center jobopportunities-footer">
                    <h4><?php the_field('careers_get_in_touch_large_text', 'option'); ?></h4>
                    <p><?php the_field('careers_get_in_touch_small_text', 'option'); ?></p>
                    <a href="<?php the_field('careers_get_in_touch_button_link', 'option'); ?>" class="btn btn-text--black btn-underline btn-underline--purple"><?php the_field('careers_get_in_touch_button_text', 'option'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if($useSmallCallToAction) :
    $buttonClass = getItemColourClass(get_field('careers_small_call_to_action_button_border_colour', 'option'), 'btn-outline'); ?>
    <div class="container-fluid section calltoaction calltoaction-text--center">
        <div class="container" style="background-image: url(<?php echo get_field('careers_small_call_to_action_background', 'option')['url'];?>);">
            <div class="row">
                <div class="col">
                    <h2><?php the_field('careers_small_call_to_action_title', 'option'); ?></h2>
                    <p><?php the_field('careers_small_call_to_action_text', 'option'); ?></p>
                    <a href="<?php the_field('careers_small_call_to_action_button_link', 'option'); ?>" class="btn btn-text--white btn-outline <?php echo $buttonClass;?>"><?php the_field('careers_small_call_to_action_button_text', 'option'); ?></a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if($useLatestNews) : ?>
    <?php get_template_part('template-parts/latest_news'); ?>
<?php endif; ?>

<?php if($useLargeCallToAction) :
    $buttonClass = getItemColourClass(get_field('careers_large_call_to_action_button_border_colour', 'option'), 'btn-outline'); ?>
    <div class="container-fluid section calltoaction calltoaction-text--left" style="background-image: url(<?php echo get_field('careers_large_call_to_action_background', 'option')['url'];?>);">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2><?php the_field('careers_large_call_to_action_title', 'option'); ?></h2>
                    <p><?php the_field('careers_large_call_to_action_text', 'option'); ?></p>
                    <a href="<?php the_field('careers_large_call_to_action_button_link', 'option'); ?>" class="btn btn-text--white btn-outline <?php echo $buttonClass;?>"><?php the_field('careers_large_call_to_action_button_text', 'option'); ?></a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>


