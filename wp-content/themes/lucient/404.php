<?php get_header(); ?>

    <div class="container-fluid section page-404">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <p><?php the_field('404_small_title', 'option'); ?></p>
                    <h1><?php the_field('404_large_title', 'option'); ?></h1>
                    <p class="intro"><?php the_field('404_intro_text', 'option'); ?></p>
                    <p><?php the_field('404_what_are_you_looking_for_text', 'option'); ?></p>
                    <?php
                    wp_nav_menu(array(
                            'theme_location'  => 'menu_404',
                            'depth'           => 1, // 1 = no dropdowns, 2 = with dropdowns.
                            'container'       => 'div',
                            'container_id'    => '404-menu',
                            'menu_class'      => 'list',
                            'walker'          => new Custom_Walker_Nav_Menu()
                    ));
                    ?>
                </div>
                <div class="col-lg-5">
                    <img class="img-fluid" src="<?php echo getImg('Bulb-Animation.gif'); ?>" alt="Animated light bulb"/>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>