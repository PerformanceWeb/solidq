<?php

$post_id = false;
if(is_home())
    $post_id = get_option('page_for_posts');

	if (have_rows('layout_options', $post_id))
	{
		while (have_rows('layout_options', $post_id))
		{
			the_row();

			switch (get_row_layout())
			{
				case 'panels':
					get_template_part('template-parts/panels');
					break;
                case 'icon_panels':
                    get_template_part('template-parts/icon_panels');
                    break;
				case 'image_row':
                    get_template_part('template-parts/image_row');
					break;
				case 'icon_blocks':
                    get_template_part('template-parts/icon_blocks');
					break;
                case '50_50_image_and_text':
                    get_template_part('template-parts/50_50_image_and_text');
                    break;
                case '60_40_text_and_image':
                    get_template_part('template-parts/60_40_text_and_image');
                    break;
                case 'testimonials':
                    get_template_part('template-parts/testimonials');
                    break;
                case 'single_testimonial':
                    get_template_part('template-parts/single_testimonial');
                    break;
                case 'call_to_action':
                    get_template_part('template-parts/call_to_action');
                    break;
                case 'latest_news':
                    get_template_part('template-parts/latest_news');
                    break;
                case 'latest_events':
                    get_template_part('template-parts/latest_events');
                    break;
                case 'guide_download':
                    get_template_part('template-parts/guide_download');
                    break;
                case 'contact_form':
                    get_template_part('template-parts/contact_form');
                    break;
                case 'image_grid':
                    get_template_part('template-parts/image_grid');
                    break;
                case 'faqs':
                    get_template_part('template-parts/faqs');
                    break;
                case 'three_columns_text':
                    get_template_part('template-parts/three_columns_text');
                    break;
                case 'job_opportunities':
                    get_template_part('template-parts/job_opportunities');
                    break;
                case 'location':
                    get_template_part('template-parts/location');
                    break;
                case 'button_row':
                    get_template_part('template-parts/button_row');
                    break;
                case 'one_column_text':
                    get_template_part('template-parts/one_column_text');
                    break;
                case 'request_flex_trial':
                    get_template_part('template-parts/request_flex_trial');
                    break;
                case 'tabbed_accordions':
                    get_template_part('template-parts/tabbed_accordions');
                    break;
                case 'newsletter':
                    get_template_part('template-parts/newsletter');
                    break;
                case 'event_landing_page':
                    get_template_part('template-parts/event_landing_page');
                    break;
                case 'video_embed':
                    get_template_part('template-parts/video_embed');
                    break;
                case 'simple_text_field':
                    get_template_part('template-parts/simple_text_block');
                    break;
                default:
					echo "Unknown Layout";
					break;
			}
		}
	}
?>