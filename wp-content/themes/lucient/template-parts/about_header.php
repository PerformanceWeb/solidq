<?php
$post_id = false;
if(is_home())
    $post_id = get_option('page_for_posts');
?>

<div class="container-fluid about-header">
    <div class="container">
        <div class="row">
            <div class="col-md-5 no-margin">
                <h1><?php the_field('about_title', $post_id); ?></h1>
                <p><?php the_field('about_intro', $post_id); ?></p>
            </div>
            <div class="col-md-7 no-margin">
                <img src="<?php echo get_field('about_heading_image')['url']; ?>" alt="<?php echo get_field('about_heading_image')['alt']; ?>" class="img-fluid">
            </div>
        </div>
    </div>
</div>