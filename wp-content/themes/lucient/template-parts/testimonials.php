<?php
$useTitle = get_sub_field('use_section_title');
$columns = get_sub_field('items_per_row');
?>

<div class="container-fluid section testimonials">
    <div class="container slim">
        <?php if($useTitle) : ?>
            <div class="row">
                <div class="col no-margin">
                    <h2 class="text-center"><?php the_sub_field('section_title'); ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <div class="row mobile-slider" data-arrow-pos="20">
            <?php if(have_rows('testimonial_repeater')) : ?>
                <?php while(have_rows('testimonial_repeater')) : the_row(); ?>
                    <div class="col-md-<?php echo $columns; ?>">
                        <div class="testimonial testimonial-size--small <?php echo (get_sub_field('use_image')) ? 'with-image' : ''; ?>">
                            <div class="image">
                                <?php if(get_sub_field('use_image')) : ?>
                                    <img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['alt']; ?>" class="img-fluid">
                                <?php endif; ?>
                                <div>
                                    <h4 class="hide-desktop"><?php the_sub_field('name'); ?></h4>
                                    <p class="hide-desktop"><?php the_sub_field('role'); ?></p>
                                </div>
                            </div>
                            <div class="content">
                                <p class="body"><?php the_sub_field('text'); ?></p>
                                <h4 class="hide-mobile"><?php the_sub_field('name'); ?></h4>
                                <p class="hide-mobile"><?php the_sub_field('role'); ?></p>
                            </div>
                        </div>
                    </div>
                    <?php $count++; ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>