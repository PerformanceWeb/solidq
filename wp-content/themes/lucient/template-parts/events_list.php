<?php
$events = get_query_var('events');
var_dump($events);
?>

<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col">
                <?php if($events->have_posts()) : ?>
                    <?php while($events->have_posts()) :
                        $events->the_post();
                        $date = new DateTime(str_replace('/', '-', get_field('date'))); ?>
                        <div class="eventitem eventitem-size--large">
                            <div>
                                <p><?php the_title(); ?></p>
                            </div>
                            <div>
                                <p class="date"><?php echo $date->format('d.m.Y'); ?></p>
                                <p class="time"><?php echo the_field('time'); ?></p>
                                <p class="location"><?php echo get_field('location'); ?></p>
                            </div>
                            <div>
                                <?php the_content(); ?>
                            </div>
                            <div>
                                <a href="<?php the_permalink(); ?>" class="btn btn-solid btn-text--white btn-solid--purple"><?php the_field('event_list_register_button_text'); ?></a>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
