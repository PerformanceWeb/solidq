<?php
$useTitle = get_sub_field('use_section_title');
$count = count(get_sub_field('images'));
$useBackgroundColour = get_sub_field('use_background_colour');
$backgroundColour = get_sub_field('background_colour');
$useTextColour = get_sub_field('use_text_colour');
$textColour = get_sub_field('text_colour');
?>

<div class="container-fluid section" <?php echo ($useBackgroundColour ? 'style="background:'.$backgroundColour.'"' : ''); ?>>
    <div class="container">
        <?php if($useTitle) : ?>
            <div class="row">
                <div class="col no-margin">
                    <h2 class="text-center" <?php echo ($useTextColour ? 'style="color:'.$textColour.';"' : ''); ?>><?php the_sub_field('section_title'); ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <div class="row align-items-center justify-content-between imagerow">
            <?php if(have_rows('images')) : ?>
                <?php while(have_rows('images')) : the_row(); ?>
                    <div class="<?php echo ($count == 1 ? 'col' : 'col-6 col-sm-6'); ?> col-md">
                        <?php if(get_sub_field('use_image_link')) : ?>
                            <a href="<?php the_sub_field('image_link'); ?>" target="_blank">
                                <img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['alt']; ?>" class="img-fluid">
                            </a>
                        <?php else : ?>
                            <img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['alt']; ?>" class="img-fluid">
                        <?php endif; ?>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>