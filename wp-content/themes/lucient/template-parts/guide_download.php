<?php
$useBackgroundColour = get_sub_field('use_background_colour');
$backgroundColour = get_sub_field('background_colour');
?>

<div class="container-fluid section guidedownload" <?php echo ($useBackgroundColour ? 'style="background:'.$backgroundColour.';"' : ''); ?>>
    <div class="container">
        <div class="row">
            <div class="col-md">
                <div class="content">
                    <h2><?php the_sub_field('section_title'); ?></h2>
                    <div>
                        <p><?php the_sub_field('text'); ?></p>
                        <img src="<?php echo get_sub_field('guide_image')['url']; ?>" alt="<?php echo get_sub_field('guide_image')['alt']; ?>" class="img-fluid hide-desktop">
                    </div>
                    <div class="hide-mobile">
                        <?php echo do_shortcode(get_sub_field('gravity_forms_shortcode')); ?>
                    </div>
                </div>
            </div>
            <div class="col-md">
                <div class="image">
                    <img src="<?php echo get_sub_field('guide_image')['url']; ?>" alt="<?php echo get_sub_field('guide_image')['alt']; ?>" class="img-fluid hide-mobile">
                    <div class="hide-desktop">
                        <?php echo do_shortcode(get_sub_field('gravity_forms_shortcode')); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>