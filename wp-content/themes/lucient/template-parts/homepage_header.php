<?php
$buttonClass = getItemColourClass(get_field('button_border_colour'), 'btn-outline');
?>

<div class="container-fluid homepage-header" style="background-image: url(<?php echo get_field('background_image')['url']; ?>);">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1><?php the_field('title'); ?></h1>
                <p><?php the_field('intro_text'); ?></p>
                <a href="<?php the_field('button_link'); ?>" class="btn btn-text--white btn-outline <?php echo $buttonClass; ?>"><?php the_field('button_text'); ?></a>
            </div>
        </div>
    </div>
</div>