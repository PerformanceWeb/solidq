<?php
$useTitle = get_sub_field('use_section_title');
$useBackgroundColour = get_sub_field('use_background_colour');
$backgroundColour = get_sub_field('background_colour');
?>

<div class="container-fluid section iconblocks" <?php echo ($useBackgroundColour ? 'style="background:'.$backgroundColour.';"' : ''); ?>>
    <div class="container">
        <?php if($useTitle) : ?>
            <div class="row">
                <div class="col no-margin">
                    <h2><?php the_sub_field('section_title'); ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <?php if(have_rows('icon_blocks_repeater')) : ?>
                <?php while(have_rows('icon_blocks_repeater')) : the_row();
                    $useButton = get_sub_field('use_button');
                    $buttonText = get_sub_field('button_text');
                    $buttonLink = get_sub_field('button_link');
                    $useCustomButton = get_sub_field('use_custom_button_colour');
                    $customButtonColour = get_sub_field('custom_button_colour');
                    $customButtonTextColour = get_sub_field('custom_button_text_colour');
                    $buttonStyles = ($useCustomButton ? 'style="color:'.$customButtonTextColour.'; background:'.$customButtonColour.'; border-width:0px;"' : ''); ?>
                    <div class="col-lg">
                        <div class="iconblock">
                            <img src="<?php echo get_sub_field('icon')['url']; ?>" alt="<?php echo get_sub_field('icon')['alt']; ?>" class="img-fluid">
                            <div class="content">
                                <h3><?php the_sub_field('title'); ?></h3>
                                <p><?php the_sub_field('text'); ?></p>
                                <?php if($useButton) : ?>
                                    <a href="<?php echo $buttonLink; ?>" class="btn btn-solid btn-text--white btn-solid--purple" <?php echo $buttonStyles; ?>><?php echo $buttonText; ?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>