<?php
$buttonText = get_field('case_study_slider_read_more_button_text', 'option');
$wpQuery = new WP_Query(array(
    'post_type'         => 'case_studies',
    'posts_per_page'    => -1,
    'meta_key'          => 'add_to_slider',
    'meta_value'        => 1
));
?>

<div class="container-fluid section case-study-slider">
    <div class="arrows"></div>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="slider">
                    <?php if($wpQuery->have_posts()) : ?>
                        <?php while($wpQuery->have_posts()) :
                            $wpQuery->the_post();
                            $company = get_field('company_author');
                            $title = get_the_title();
                            $text = get_field('text');
                            $image = get_field('image');
                            $link = get_permalink(); ?>
                            <div class="slide">
                                <div class="text">
                                    <h3><?php echo $company; ?></h3>
                                    <p><?php echo $title; ?></p>
                                    <p class="body"><?php echo $text; ?></p>
                                    <a href="<?php echo $link; ?>" class="btn btn-text--black btn-underline btn-underline--purple"><?php echo $buttonText; ?></a>
                                </div>
                                <div class="image">
                                    <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt']; ?>" class="img-fluid">
                                </div>
                            </div>
                        <?php endwhile;
                        wp_reset_postdata() ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
