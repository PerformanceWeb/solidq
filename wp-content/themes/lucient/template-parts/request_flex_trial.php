<?php
global $wp;
$url = home_url().'/'.$wp->request;
?>

<div class="container-fluid section flextrial">
    <div class="container" style="background-image: url(<?php echo '/wp-content/themes/lucient/public/images/flexTrialBg.png'; ?>);">
        <div class="row align-items-center">
            <div class="col-md-8 no-margin">
                <div class="text">
                    <h2><?php the_sub_field('title_text'); ?></h2>
                    <p><?php the_sub_field('small_text'); ?></p>
                </div>
            </div>
            <div class="col-md-4 no-margin">
                <div class="form">
                    <?php //echo do_shortcode('[contact-form-7 id="470" title="Flex trial signup"]'); ?>
                    <?php echo do_shortcode('[pardot-form id="41415" querystring="form_url='.$url.'"]'); ?>
                </div>
            </div>
        </div>
    </div>
</div>