<?php
?>


<div class="container-fluid section tabbedaccordions">
    <div class="container slim">
        <div class="row">
            <div class="col">
                <div class="tabs">
                    <?php if(have_rows('tabs')) :
                        $i = 0; ?>
                        <?php while(have_rows('tabs')) :  the_row(); ?>
                            <p class="<?php echo($i === 0) ? 'active' : ''; ?>" data-tab="<?php echo strtolower(str_replace([' ', ','], ['_', ''], get_sub_field('name'))); ?>"><?php the_sub_field('name'); ?></p>
                            <?php $i++; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <?php if(have_rows('tabs')) :
                    $i = 0; ?>
                    <?php while(have_rows('tabs')) :  the_row(); ?>
                        <div class="tab-content faqs <?php echo($i === 0) ? 'active' : ''; ?>" id="<?php echo strtolower(str_replace([' ', ','], ['_', ''], get_sub_field('name'))); ?>">
                            <?php if(have_rows('items')) : ?>
                                <?php while(have_rows('items')) : the_row(); ?>
                                    <div class="faq">
                                        <p class="faq-question"><?php the_sub_field('title'); ?><span class="plus"></span></p>
                                        <p class="faq-answer"><?php the_sub_field('content');?></p>
                                    </div>
                                    <?php $i++; ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>