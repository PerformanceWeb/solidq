<?php
$useTitle = get_sub_field('use_section_title');
$purpleBg = get_sub_field('purple_background');
?>
<style>
    .gform_wrapper input[type="submit"] {
        cursor: pointer;
        background: #a17df6;
        padding-left: 25px;
        padding-right: 25px;
        border-radius: 5px;
        border: 0;
        color: #fff;
        font-family: 'Aventa SemiBold', sans-serif;
        font-weight: 300;
        font-size: 20px;
        font-style: normal;
        text-align: center;
    }
    .gform_wrapper input[type="checkbox"] {
        width: auto;
        margin-top: 9px;
        margin-bottom: 10px;
    }
    .gf_checkbox_inline input[type="checkbox"] {
        display: inline;
    }
    .gf_checkbox_inline label {
        display: inline;
    }
</style>

<?php if($purpleBg) : ?>
    <style>
        .gform_wrapper label {
            color: #fff;
        }
    </style>
<?php else : ?>
    <style>
        input, select, textarea {
            border: 1px solid #a17df6;
        }
    </style>
<?php endif; ?>

<div class="container-fluid section" <?php echo ($purpleBg ? 'style="background:#110027;"' : ''); ?>>
    <div class="container">
        <?php if($useTitle) : ?>
            <div class="row">
                <div class="col-md no-margin">
                    <h2 class="text-center" <?php echo ($purpleBg ? 'style="color:#fff;"' : ''); ?>><?php the_sub_field('section_title'); ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col">
                <?php the_sub_field('content'); ?>
            </div>
        </div>
    </div>
</div>