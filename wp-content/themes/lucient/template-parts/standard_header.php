<?php
$post_id = false;
if(is_home())
    $post_id = get_option('page_for_posts');
$useBackgroundImage = get_field('use_background_image', $post_id);
$useSectionText = get_field('use_section_text', $post_id);
$useTitleText = get_field('use_title_text', $post_id);
$useIntroText = get_field('use_intro_text', $post_id);
$useButton = get_field('use_button', $post_id);
$buttonClass = getItemColourClass(get_field('button_border_colour', $post_id), 'btn-outline');
?>

<div class="container-fluid standard-header <?php echo (is_home()) ? 'mobile-left-align' : ''; ?> <?php echo (!$useBackgroundImage) ? 'no-bg' : ''; ?> <?php echo (is_page('cookies')) ? 'cookies' : ''; ?>" <?php echo ($useBackgroundImage) ? 'style="background-image: url('.get_field('background_image', $post_id)['url'].');"' : ''; ?>>
    <div class="container">
        <div class="row">
            <div class="col">
                <?php if($useSectionText) : ?>
                    <p><?php the_field('section_text', $post_id); ?></p>
                <?php endif; ?>
                <?php if($useTitleText) : ?>
                    <h1><?php the_field('title_text', $post_id); ?></h1>
                <?php endif; ?>
                <?php if($useIntroText) : ?>
                    <p class="hide-mobile"><?php the_field('intro_text', $post_id); ?></p>
                <?php endif; ?>
                <?php if($useButton) : ?>
                    <a href="<?php the_field('button_link', $post_id); ?>" class="btn btn-text--white btn-outline <?php echo $buttonClass; ?>"><?php the_field('button_text', $post_id); ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>