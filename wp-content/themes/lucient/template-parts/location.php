<?php
$useTitle = get_sub_field('use_section_title');
$useDetails = get_sub_field('use_location_details');
$useContinent = get_sub_field('use_continent');
$useImage = get_sub_field('use_image');
$thinClass = (!$useImage) ? 'thin' : '';
?>

<div class="container-fluid section location <?php echo  $thinClass; ?>">
    <div class="container">
        <?php if($useTitle) : ?>
            <div class="row">
                <div class="col no-margin">
                    <h2 class="text-center"><?php the_sub_field('section_title'); ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <div class="row location-info">
            <div class="col-lg-6">
                <div class="location-text">
                    <?php if($useContinent) : ?>
                        <h3><?php the_sub_field('continent'); ?></h3>
                    <?php endif; ?>
                    <h3 class="city"><i class="fas fa-map-marker-alt"></i><?php the_sub_field('city_region'); ?></h3>
                </div>
                <?php if(get_sub_field('address')) : ?>
                    <p class="address"><?php the_sub_field('address'); ?></p>
                <?php endif; ?>
                <?php if(get_sub_field('website')) : ?>
                    <p class="website"><?php the_sub_field('website'); ?></p>
                <?php endif; ?>
                <div class="buttons">
                    <?php if(get_sub_field('phone')) : ?>
                        <a href="tel:<?php the_sub_field('phone'); ?>" class="btn btn-text--purple btn-outline btn-outline--purple"><?php the_sub_field('phone'); ?></a>
                    <?php endif; ?>
                    <a href="<?php the_sub_field('email_address'); ?>" class="btn btn-text--white btn-solid btn-solid--purple"><?php the_sub_field('email_button_text'); ?></a>
                </div>
                <?php if($useDetails) : ?>
                <button type="button" class="btn btn-text--black btn-underline btn-underline--purple more-info"><?php the_sub_field('more_info_button_text'); ?></button>
                <?php endif; ?>
            </div>
            <div class="col-lg-6">
                <?php if($useImage) : ?>
                    <img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php get_sub_field('image')['alt']; ?>" class="img-fluid hide-mobile img-radius">
                <?php endif; ?>
            </div>
        </div>
        <?php if($useDetails) : ?>
            <div class="row location-details">
                <?php if(have_rows('location_details')) : ?>
                    <?php while(have_rows('location_details')) :  the_row(); ?>
                        <div class="col-md-6">
                            <h3><?php the_sub_field('title'); ?></h3>
                            <p><?php the_sub_field('text'); ?></p>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
