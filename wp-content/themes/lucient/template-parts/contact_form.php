<?php
global $wp;
$url = home_url() . '/' . $wp->request;
$useTitle = get_sub_field('use_section_title');
$siteSuffix = str_replace('/', '', get_blog_details()->path) ?: 'en';
?>

<div id="contactform" class="container-fluid contactform section">
    <div class="container slim">
        <?php if($useTitle) : ?>
            <div class="row">
                <div class="col-md no-margin">
                    <h2 class="text-center"><?php the_sub_field('section_title'); ?></h2>
                </div>
            </div>
        <?php endif; ?>

        <?php if($siteSuffix == 'en') : ?>
            <?php echo do_shortcode('[pardot-form id="41393" querystring="form_url='.$url.'"]'); ?>
        <?php elseif($siteSuffix == 'de') : ?>
            <?php echo do_shortcode('[pardot-form id="41977" querystring="form_url='.$url.'"]'); ?>
        <?php elseif($siteSuffix == 'it') : ?>
            <?php echo do_shortcode('[pardot-form id="41971" querystring="form_url='.$url.'"]'); ?>
        <?php else : ?>
            <?php echo do_shortcode('[pardot-form id="41393" querystring="form_url='.$url.'"]'); ?>
        <?php endif; ?>
    </div>
</div>

