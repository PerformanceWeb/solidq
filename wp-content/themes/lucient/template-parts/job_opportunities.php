<?php
$useTitle = get_sub_field('use_section_title');
$useIntro = get_sub_field('use_section_intro');
$wpQuery = new WP_Query(array('post_type' => 'careers', 'post_per_page' => 3));
?>

<div class="container-fluid section jobopportunities">
    <div class="container slim">
        <?php if($useTitle) : ?>
            <div class="row">
                <div class="col no-margin">
                    <h2 class="text-center"><?php the_sub_field('section_title'); ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <?php if($useIntro) : ?>
            <div class="row">
                <div class="col no-margin">
                    <p class="text-center intro-text"><?php the_sub_field('section_intro'); ?></p>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col">
                <?php if($wpQuery->have_posts()) : ?>
                    <?php while($wpQuery->have_posts()) : $wpQuery->the_post(); ?>
                        <div class="jobopportunity jobopportunity-size--small">
                            <div>
                                <h4><?php the_title(); ?></h4>
                                <p class="location"><i class="fas fa-map-marker-alt"></i><?php the_field('location'); ?></p>
                            </div>
                            <p><?php the_excerpt(); ?></p>
                        </div>
                    <?php endwhile;
                    wp_reset_postdata(); ?>
                <?php endif ?>
            </div>
        </div>
        <div class="row">
            <div class="col text-center jobopportunities-footer">
                <h4><?php the_sub_field('get_in_touch_large_text'); ?></h4>
                <p><?php the_sub_field('get_in_touch_small_text'); ?></p>
                <a href="<?php the_sub_field('get_in_touch_button_link'); ?>" class="btn btn-text--black btn-underline btn-underline--purple"><?php the_sub_field('get_in_touch_button_text'); ?></a>
            </div>
        </div>
    </div>
</div>
