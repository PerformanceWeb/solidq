<?php
$type = get_sub_field('type');
$buttonClass = getItemColourClass(get_sub_field('button_border_colour'), 'btn-outline');
$containerClass = getItemColourClass($type, 'calltoaction');
$buttonPosition = get_sub_field('button_position');
$useCustomButton = get_sub_field('use_custom_button_colour');
$customButtonColour = get_sub_field('custom_button_colour');
$customButtonTextColour = get_sub_field('custom_button_text_colour');
if($buttonPosition == 'left')
    $buttonPositionClass = 'justify-content-start';
if($buttonPosition == 'center')
    $buttonPositionClass = 'justify-content-center';
if($buttonPosition == 'right')
    $buttonPositionClass = 'justify-content-end';
$buttonStyles = ($useCustomButton ? 'style="color:'.$customButtonTextColour.'; background:'.$customButtonColour.'; border-width:0px;"' : '');
?>

<div class="container-fluid section calltoaction <?php echo  $containerClass; ?>" <?php if($type == 'full') { echo 'style="background-image:url('.get_sub_field('background_image')['url'].')"';} ?>>
    <div class="container" <?php if($type == 'small') { echo 'style="background-image:url('.get_sub_field('background_image')['url'].')"'; } ?>>
        <div class="row">
            <div class="col">
                <h2><?php the_sub_field('section_title'); ?></h2>
                <p><?php the_sub_field('text'); ?></p>
                <div class="d-flex <?php echo $buttonPositionClass; ?>">
                    <a href="<?php the_sub_field('button_link'); ?>" class="btn btn-text--white btn-outline <?php echo $buttonClass;?>" <?php echo $buttonStyles; ?>><?php the_sub_field('button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>