<?php
$useTitle = get_sub_field('use_section_title');
$useIntro = get_sub_field('use_intro_text');
$greyBg = get_sub_field('grey_background');
$columns = get_sub_field('images_per_row');
$noOfRows = $count = count(get_sub_field('images_repeater'));
$imagesPerRow = 12/$columns;
?>

<div class="container-fluid section imagegrid" <?php echo ($greyBg) ? 'style="background:#F0F0F0;"' : ''; ?>>
    <div class="container slim">
        <?php if($useTitle) : ?>
            <div class="row">
                <div class="col no-margin">
                    <h2 class="text-center"><?php the_sub_field('section_title'); ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <?php if($useIntro) : ?>
            <div class="row">
                <div class="col no-margin">
                    <p class="text-center intro-text"><?php the_sub_field('intro_text'); ?></p>
                </div>
            </div>
        <?php endif; ?>
        <div class="row mobile-slider" data-arrow-pos="45">
            <?php if(have_rows('images_repeater')) :
                $count = 1; ?>
                <?php while(have_rows('images_repeater')) : the_row(); ?>
                    <?php if($count % $columns == 0 && $count < $noOfRows) : ?>
                        <div class="w-100"></div>
                    <?php endif; ?>
                    <div class="col-md-<?php echo $columns; ?>">
                        <img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['alt'];?>" class="img-fluid imagegrid-image img-radius">
                        <?php if(get_sub_field('use_image_description')) : ?>
                            <p class="title"><?php the_sub_field('title'); ?></p>
                            <p><?php the_sub_field('text'); ?></p>
                        <?php endif; ?>
                    </div>
                    <?php $count++; ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>