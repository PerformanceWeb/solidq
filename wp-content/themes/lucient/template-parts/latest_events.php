<?php
$useTitle = get_sub_field('use_section_title');
$blogPageHeading = get_field('blog_post_latest_news_section_heading', 'option');
$wpQuery = new WP_Query(array(
    'post_type'         => 'events',
    'posts_per_page'    => 3,
    'meta_key'          => 'date',
    'meta_value'        => date('Ymd', strtotime('-1 day')),
    'meta_compare'      => '>=', 
    'orderby'           => 'date',
    'order'             => 'ASC'
));

?>

<div class="container-fluid section latestevents">
    <div class="container slim">
        <div class="row">
            <div class="col-md">
                <div class="text">
                    <h2><?php the_sub_field('section_title'); ?></h2>
                    <p><?php the_sub_field('text'); ?></p>
                    <a href="/events" class="btn btn-text--white btn-solid btn-solid--purple"><?php the_sub_field('button_text'); ?></a>
                </div>
            </div>
            <div class="col-md">
                <?php if($wpQuery->have_posts()) : ?>
                    <?php while($wpQuery->have_posts()) :
                        $wpQuery->the_post();
                        $date = new DateTime(str_replace('/', '-', get_field('date'))); ?>
                        <a href="<?php the_permalink(); ?>" class="eventitem eventitem-size--small">
                            <div>
                                <p><?php echo $date->format('d'); ?></p>
                            </div>
                            <div>
                                <p class="date"><?php echo $date->format('d.m.Y'); ?></p>
                                <p class="time"><?php echo the_field('time'); ?></p>
                                <p class="location"><?php echo get_field('location'); ?></p>
                            </div>
                        </a>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>