<?php
$useTitle = get_sub_field('use_section_title');
?>

<div class="container-fluid section">
    <div class="container">
        <?php if($useTitle) : ?>
            <div class="row">
                <div class="col no-margin">
                    <h2 class="text-center"><?php the_sub_field('section_title'); ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="faqs">
                    <?php if(have_rows('faqs_repeater')) : ?>
                        <?php while(have_rows('faqs_repeater')) : the_row(); ?>
                            <div class="faq">
                                <p class="faq-question"><?php the_sub_field('question'); ?><span class="plus"></span></p>
                                <p class="faq-answer"><?php the_sub_field('answer');?></p>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

