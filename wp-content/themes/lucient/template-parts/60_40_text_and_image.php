<?php
$useBackgroundColour = get_sub_field('use_background_colour');
$backgroundColour = get_sub_field('background_colour');
$useList = get_sub_field('use_list');
$imageSide = get_sub_field('image_side');
$hideImageMobile = get_sub_field('hide_image_on_mobile');
$fistColumnClass = ($imageSide == 'left') ? '-4' : '-8';
$secondColumnClass = ($imageSide == 'right') ? '-4' : '-8';
$useButton = get_sub_field('use_button');
$buttonText = get_sub_field('button_text');
$buttonLink = get_sub_field('button_link');
$useCustomButton = get_sub_field('use_custom_button_colour');
$customButtonColour = get_sub_field('custom_button_colour');
$customButtonTextColour = get_sub_field('custom_button_text_colour');
$buttonStyles = ($useCustomButton ? 'style="color:'.$customButtonTextColour.'; background:'.$customButtonColour.'; border-width:0px;"' : '');
?>

<div class="container-fluid section sixtyfortytextimage image-side--<?php echo $imageSide; ?>" <?php echo ($useBackgroundColour ? 'style="background:'.$backgroundColour.';"' : ''); ?>>
    <div class="container">
        <div class="row">
            <div class="col-md<?php echo $fistColumnClass; ?> <?php echo ($imageSide == 'left' && $hideImageMobile) ? 'hide-mobile' : '';?>">
                <?php if($imageSide == 'left') : ?>
                    <img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['alt']; ?>" class="img-fluid img-left">
                <?php else : ?>
                    <div class="content">
                        <h2><?php the_sub_field('title'); ?></h2>
                        <?php the_sub_field('text'); ?>
                        <?php if($useList) : ?>
                            <div class="list list-type--columns">
                                <?php if(have_rows('list')) : ?>
                                    <?php while(have_rows('list')) : the_row(); ?>
                                        <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('name'); ?></a>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <?php if($useButton) : ?>
                            <a href="<?php echo $buttonLink; ?>" class="btn btn-solid btn-text--white btn-solid--purple" <?php echo $buttonStyles; ?>><?php echo $buttonText; ?></a>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-md<?php echo $secondColumnClass; ?> <?php echo ($imageSide == 'right' && $hideImageMobile) ? 'hide-mobile' : '';?>">
                <?php if($imageSide == 'right') : ?>
                    <img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['alt']; ?>" class="img-fluid img-right">
                <?php else : ?>
                    <div class="content">
                        <h2><?php the_sub_field('title'); ?></h2>
                        <?php the_sub_field('text'); ?>
                        <?php if($useList) : ?>
                            <div class="list list-type--columns">
                                <?php if(have_rows('list')) : ?>
                                    <?php while(have_rows('list')) : the_row(); ?>
                                        <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('name'); ?></a>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <?php if($useButton) : ?>
                            <a href="<?php echo $buttonLink; ?>" class="btn btn-solid btn-text--white btn-solid--purple" <?php echo $buttonStyles; ?>><?php echo $buttonText; ?></a>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

