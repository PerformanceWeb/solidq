<?php
$useTitle = get_sub_field('use_section_title');
?>

<div class="container-fluid section <?php echo (is_page('cookies')) ? 'cookies' : ''; ?>">
    <div class="container slim">
        <?php
        $useTitle = get_sub_field('use_section_title');
        ?>
        <div class="row">
            <div class="col">
                <?php the_sub_field('content'); ?>
            </div>
        </div>
    </div>
</div>
