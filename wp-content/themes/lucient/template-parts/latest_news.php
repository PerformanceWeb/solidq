<?php
$useTitle = get_sub_field('use_section_title');
$wpQuery = new WP_Query(array('post_type' => 'post', 'posts_per_page' => 3));
$blogPageHeading = get_field('blog_post_latest_news_section_heading', 'option');
$eventsPageHeading = get_field('events_latest_news_section_title', 'option');
$careersPageHeading = get_field('careers_latest_news_title', 'option');
?>

<div class="container-fluid section latestnews">
    <div class="container">
        <?php if($useTitle) : ?>
            <div class="row">
                <div class="col no-margin">
                    <h2><?php the_sub_field('section_title'); ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <?php if(is_single()) : ?>
            <div class="row">
                <div class="col no-margin">
                    <h2><?php echo $blogPageHeading; ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <?php if(is_post_type_archive('events')) : ?>
            <div class="row">
                <div class="col no-margin">
                    <h2><?php echo $eventsPageHeading; ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <?php if(is_post_type_archive('careers')) : ?>
            <div class="row">
                <div class="col no-margin">
                    <h2><?php echo $careersPageHeading; ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <?php if($wpQuery->have_posts()) : ?>
                <?php while($wpQuery->have_posts()) :
                    $wpQuery->the_post();
                    $featuredImage = get_the_post_thumbnail_url(null, 'full');
                    $excerpt = get_the_excerpt();
                    $title = get_the_title();
                    $link = get_the_permalink();
                    $tags = wp_get_post_terms(get_the_ID(), 'post_tag'); ?>
                    <div class="col-lg-4">
                        <div class="news-item">
                            <?php if($featuredImage) : ?>
                                <img src="<?php echo $featuredImage; ?>" alt="<?php echo $title; ?>" class="img-fluid">
                            <?php endif; ?>
                            <?php if(!empty($tags)) : ?>
                                <p class="label"><?php echo $tags[0]->name; ?></p>
                            <?php endif; ?>
                            <h4><a href="<?php echo $link; ?>"><?php echo $title; ?></a></h4>
                            <p><?php echo $excerpt; ?></p>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
    </div>
</div>