<?php
$btnType = get_sub_field('button_type');
$btnTextColour = get_sub_field('button_text_colour');
$btnColour= get_sub_field('button_colour');
$buttonClasses = $btnType . ' ' . $btnTextColour . ' ' . $btnType.$btnColour;
?>

<div class="container-fluid section buttonrow">
    <div class="container slim">
        <div class="row justify-content-center">
            <?php if(have_rows('buttons')) : ?>
                <?php while(have_rows('buttons')) : the_row();
                    $size = get_sub_field('size'); ?>
                    <?php
                        switch($size)
                        {
                            case 'small':
                                $class = 'col-md-2';
                                break;
                            case 'medium':
                                $class = 'col-md-4';
                                break;
                            case 'large':
                                $class = 'col-md-6';
                                break;
                            default:
                                $class = 'col-md-3';
                                break;
                        }
                    ?>
                    <div class="<?php echo $class ;?>">
                        <a href="<?php the_sub_field('link'); ?>" class="btn <?php echo $buttonClasses; ?>"><?php the_sub_field('text'); ?></a>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
