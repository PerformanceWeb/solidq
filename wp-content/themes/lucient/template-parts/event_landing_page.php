<?php

global $wp;
$url = home_url() . '/' . $wp->request;

$useTitle = get_sub_field('use_title');
$shortcode = get_sub_field('gravity_forms_shortcode')
?>

<!-- TEMP GRAVITY FORMS STYLES -->
<style>
    .gform_wrapper input[type="submit"] {
        cursor: pointer;
        background: #a17df6;
        padding-left: 25px;
        padding-right: 25px;
        border-radius: 5px;
        border: 0;
        color: #fff;
        font-family: 'Aventa SemiBold', sans-serif;
        font-weight: 300;
        font-size: 20px;
        font-style: normal;
        text-align: center;
    }
    .gform_wrapper input[type="checkbox"] {
        width: auto;
        margin-top: 9px;
        margin-bottom: 10px;
    }
    .gf_checkbox_inline input[type="checkbox"] {
        display: inline;
    }
    .gf_checkbox_inline label {
        display: inline;
    }
    input, select, textarea {
        border: 1px solid #a17df6;
    }
</style>

<div class="container-fluid section eventlandingpage">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="content">
                    <?php if($useTitle) : ?>
                        <h2><?php the_sub_field('title'); ?></h2>
                    <?php endif; ?>
                    <?php the_sub_field('content'); ?>
                </div>
            </div>
            <div class="col-lg-5">
                <h3><?php the_sub_field('form_title'); ?></h3>
                <div class="event-download-form-container">
                    <?php echo do_shortcode($shortcode); ?>
                </div>
            </div>
        </div>
    </div>
</div>