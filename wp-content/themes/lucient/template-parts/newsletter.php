<?php
global $wp;
$url = home_url().'/'.$wp->request;
?>

<div class="container-fluid newsletter section" style="background-image: url('<?php echo get_field('newsletter_background', 'option')['url']; ?>');">
    <div class="container slim">
            <div class="row">
                <div class="col-md no-margin">
                    <?php if(is_post_type_archive('books_publications')) : ?>
                        <p><?php the_field('books_small_title', 'option'); ?></p>
                        <h2><?php the_field('books_large_title', 'option'); ?></h2>
                    <?php else : ?>
                        <p><?php the_field('newsletter_small_title', 'option'); ?></p>
                        <h2><?php the_field('newsletter_big_title', 'option'); ?></h2>
                    <?php endif; ?>
<!--                    <form id="newsletter">-->
<!--                        <input type="email" placeholder="Enter Email" name="email">-->
<!--                        <button type="submit" class="btn  btn-solid btn-solid--purple btn-text--white">Subscribe</button>-->
<!--                    </form>-->
                    <?php echo do_shortcode('[pardot-form id="41417" querystring="form_url='.$url.'"]'); ?>
                </div>
            </div>
    </div>
</div>

