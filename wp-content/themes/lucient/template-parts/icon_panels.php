<?php
$useTitle = get_sub_field('use_section_title');
$panelColourClass = getItemColourClass(get_sub_field('panel_colour'), 'panel');
$useBackgroundColour = get_sub_field('use_background_colour');
$backgroundColour = get_sub_field('background_colour');
?>

<div class="container-fluid section" <?php echo ($useBackgroundColour ? 'style="background:'.$backgroundColour.';"' : ''); ?>>
    <div class="container slim">
        <?php if($useTitle) : ?>
            <div class="row">
                <div class="col no-margin">
                    <h2 class="text-center"><?php the_sub_field('section_title'); ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <?php if(have_rows('panels_repeater')) : ?>
                <?php while(have_rows('panels_repeater')) : the_row(); ?>
                    <?php $useButton = get_sub_field('use_button'); ?>
                    <div class="col-lg-4">
                        <div class="panel panel-type--icon <?php echo $panelColourClass; ?>">
                            <img src="<?php echo get_sub_field('icon')['url']; ?>" alt="<?php echo get_sub_field('icon')['alt']; ?>" class="img-fluid">
                            <div>
                                <h3><?php the_sub_field('large_text'); ?></h3>
                                <p><?php the_sub_field('small_text'); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>