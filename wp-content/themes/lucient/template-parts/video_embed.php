<?php
$useTitle = get_sub_field('use_section_title');
$useIntro = get_sub_field('use_intro_text');
?>

<div class="container-fluid section videoembed">
    <div class="container">
        <?php if($useTitle) : ?>
            <div class="row">
                <div class="col no-margin">
                    <h2 class="text-center"><?php the_sub_field('section_title'); ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <?php if($useIntro) : ?>
            <div class="row">
                <div class="col no-margin">
                    <p class="text-center"><?php the_sub_field('intro_text'); ?></p>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col">
                <div class="resp-iframe-container" style="width:<?php the_sub_field('video_width'); ?>%;">
                    <?php the_sub_field('video_embed_code'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
