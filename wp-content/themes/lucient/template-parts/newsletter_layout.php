<?php
global $wp;
$url = home_url() . '/' . $wp->request;
?>

<div class="container-fluid newsletter section" style="background-image: url('<?php echo get_field('newsletter_background', 'option')['url']; ?>');">
    <div class="container slim">
            <div class="row">
                <div class="col-md no-margin">
                    <p><?php the_sub_field('small_title'); ?></p>
                    <h2><?php the_sub_field('large_title'); ?></h2>
<!--                    <form id="newsletter">-->
<!--                        <input type="email" placeholder="Enter Email" name="email">-->
<!--                        <button type="submit" class="btn  btn-solid btn-solid--purple btn-text--white">Subscribe</button>-->
<!--                    </form>-->
                    <?php echo do_shortcode('[pardot-form id="41417" querystring="form_url='.$url.'"]'); ?>
                </div>
            </div>
    </div>
</div>

