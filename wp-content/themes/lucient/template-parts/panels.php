<?php
$useBackgroundColour = get_sub_field('use_background_colour');
$backgroundColour = get_sub_field('background_colour');
$useTitle = get_sub_field('use_section_title');
$panelColourClass = getItemColourClass(get_sub_field('panel_colour'), 'panel');
?>

<div class="container-fluid section" <?php echo ($useBackgroundColour ? 'style="background:'.$backgroundColour.';"' : ''); ?>>
    <div class="container">
        <?php if($useTitle) : ?>
            <div class="row">
                <div class="col no-margin">
                    <h2 class="text-center"><?php the_sub_field('section_title'); ?></h2>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <?php if(have_rows('panels_repeater')) : ?>
                <?php while(have_rows('panels_repeater')) : the_row(); ?>
                    <?php $useButton = get_sub_field('use_button'); ?>
                    <div class="col-lg-4">
                        <div class="panel <?php echo $panelColourClass; ?>">
                            <h3><?php the_sub_field('title'); ?></h3>
                            <p><?php the_sub_field('text'); ?></p>
                            <?php if($useButton) : ?>
                                <a href="<?php the_sub_field('button_link');?>" class="btn btn-underline btn-underline--purple btn-text--black"><?php the_sub_field('button_text'); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>