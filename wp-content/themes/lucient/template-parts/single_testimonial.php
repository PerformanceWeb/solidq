<?php

?>

<div class="container-fluid testimonial section testimonial-size--large">
    <div class="row">
        <div class="col-lg no-padding no-margin">
            <div class="content-container">
                <div class="content">
                    <h3><?php the_sub_field('name'); ?></h3>
                    <p><?php the_sub_field('role'); ?></p>
                    <p class="testimonial-text"><?php the_sub_field('testimonial'); ?></p>
                </div>
            </div>
        </div>
        <div class="col-lg image no-padding">
            <img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['alt']; ?>" class="img-fluid">
        </div>
    </div>
</div>