<?php
$allTags  = get_terms(array('taxonomy' => 'case_study_category', 'hide_empty' => false));
?>

<?php get_header(); ?>
<div id="casestudies">
    <div class="container-fluid standard-header mobile-left-align no-bg">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p><?php the_field('case_studies_list_page_small_title', 'option'); ?></p>
                    <h1><?php the_field('case_studies_list_page_large_title', 'option'); ?></h1>
<!--                    <div class="filter-container">-->
<!--                        <select name="filter" class="grey-outline">-->
<!--                            <option value="all">Category</option>-->
<!--                            --><?php //foreach($allTags as $tag) : ?>
<!--                                <option value="--><?php //echo $tag->slug; ?><!--">--><?php //echo $tag->name; ?><!--</option>-->
<!--                            --><?php //endforeach; ?>
<!--                        </select>-->
<!--                        <button type="button" class="filter-posts btn btn-text--purple btn-outline btn-outline--purple">--><?php //the_field('case_studies_filter_button_text', 'option'); ?><!--</button>-->
<!--                    </div>-->
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid section">
        <div class="container">
            <?php if (have_posts()) :
                $postCount = 0;
                while (have_posts()) : the_post(); $postCount++;
                    $excerpt = get_the_excerpt();
                    $featuredImage = get_the_post_thumbnail_url(null, 'full');
                    $title = get_the_title();
                    $link = get_the_permalink();
                    $tags = wp_get_post_terms(get_the_ID(), 'case_study_category');
                    if($postCount == 1) : ?>
                        <div class="row featured-post hide-mobile-flex post-container" data-tags="<?php echo implode('|', array_column($tags, 'slug')); ?>">
                            <div class="col-md-8"><img src="<?php echo $featuredImage; ?>" alt="<?php echo $title; ?>" class="img-fluid"></div>
                            <div class="col-md-4">
                                <p class="label"><?php echo implode(', ', array_column($tags, 'name')); ?></p>
                                <h2><a href="<?php echo $link; ?>"><?php echo $title; ?></a></h2>
                                <p><?php echo $excerpt; ?></p>
                                <a href="<?php echo $link; ?>"><img src="/wp-content/themes/lucient/public/images/arrowRight.png" class="img-fluid"></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 hide-desktop-flex post-container" data-tags="<?php echo implode('|', array_column($tags, 'slug')); ?>">
                                <div class="news-item">
                                    <img src="<?php echo $featuredImage; ?>" alt="<?php echo $title; ?>" class="img-fluid">
                                    <?php if(!empty($tags)) : ?>
                                        <p class="label"><?php echo implode(', ', array_column($tags, 'name')); ?></p>
                                    <?php endif; ?>
                                    <h4><a href="<?php echo $link; ?>"><?php echo $title; ?></a></h4>
                                    <p><?php echo $excerpt; ?></p>
                                </div>
                            </div>
                    <?php else : ?>
                        <div class="col-lg-4 post-container" data-tags="<?php echo implode('|', array_column($tags, 'slug')); ?>">
                            <div class="news-item">
                                <img src="<?php echo $featuredImage; ?>" alt="<?php echo $title; ?>" class="img-fluid">
                                <?php if(!empty($tags)) : ?>
                                    <p class="label"><?php echo implode(', ', array_column($tags, 'name')); ?></p>
                                <?php endif; ?>
                                <h4><a href="<?php echo $link; ?>"><?php echo $title; ?></a></h4>
                                <p><?php echo $excerpt; ?></p>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endwhile;
                wp_reset_postdata(); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php
$buttonClass = getItemColourClass(get_field('case_studies_call_to_action_button_border_colour', 'option'), 'btn-outline');
?>

<div class="container-fluid section calltoaction calltoaction-text--left" style="background-image: url(<?php echo get_field('case_studies_call_to_action_background_image', 'option')['url'];?>);">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2><?php the_field('case_studies_call_to_action_title', 'option'); ?></h2>
                <p><?php the_field('case_studies_call_to_action_text', 'option'); ?></p>
                <a href="<?php the_field('case_studies_call_to_action_button_link', 'option'); ?>" class="btn btn-text--white btn-outline <?php echo $buttonClass;?>"><?php the_field('case_studies_call_to_action_button_text', 'option'); ?></a>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>


